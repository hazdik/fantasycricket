WebsocketRails::EventMap.describe do
  subscribe :hello, 'socket#send_live_score'
  subscribe :update, 'socket#send_live_score'
end
