set :branch, 'staging'
set :user, 'fantoss'
set :rails_env, 'staging'
set :enable_ssl, false
set :rvm_map_bins, fetch(:rvm_map_bins).to_a.concat(%w{ puma pumactl sidekiq sidekiqctl })
# set :puma_env, 'staging'

server 'www.fantoss.com', user: 'fantoss', roles: %w{web app db}, primary: true

after 'deploy', 'deploy:migrate'
