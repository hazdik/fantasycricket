Rails.application.config.middleware.use ExceptionNotification::Rack,
  :email => {
    :email_prefix => "",
    :sender_address => %{"fantoss_exceptions" <play@fantoss.com>},
    :exception_recipients => %w{ schnmudgal@gmail.com }
  }
