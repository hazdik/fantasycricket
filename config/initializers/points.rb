module Rails::Application::Config
  class Points < Settingslogic
    source "#{Rails.root}/config/points.yml"
    namespace Rails.env
    load!
  end
end

Rails.application.config.points = Rails::Application::Config::Points
