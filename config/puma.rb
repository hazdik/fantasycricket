if defined? PumaWorkerKiller

  before_fork do
    PumaWorkerKiller.config do |config|
      config.ram           = 1024 # mb
      config.frequency     = 5    # seconds
      config.percent_usage = 0.88
      config.rolling_restart_frequency = 12 * 3600 # 12 hours in seconds
    end
    PumaWorkerKiller.start
  end

end

