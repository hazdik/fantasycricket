Rails.application.routes.draw do
# mount Lockup::Engine, at: '/lockup'

# API Docs
# resources :api_docs, only: :index

# API Routes
# get 'opera' => 'opera#index'
# get 'databuddy' => 'databuddy#index'

# namespace :api do
#     api_version(module: "V1", header: { name: "Accept", value: "application/fantoss; version=V1" }, defaults: { format: :json }) do
#       #Users
#       post 'send_otp'     => 'users#send_otp'
#       post 'verify_otp'   => 'users#verify_user_otp'
#       post 'signin_fb'    => 'users#signin_fb'
#       get  'user_details' => 'users#user_details'
#       get  'old_matches'  => 'matches#old_matches'
#       get 'getTestingResults' => 'users#getTestingResults'
#       get  'user_referral_details' => 'users#user_referral_details'

#       resources :users, only: :update
#       resources :coupons, only: [] do
#         collection do
#           post :apply
#         end
#       end

#       #Matches
#       resources :match, only: [] do
#         collection do
#           get 'upcoming_matches' => "matches#upcoming_matches"
#           get 'live_matches'     => "matches#live_matches"
#         end
#         get 'players'       => "matches#players"
#         get 'teams'         => "user_team#index"
#         get 'leaderboards'  => 'matches#leaderboards'
#       end

#       #UserTeams
#       resources :user_team, only: [] do
#         collection do
#           post 'create' => "user_team#create_team"
#           get 'leaderboards' => 'user_team#getleaderboards'
#         end
#         member do
#           get "edit" => "user_team#edit"
#           put "update" => "user_team#update"
#         end

#       end

#       resources :money_transactions, only: [:index, :create, :update]
#       resources :leagues, only: [:show]

#       #UserTransactions
#       post "user/add_to_wallet"   => "user_transactions#add_to_wallet"
#       post 'join_league'          => "user_transactions#join_league"
#       get  'transaction_history'  => "user_transactions#transaction_history"
#       get 'gettransactions'      => "user_transactions#gettransactions"

#     end
# end


  namespace :admin do
    DashboardManifest::DASHBOARDS.each do |dashboard_resource|
      case dashboard_resource
      when :matches then
        resources :matches do
          get :winners, on: :member
        end
      else
        resources dashboard_resource
      end
    end

    resources :entity_api_provider, only: [] do
      collection do
        get :seasons
        get :seasons_competitions
        get :competition_show
      end
    end

    resources :data, only: [] do
      collection do
        get :money_transactions
        get :new_users
        User::AFFILIATE_DETAILS.keys.each do |affiliate|
          get "#{ affiliate }_users".to_sym
        end
        get :stats
        get :matches
        get :leagues
        get :upcoming_series
        get :sync
        get :sync_squad
        get :league_odd_even
        #ScoresPRO
        get :sync_match
        get :sync_scorespro_squad
      end
    end

    root controller: DashboardManifest::ROOT_DASHBOARD, action: :index
  end

  if defined? Sidekiq
    require 'sidekiq/web'
    require 'sidekiq/cron/web'
    authenticate :user, lambda {|u| u.is_admin? } do
      mount Sidekiq::Web, at: '/admin/sidekiq/jobs', as: :sidekiq
    end
  end



  # mount RailsAdmin::Engine => '/admin', :as => 'rails_admin' if defined? RailsAdmin

  # OAuth
  oauth_prefix = Rails.application.config.auth.omniauth.path_prefix
  get "#{oauth_prefix}/:provider/callback" => 'users/oauth#create'
  get "#{oauth_prefix}/failure" => 'users/oauth#failure'
  get "#{oauth_prefix}/:provider" => 'users/oauth#passthru', as: 'provider_auth'
  get oauth_prefix => redirect("#{oauth_prefix}/login")

  # Devise
  devise_prefix = Rails.application.config.auth.devise.path_prefix
  devise_for :users, path: devise_prefix,
    controllers: {registrations: 'users/registrations', sessions: 'users/sessions',
      passwords: 'users/passwords', confirmations: 'users/confirmations', unlocks: 'users/unlocks',
      :omniauth_callbacks => "users/omniauth_callbacks" },
    path_names: { sign_up: 'signup', sign_in: 'login', sign_out: 'logout' }
  devise_scope :user do
    get "#{devise_prefix}/after" => 'users/registrations#after_auth', as: 'user_root'
    get '/check_location' => "users/registrations#check_location"
  end
  get devise_prefix => redirect('/a/signup')


  authenticated :user do
    root 'pages#home', as: :authenticated_root
  end

  # User
  resources :users, path: 'u', only: [:show, :update] do
    resources :authentications, path: 'accounts'
  end

  resources :match, path: 'm', only: :show do
    member do
      get :leaderboard
      get :load_player_card
    end
    resources :contest, path: 'c', only: :show do
      resources :user_teams, path: 'ut' do
        collection do
          get :n
        end
        resources :league, except: :create, path: 'l' do
          collection do
            get :join
            get :join_for_free
            # get :play_with_friends
            # put :invite_friends
          end
        end
      end
    end
  end

  # post '/league' => 'league#create'

  resources :payment, only: :new do
    post :callback,         on: :collection
    post :mock_callback,    on: :collection
    post :mobile_callback,  on: :collection
    post :add_to_wallet,    on: :collection
    get :wallet,            on: :collection
    get :mobile,            on: :collection
    get :success,           on: :collection
    get :failure,           on: :collection
  end

  resources :money_transactions, only: [:create, :update]


  get '/challenge/:invite_code', to: 'league#join_via_code', as: :join_via_code
  get '/ref/:invite_code', to: 'pages#ref', as: :ref

  resource :api_data, only: [] do
    get :sync_to_db
    put :refresh_player_score
  end

  # Static pages
  # get '/hello'             => 'pages#hello'
  get '/privacy_policy'    => 'pages#privacy_policy'
  post '/update_score'     => 'pages#update_score'
  get '/push_live_score'   => 'socket#send_live_score'
  match '/error'           => 'pages#error', via: [:get, :post], as: 'error_page'
  get '/about'             => 'pages#about', as: 'about'
  get '/terms'             => 'pages#terms', as: 'terms'
  get '/rules'             => 'pages#rules', as: 'rules'
  get '/embed'             => 'pages#embed', as: 'embed'
  get '/privacy'           => 'pages#privacy', as: 'privacy'
  get '/disclaimer'           => 'pages#disclaimer', as: 'disclaimer'
  get '/faq'               => 'pages#faq', as: 'faq'
  get '/payments'          => 'pages#payments', as: 'payments'
  get '/how_to_play'       => 'pages#how_to_play', as: 'how_to_play'
  get '/contact'           => 'pages#contact', as: 'contact'
  get '/location_disabled' => 'pages#location_disabled', as: 'location_disabled'
  get '/home'              => 'users#show', as: 'user_home'
  get '/old'               => 'users#past_games', as: 'past_games'
  get '/account'           => 'users#account', as: 'account'
  post 'admin/matches/:id/send_money', to: "admin/matches#send_money", as: 'send_money_admin_match'

  get '/upcomingData' => 'pages#upcomingMatches'
  get '/landing' => 'pages#landing'
  post '/landing' => 'pages#landing'
  get '/intro' => 'pages#intro'

  # Dummy preview pages for testing.
  get '/p/test' => 'pages#test', as: 'test'
  get '/p/email' => 'pages#email' if ENV['ALLOW_EMAIL_PREVIEW'].present?

  get 'robots.:format' => 'robots#index'

  root 'pages#home'

  resources :dashboard, only: [] do
    collection do
      User::AFFILIATE_CODE_STRINGS.keys.each do |affiliate|
        get "#{ affiliate }_users".to_sym
      end
      get :index
    end
  end

end
