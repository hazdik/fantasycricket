require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require "active_model/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "sprockets/railtie"
require 'bourbon'
# require "rails/test_unit/railtie"
# ==== OR ====
# require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module FanGully
  class Application < Rails::Application
    config.middleware.delete Rack::Lock
    config.middleware.insert_before(ActionDispatch::RemoteIp, Rack::Cloudflare::XForwardedFor)

    config.middleware.insert_before 0, "Rack::Cors", :debug => true, :logger => (-> { Rails.logger }) do
      allow do
        origins '*'

        resource '/cors',
          :headers => :any,
          :methods => [:post],
          :credentials => true,
          :max_age => 0

        resource '*',
          :headers => :any,
          :methods => [:get, :post, :delete, :put, :patch, :options, :head],
          :max_age => 0
      end
    end

    # Use sql instead of ruby to support case insensitive indices for postgres
    config.active_record.schema_format = :sql
    # config.active_record.raise_in_transactional_callbacks = true
    # Cache
    # config.cache_store = :memory_store
    # config.cache_store = :mem_cache_store, ENV['MEMCACHE_SERVERS].split(','),
    #   { namespace: Rails.application.config.settings.app_name, expires_in: 30.day, compress: true }
    # Set cache_store the same for all environments to avoid inconsistency issues
    # fantoss-memcache.pglcsj.cfg.aps1.cache.amazonaws.com:11211

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'New Delhi'

    # Disable I18n locale deprecation warning caused by newrelic gem
    # http://stackoverflow.com/questions/20361428/rails-i18n-validation-deprecation-warning
    I18n.enforce_available_locales = true

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Enable faster precompiles
    config.assets.manifest = File.join(Rails.root, 'config', 'assets', "#{Rails.env}.json")
    config.assets.initialize_on_precompile = true

    config.autoload_paths << Rails.root.join('lib')

    # Serve vendor fonts
    config.assets.paths << Rails.root.join('app', 'assets', 'fonts')
    config.assets.paths << Rails.root.join('vendor', 'assets', 'fonts')
    config.assets.precompile += %w( *.svg *.eot *.woff *.ttf *.woff2, ie.js )

    config.assets.precompile += %w( head payment/success.js payment/new.js)
    config.assets.precompile += %w( head.js ie.js emails/inline.css emails/include.css emails/inline2.css typed.js)
    config.assets.precompile += %w( ajax_loader.gif material.min.css humane.js user/user_team_form.js )
    config.assets.precompile += %w( slider_images/*.jpg )
    config.assets.precompile += %w( country_flags/*.jpg )
    config.assets.precompile += %w( team_logos/* )
    config.assets.precompile += %w( robo_avatar/*.jpg )

    config.to_prepare do
      Devise::Mailer.layout Rails.application.config.settings.mail.layout
    end

    config.before_configuration do
      env_file = File.join(Rails.root, 'config', 'secrets', 'production-secret.yml')
      YAML.load(File.open(env_file)).each do |key, value|
        ENV[key.to_s] = value
      end if File.exists?(env_file)
    end

    unless ( Rails.env.test? || Rails.env.development? || Rails.env.staging? )
      log_level = String(ENV['LOG_LEVEL'] || "info").upcase
      config.logger = Logger.new(STDOUT)
      config.logger.level = Logger.const_get(log_level)
      config.log_level = log_level
      config.lograge.enabled = true
      config.lograge.custom_options = lambda do |event|
        payload = { "params" => event.payload[:params].except('controller', 'action') }
        payload.merge(event.payload.select { |k,v| [:ip, :uid, :ua].include?(k) && v.present? })
      end
    end

  end
end

require File.expand_path('../settings', __FILE__)
