
source 'https://rubygems.org'
ruby '2.2.5'

gem 'rails', '~> 4.2.8'
# gem 'mysql2'
# Monitoring
# gem 'airbrake', '~> 3.2.1'         # use with airbrake.io or errbit
# gem 'airbrake_user_attributes'  # use with self-hosted errbit; see config/initializers/airbrake.rb
# gem 'rack-google-analytics'
gem 'rack-cloudflare', github: 'tatey/rack-cloudflare'
gem 'rack-timeout', '~> 0.1.0beta4'
gem 'rack-cors', :require => 'rack/cors'
# gem 'newrelic_rpm'

# Data
gem 'pg'
#gem 'bullet', group: 'development'
#Versioing
gem 'versionist', '~> 1.5.0'
gem 'jbuilder'
gem 'draper', '~> 2.1.0'
#gem 'jquery-datatables-rails'
#gem 'jquery-ui-rails'

# Assets
gem 'sass-rails'
gem 'haml-rails'
gem 'uglifier'
gem 'headjs-rails'
# gem 'simple_form'

gem 'carrierwave', '~> 1.0'
gem 'fog-aws', group: [:production, :staging]
gem 'rails_12factor', group: [:staging]

# gem "asset_sync", "~> 1.0.0"

# Javascript
# gem 'execjs'
gem 'jquery-rails'
# gem 'turbolinks'
# gem 'jquery-turbolinks'
# gem 'nprogress-rails'


#socket
# gem 'websocket-rails'
# gem 'faye-websocket', '0.10.0'

# CoffeeScript
gem 'coffee-rails', '~> 4.2.1'
# Uncomment if node.js is not installed
# gem 'therubyracer', platforms: :ruby, group: [:development, :staging, :production]

# Design
#gem 'bootstrap-sass'
# gem 'ionicons-rails'

# gem 'neat'
# gem 'chosen-rails'
# gem "selectize-rails"
# gem 'material_design_lite-rails'
# gem 'material_design_lite-sass'
# gem 'material_icons'
# gem 'country_select'

# Email
# gem 'premailer-rails'
# gem 'aws-ses', '~> 0.4.4', require: 'aws/ses'

# Authentication
gem 'devise', '~> 3.5.10'
gem 'cancancan', '~> 1.9'
gem 'omniauth', '~> 1.6.1'
gem 'omniauth-facebook', '~> 4.0.0'
# gem 'omniauth-twitter'
# gem 'omniauth-persona'
gem 'omniauth-google-oauth2', '~> 0.4.1'
# gem 'omniauth-linkedin'

# gem "koala", "~> 2.2"

# Admin
gem "administrate", "~> 0.3.0"
# gem 'rails_admin', "~> 0.6.6"
# gem "rails_admin_import", "~> 1.0.0"


# Workers
gem 'sidekiq', '~> 4.2.9'
gem "sidekiq-cron", "~> 0.4.0"
gem 'devise-async', '~> 0.10.2'
# gem 'sinatra', require: false

# Utils
gem 'addressable', '~> 2.5.0'
gem 'settingslogic', '~> 2.0.9'
gem 'meta-tags', '~> 2.4.0'
gem 'exception_notification', '~> 4.2.1'
gem 'kaminari', '~> 0.17.0'
# gem 'lockup'
gem 'concurrent-ruby', '~> 1.0.5', require: 'concurrent'

# HTTP client
gem 'httparty', '~> 0.14.0'
# gem 'rubber', '~>3.1.0'

# logging
gem 'lograge', '~> 0.4.1'

#
# PLATFORM SPECIFIC
#
# OSX
# gem 'rb-fsevent', group: [:development, :test]        # monitor file changes without hammering the disk
# gem 'terminal-notifier-guard', group: [:development]  # notify terminal when specs run
# gem 'terminal-notifier', group: [:development]
# LINUX
# gem 'rb-inotify', :group => [:development, :test]   # monitor file changes without hammering the disk

group :development, :production, :staging do
  # gem 'memcachier'              # heroku add-on for auto config of dalli
  gem 'puma', '~> 3.7.1'
  gem 'dalli-elasticache', '~> 0.2.0'
  gem 'puma_worker_killer', '~> 0.0.7'
end

group :development, :test do
  gem 'dalli'                     # memcached
  # Use spring or zeus
  # gem 'ngrok-tunnel'
  gem 'spring'                  # keep application running in the background
  # gem 'spring-commands-rspec'
  gem 'annotate'
  gem 'mailcatcher'
  # Debugging
  gem 'byebug'
  # gem 'pry'               # ruby 2.0 debugger with built-in pry
  # gem 'pry-rails'               # adds rails specific commands to pry
  # gem 'pry-byebug'              # add debugging commands to pry
  # gem 'pry-stack_explorer'      # navigate call stack
  gem 'derailed'
  gem 'faker'
end
group :development do
  # Docs
  # gem 'sdoc', require: false    # bundle exec rake doc:rails
  gem 'rack-mini-profiler'
  # Errors
  # gem 'better_errors'
  # gem 'binding_of_caller'     # extra features for better_errors
  # gem 'meta_request'          # for rails_panel chrome extension

  # Deployment
  gem 'capistrano', '3.9.0'
  gem 'capistrano-bundler', '~> 1.3.0'
  gem 'capistrano-rails', '~> 1.3.1'
  gem 'capistrano-rvm', '~> 0.1.2'
  # gem 'capistrano-passenger'
  gem 'capistrano-sidekiq'
  gem 'capistrano3-puma', '3.1.1'

  # gem 'dotenv-rails'
  # Guard
  # gem 'guard-rspec'
  # # gem 'guard-livereload'
  # # gem 'rack-livereload'
  # gem 'schema_plus'
  gem 'bourbon', '~> 4.2', '>= 4.2.6'
  gem 'rails-erd', require: false, group: :development
end
