if @user_teams_data.present?
  json.responseCode 200
  json.responseMessage "Leaderboards result"
  json.user_teams @user_teams_data
  json.current_user_teams @current_user_teams_data
else
  json.responseCode 404
  json.responseMessage "No users have joined this match."
end
