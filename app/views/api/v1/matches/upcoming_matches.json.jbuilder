if @upcoming_matches.present?
	json.responseCode 200
	json.responseMessage "Matches found"
	json.matches @upcoming_matches.map{|m|  m.attributes.merge(
		#coming soon
		is_upcoming: !(m.has_squad_data?) ,
		is_team_present: ( @current_user.blank? ? false : (m.user_ids.include?(@current_user.id.to_i)) ),
		teams: {
			home_team: {name: m.home_team.name , flag_path: m.home_team.flag_path , short_name: m.home_team.short_name },
			away_team: {name: m.away_team.name , flag_path: m.away_team.flag_path , short_name: m.away_team.short_name }
			},
		playing_members: {
			count: m.user_teams.count + m.id + rand(100),
			images: m.user_playing_imgs
			},
		title_color: m.title_color)
	}

else
	json.responseCode 404
	json.responseMessage "No upcoming matches."
end
