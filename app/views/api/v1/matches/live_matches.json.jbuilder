if @live_matches.present?
	if @current_user.present?
		json.responseCode 200
		json.responseMessage "Live matches found"
		json.matches @live_matches.map{|m|  m.attributes.merge(
			is_live: true,
			is_team_present: m.user_ids.include?(@current_user.id.to_i) ,
			teams: {
				home_team: {name: m.home_team.name , flag_path: m.home_team.flag_path , short_name: m.home_team.short_name},
				away_team: {name: m.away_team.name , flag_path: m.away_team.flag_path , short_name: m.away_team.short_name}
				},

			playing_members: {
				count: m.user_teams.count + m.id + rand(100),
				images: m.user_playing_imgs
				},
			title_color: m.title_color)
		}
	else
		json.responseCode 200
		json.responseMessage "Live matches found"
		json.matches @live_matches.map{|m|  m.attributes.merge(
			is_live: true,
			is_team_present: false ,
			teams: {
				home_team: {name: m.home_team.name , flag_path: m.home_team.flag_path , short_name: m.home_team.short_name},
				away_team: {name: m.away_team.name , flag_path: m.away_team.flag_path , short_name: m.away_team.short_name}
				},

			playing_members: {
				count: m.user_teams.count + m.id + rand(100),
				images: m.user_playing_imgs
				},
			title_color: m.title_color)
		}
	end
else
	json.responseCode 404
	json.responseMessage "No live matches."
end
