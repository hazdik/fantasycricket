if @players.present?
	json.responseCode 200
	json.responseMessage "Players fetched successfully."
	json.contest_id @contest_id
	json.match_data @match.attributes.merge(
		teams: {
			home_team: {name: @match.home_team.name , flag_path: @match.home_team.flag_path , short_name: @match.home_team.short_name },
			away_team: {name: @match.away_team.name , flag_path: @match.away_team.flag_path , short_name: @match.away_team.short_name }
			},
			title_color: @match.title_color
		)

	json.players @players.as_json(except: [:created_at , :updated_at])
else
	json.responseCode 401
	json.responseMessage "Players are suiting up!! wait until Admin updates the data."
	json.players []
end
