if @user_teams.present?
	json.responseCode 200
	json.responseMessage 'User team found'

	user_teams_response = []

	@user_teams.each do |team|
		user_teams_response << team.attributes.merge(
			team_members: team.players ,
			league: team.league.present? ? team.league.attributes.merge(
				members: team.league.user_teams.sort_by{|obj| -obj.totalscore}.map do |user_team|
					user_team.user.as_json(only: [:first_name, :username, :phone, :image_url]).merge(
						user_team: user_team,
						players: (
							if @match.is_live? || (user_team.user.id == current_user.id) || @match.completed?
								user_team.players.map do |player|
									player.as_json.merge(player_score: player.player_scores.where(match_id: user_team.match_id).first)
								end
							else
								nil
							end
						)
					)
				end
			)
			:
			{members: {  players: []} }
		)
	end

	json.user_teams user_teams_response
	json.contest_id @contest_id
	json.match_details(@match.attributes.merge(
		{
			teams: {
			home_team: {name: @match.home_team.name , flag_path: @match.home_team.flag_path , short_name: @match.home_team.short_name },
			away_team: {name: @match.away_team.name , flag_path: @match.away_team.flag_path , short_name: @match.away_team.short_name }
			},
			description: "#{ @match.tournament.name } | #{ @match.team_names } | #{ @match.title }"
		}
	))

else
	json.responseCode 204
	json.responseMessage 'No user teams present.'
end
