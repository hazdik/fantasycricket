json.responseCode 200
json.responseMessage "Team fetched successfully."
json.user_team @user_team.attributes.merge(player_ids: @user_team.players.pluck(:id))
json.match_players @user_team.match.all_batsman
