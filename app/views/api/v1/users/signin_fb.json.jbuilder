if @user
	json.responseCode  200
	json.responseMessage  "Signed in successfully!"
	json.user current_user.as_json(only: [:id, :authentication_token, :image_url, :email, :phone, :username, :wallet_balance])
else
	json.responseCode 404
	json.responseMessage "User Not found, Please go through signup process."
end
