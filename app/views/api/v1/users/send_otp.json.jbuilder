if @response["responseCode"] == "200"
	# success
	json.responseCode 200
	json.responseMessage "Otp sent"
	json.response @response.as_json(only: ["verificationId", "mobileNumber" , "responseCode"]) 
elsif @response["responseCode"] == "506"
	# Pending 
	json.responseCode 506
	json.responseMessage "Request already pending, please enter your recent OTP."
	json.response @response.as_json(only: ["verificationId" ,"errorMessage" , "responseCode"])
elsif @response["responseCode"] == "504"
	# wrong phone format error
	json.responseCode 501
	json.responseMessage "Phone format error!"
	json.response @response.as_json(only: ["errorMessage" , "responseCode"])
else
	# uncertain error/timeout
	json.responseCode 404
	json.responseMessage "Unknown Server Error!"
	json.response :responseCode => 500 , :responseMessage => "Oops!! Something went wrong."
end