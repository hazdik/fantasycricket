if @joined_league
	json.responseCode 200
	json.responseMessage "Successfully! joined the league."
  json.token @user_transaction.user_team.try(:token)
elsif @wallet_insufficient
  json.responseCode 400
  json.responseMessage "Wallet balance insufficient! Recharge and play."
else
	json.responseCode 402
	json.responseMessage "redirecting to Paytm..."
  json.link "https://web.fantoss.com/payment/mobile?transaction_id=#{@user_transaction.token}&gateway=1"
  json.mock_link "https://web.fantoss.com/payment/mobile?transaction_id=#{@user_transaction.token}&gateway=1&mode=test"
	json.balance_amount @user_transaction.balance_amount
end
