class NotificationSchedularWorker

  include Sidekiq::Worker
  sidekiq_options :queue => :notification_schedular_worker, :retry => true, :backtrace => true

  def perform
    NotificationSchedularService.schedule
  end

end
