class MatchWorker

  include Sidekiq::Worker

  def perform(method, id, options = {})
    options = HashWithIndifferentAccess.new options
    match = ::Match.find(id)
    if options[:send_options]
      match.send(method, options)
    else
      match.send(method)
    end
  end

end
