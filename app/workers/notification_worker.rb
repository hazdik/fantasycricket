class NotificationWorker
  include Sidekiq::Worker

  def perform(options)
    options = HashWithIndifferentAccess.new options
    NotificationService.new(
      recipient: options[:recipient],
      recipient_id: options[:recipient_id],
      content_plain: options[:content_plain],
      content_html: options[:content_html],
    )._send(
      channels: options[:channels],
      mailer: options[:mailer_options][:mailer],
      mail_method: options[:mailer_options][:mail_method],
      mail_params: options[:mailer_options][:mail_params],
      sms_type: options[:sms_type]
    )
  end
end
