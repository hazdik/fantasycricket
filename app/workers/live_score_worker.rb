class LiveScoreWorker

  include Sidekiq::Worker
  sidekiq_options :retry => false

  def perform
    return nil if Match.ongoing.blank?

    Match.ongoing.each do |match|
      # live_score_response = ScoreAPI.getLiveScore(match.api_match_id)["query"]["results"] if match.api_match_id.present?
      # WebsocketRails[:update].trigger(:update, live_score_response) if live_score_response.present?
    end
  end

end
