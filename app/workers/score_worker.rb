class ScoreWorker

  include Sidekiq::Worker
  sidekiq_options :queue => :score_worker, :retry => true, :backtrace => true

  def perform
    return nil if Match.ongoing.blank?

#Yahoo Score Syncing
    Match.ongoing.where(api_provider: nil).each do |match|
      live_score_response = ScoreAPI.getLiveScore(match.api_match_id)["query"]["results"] if match.api_match_id.present?

      if live_score_response.present?
        ::ApiLiveScoreService.new(live_score_response).call
        case live_score_response['Scorecard'].try(:[], 'result').try(:[], 'r').to_i
          when 0
            # Match in Progress
            # Update match end time if required
            if (match.end_time - Time.current) <= 15.minutes
              match.update(end_time: (match.end_time + 1.hour))
            end
          when 1..3
            # 1 - Match Ended with Result
            # 2 - Match Draw
            # 3 - Match Tie
            match.update(end_time: Time.current)

            # Schedule league worker results task
            #   DL Method result - nullify leagues ########## NO NULLIFYING LEAGUE ON DL NOW ##########
            # if live_score_response['Scorecard']['result'].try(:[], 'isdl').to_s == '1'
            #   match.process_league_rankings({ send_options: true, nullify_league: true })
            # else
              match.process_league_rankings
            # end
          when 4..6
            # 4 - Match Abandoned
            # 5 - Match Cancelled
            # 6 - Match Postponed

            match.update(end_time: Time.current)

            # Refund Amount by nullifying leagues
            match.process_league_rankings({ send_options: true, nullify_league: true })
        end
      end

    end

#Goalserve Score Syncing
    goalserve_matches = Match.ongoing.where(api_provider: 'goalserve').pluck(:id)
    if goalserve_matches.present?
      CricketApi::LiveScore.sync(api_provider: :goalserve, match_ids: goalserve_matches)
    end

#ScoresPro Score Syncing
    scoresPro_matches = Match.ongoing.where(api_provider: 'scorespro').pluck(:id)
    if scoresPro_matches.present?
      ScoresPro::LiveScoreScoresPro.sync(api_provider: :scorespro, match_ids: scoresPro_matches)
    end
  end

end
