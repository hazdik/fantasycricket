class SnapshotWorker
  include Sidekiq::Worker
  sidekiq_options :queue => :snapshot_worker, :retry => true, :backtrace => true

  def perform
    ActiveRecord::Base.uncached do
      current_blocked_users = User.where(id: MetaInfo.blocked_users.pluck(:identifier)).pluck(:id)
      Wallet.joins(:user).where('users.last_sign_in_at >= ?', Time.current - 40.days).order(:user_id).find_each(batch_size: 1000).each do |wallet|

        user = wallet.user
        first_wallet_snap = wallet.wallet_snaps.first

        u_txns = user.user_transactions.completed.where('created_at > ?', first_wallet_snap.created_at)
        w_txns = user.wallet_transactions.where('created_at > ?', first_wallet_snap.created_at)

        all_utxn_debit = u_txns.where(transaction_category: ['join_league', 'withdraw_to_bank', 'deduction', 'referral_expiry']).sum(:total_amount)
        all_utxn_credit = u_txns.where(transaction_category: ['match_win', 'tie_refund', 'add_to_wallet', 'referral', 'refund', 'coupon_refund']).sum(:balance_amount)

        all_wtxn_debit = w_txns.where(transaction_type: 'debit').sum(:amount)
        all_wtxn_credit = w_txns.where(transaction_type: 'credit').sum(:amount)

        ## CHECK CONDITIONS
        #1
        if all_utxn_credit != all_wtxn_credit
          # Block user
          MetaInfo.create(name: 'blocked_user', identifier: user.id, info: { event: 'all_utxn_credit != all_wtxn_credit' })
          # Notify Admin
          User.notify_admin
        end
        #2
        if all_utxn_debit != all_wtxn_debit
          # Block user
          MetaInfo.create(name: 'blocked_user', identifier: user.id, info: { event: 'all_utxn_debit != all_wtxn_debit' })
          # Notify Admin
          User.notify_admin
        end
        #3
        if (
            first_wallet_snap.main_balance +
            first_wallet_snap.referral_balance +
            all_utxn_credit - all_utxn_debit
          ) != (
            wallet.main_balance + wallet.referral_balance
          )
          unless current_blocked_users.include?(user.id)
            # Block user
            MetaInfo.create(name: 'blocked_user', identifier: user.id, info: { event: 'first_wallet_snap + credit - debit != wallet' })
            # Notify Admin
            User.notify_admin
          end
        end

        begin
          wallet_snap = wallet.wallet_snaps.create!(
            main_balance: wallet.main_balance,
            referral_balance: wallet.referral_balance,
            user: wallet.user
          )
        rescue Exception => e
          # Notify Admin
          User.notify_admin({details: "Wallet Snaps FAILURE: #{wallet.id}, #{Time.current}, #{ e.as_json }"})
        end
      end
    end

  end
end
