module DynamicLeagueAllocation
  extend ActiveSupport::Concern

  def do_reordering
    case limit
    when 2 then handle_allocation_for_two_teams
    when 4 then handle_allocation_for_four_teams
    when 8 then handle_allocation_for_eight_teams
    end
  end

  module ClassMethods
    def dummy_user_account_id
      [6506, 2071].sample
    end
    def user_team_account(mid)
      User.first.user_teams.find_by_match_id(mid) || User.find(5844).user_teams.find_by_match_id(mid)
    end
  end

  private
    def handle_allocation_for_two_teams
      case joined
      when 1 then add_another_random_team
      when 2 then do_nothing
      end
    end

    def handle_allocation_for_four_teams
      case joined
      when 1 then add_another_random_team; demote_league(2)
      when 2 then demote_league(2)
      when 3 then add_another_random_team
      when 4 then do_nothing
      end
    end

    def handle_allocation_for_eight_teams
      case joined
      when 1 then add_another_random_team; demote_league(2)
      when 2 then demote_league(2)
      when 3 then add_another_random_team; demote_league(4)
      when 4 then demote_league(4)
      when 5 then add_another_random_team; split_league([4,2])
      when 6 then split_league([4,2])
      when 7 then add_another_random_team
      when 8 then do_nothing
      end
    end

    def add_another_random_team
      existing_team = self.class.user_team_account(match.id)
      if existing_team.present?
        randomly_selected_players = existing_team.players
        captain_id = existing_team.captain_id
      else
        randomly_selected_players = match.all_batsmen.sample(5)
        captain_id = randomly_selected_players.sample.id
      end
      _user_team = match.user_teams.build(contest_id: contest_id,
        league_id: id, user_id: self.class.dummy_user_account_id, captain_id: captain_id)
      _user_team.players = randomly_selected_players
      _user_team.save(validate: false)
      self.user_teams << _user_team
    end

    def demote_league(league_limit)
      update_column(:limit, league_limit)
      notify_about_demotion(self)
    end

    def split_league(league_types = [])
      league_types.each do |league_type|
        _league = League.create(limit: league_type,
          match_id: match_id,
          prize_money: prize_money,
          entry_fee: entry_fee,
          name: "#{name}/#{league_type}")
        user_teams(true).limit(league_type).update_all(league_id: _league.id)
        _league.user_teams << user_teams
        League.reset_counters(_league.id, :user_teams)
        notify_about_demotion(_league)
      end
      self.destroy
    end

    def do_nothing
      Rails.logger.info 'League is filled and we dont need to anything.'
    end

    def notify_about_demotion(league)
      LeagueMailer.notify_about_demotion(league).deliver_now
    end
end
