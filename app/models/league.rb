# == Schema Information
#
# Table name: leagues
#
#  id                  :integer          not null, primary key
#  name                :string
#  match_id            :integer
#  limit               :integer          default(0)
#  is_live             :boolean          default(FALSE)
#  prize_money         :integer          default(0)
#  is_multiple_allowed :boolean          default(FALSE)
#  is_fake             :boolean          default(FALSE)
#  fake_size           :integer          default(0)
#  created_at          :datetime
#  updated_at          :datetime
#  entry_fee           :integer
#  contest_id          :integer
#  is_private          :boolean          default(FALSE)
#  is_tied             :boolean          default(FALSE)
#  user_teams_count    :integer          default(0)
#  invite_code         :string
#  winner_ranks        :text             default([]), is an Array
#  loser_ranks         :text             default([]), is an Array
#  tied_team_ranks     :text             default([]), is an Array
#

class League < ActiveRecord::Base
  include DynamicLeagueAllocation
  include RankingAllocation

  # Constants
  ENTRY_FEE_ENUM    = %w( 10 25 )
  PRIZE_ENUM        = %w( 20 50 )
  PAID_LEAGUE_ENUM  = %w( 10 25 )
  PRIZE_AGAINST_FEE = { '10': 20, '25': 50 }
  PRIZE_MONEY_HASH  = { '10' => 20, '25' => 50 }

  #Odd League Details
  ODD_ENTRY_FEE_ENUM    = %w( 15 50  )
  ODD_PRIZE_ENUM        = %w( 45 150 )
  ODD_PAID_LEAGUE_ENUM  = %w( 15 50 )
  ODD_PRIZE_AGAINST_FEE = { '15': 45, '50': 150 }
  ODD_PRIZE_MONEY_HASH  = { '15' => 45, '50' => 150 }

  EVEN_LEAGUE_SIZE = [ '2' ]
  ODD_LEAGUE_SIZE = [ '3' ]
  LEAGUE_SIZE       = EVEN_LEAGUE_SIZE + ODD_LEAGUE_SIZE
  MIN_ENTRY_FEE_FOR_REFERRAL_CREDIT = 10
  MIN_ENTRY_FEE_FOR_PARENT_REFERRAL_CREDIT = 10

  ALL_ENTRY_FEE_ENUM = %w( 10 15 25 50 )

  # Attrs
  attr_reader :certain_winner_ranks, :certain_loser_ranks, :refundable_ranks

  # Scopes
  scope :free, -> { where(prize_money: 0) }
  scope :paid, -> { where.not(prize_money: 0) }
  scope :of_entry, ->(e_fee) { where(entry_fee: e_fee) }
  scope :of_prize, ->(p_money) { where('prize_money = ?', p_money) }
  scope :vacant, -> { where('leagues.limit > leagues.user_teams_count').order(entry_fee: :desc) }
  scope :referral_eligible, -> { where('entry_fee >= ?', MIN_ENTRY_FEE_FOR_REFERRAL_CREDIT) }
  scope :even, -> { where(league_type: ['even', nil]) }
  scope :odd, -> { where(league_type: 'odd') }

  # Associations
  has_many :user_teams
  has_many :user_transactions
  has_many :challenges
  belongs_to :match
  belongs_to :contest
  has_many :users, through: :user_teams
  has_many :users_coupons
  has_many :coupons, through: :users_coupons

  #Validations
  validates :name, :match_id, :limit, :prize_money, :entry_fee, presence: true, unless: -> { is_private? }
  validates :entry_fee, inclusion: { in: (ALL_ENTRY_FEE_ENUM  + ALL_ENTRY_FEE_ENUM.map(&:to_i)) }
  validate :entry_fee_and_prize_money_pair_correct?
  # We are not setting the above validation for now as we are rectifying data by ourselves as per current implementation.

  before_create :set_invite_code
  # before_create :ensure_right_prize_money_for_entry_fee

  # default_scope { order('joined DESC, fake_size ASC, created_at ASC') }

  class << self
    def from_transaction(user_transaction)
      transaction do
        if user_transaction.transaction_category == "join_league"
          return user_transaction.league if user_transaction.league.present?
          params = user_transaction.params

          if params["league_id"].present? || params["user_team_token"].present?
            _league = League.find_by(id: params["league_id"]) || UserTeam.find_by(token: params['user_team_token']).try(:league)
            _league = load_unique_league_or_create(user_transaction) if _league.spots_left.zero?
          else
            _league = load_unique_league_or_create(user_transaction)
          end

          _ut = UserTeam.find(params['user_team_id'])
          # _ut.league_id = _league.id

          # Updating user_team with money_usage_details
          _ut.total_wallet_balance_used = params.try(:[], 'total_wallet_balance_used').to_f
          _ut.referral_wallet_balance_used = params.try(:[], 'referral_wallet_balance_used').to_f
          _ut.main_wallet_balance_used = params.try(:[], 'main_wallet_balance_used').to_f
          _ut.save(validate: false)

          user_transaction.league_id = _league.id
          user_transaction.params[:league_id] = _league.id
          user_transaction.state = "completed"
          user_transaction.save!

          consume_coupon(params['users_coupon_id'], _league.id, user_transaction.id) if params['users_coupon_id']
          _league.notify_user(user_transaction)
          # user_transaction.notify_admin_about_successful_payment if _league.entry_fee!=0
          _league
        else
          user_transaction.state = "completed"
          user_transaction.save!
          _league
          #todo: Notify User
        end
      end
    end

    def available(user_transaction)
      params = user_transaction.params
      leagues = user_transaction.match.leagues
      .where({
        limit: params['member'],
        entry_fee: params['entry'],
        prize_money: params['prize'],
        contest_id: params['contest_id']
      })
      .where("leagues.user_teams_count < leagues.limit")

      leagues.select{|l| l if !l.user_ids.include?(user_transaction.user_id) } if leagues.present?
    end

    private
      def consume_coupon(users_coupon_id, league_id, user_transaction_id)
        users_coupon = UsersCoupon.applied.find_by(id: users_coupon_id)
        users_coupon.consume!(league_id, user_transaction_id) if users_coupon
      end

      def load_league(params)
        l_name = "#{params['entry']}/#{params['prize']}"
        l_name = "Free" if params['entry']=='0'
        create(league_params(params).merge(name: l_name ))
        # find_by(league_params(params)) || create(league_params(params).merge(name: l_name ))
      end

      def load_unique_league_or_create(user_transaction)
        params = user_transaction.params
        if params['is_private'].to_s == 'true'
          return load_league(params)
        end
        leagues = user_transaction.match.leagues
          .where({
            limit: params['member'],
            entry_fee: params['entry'],
            prize_money: params['prize'],
            contest_id: params['contest_id'],
            is_private: params['is_private'].presence
          })
          .where("leagues.user_teams_count < leagues.limit")

        leagues = leagues.select{|l| l if !l.user_ids.include?(user_transaction.user_id) } if leagues.present?
        leagues.present? ? leagues.first : load_league(params)

      end

      def league_params(params)
        if EVEN_LEAGUE_SIZE.include?(params['member'].to_s)
          league_type = "even"
        elsif ODD_LEAGUE_SIZE.include?(params['member'].to_s)
          league_type = "odd"
        else
          league_type = 'invalid_league'
        end
        {
          match_id: params['match_id'],
          limit: params['member'],
          entry_fee: params['entry'],
          prize_money: params['prize'],
          contest_id: params['contest_id'],
          league_type: league_type,
          is_private: params['is_private'].presence
        }
      end
  end

  def entry_fee_and_prize_money_pair_correct?
    if self.league_type == "odd"
      unless self.prize_money.to_i == ODD_PRIZE_AGAINST_FEE[self.entry_fee.to_s.to_sym]
        errors.add(:base, 'Entry Fee and Prize Money pair is not valid!')
      end
    elsif self.league_type == 'even'
      unless self.prize_money.to_i == PRIZE_AGAINST_FEE[self.entry_fee.to_s.to_sym]
        errors.add(:base, 'Entry Fee and Prize Money pair is not valid!')
      end
    else
      errors.add(:base, 'Invalid league!')
    end
  end

  def ensure_right_prize_money_for_entry_fee
    self.prize_money = PRIZE_AGAINST_FEE[self.entry_fee.to_s]
  end

  ## Defines methods as per leagues type, i.e.:
  #    is_of_15?
  #    is_of_50?
  #    is_of_100?
  #    is_of_250?
  #    is_of_500?

  PAID_LEAGUE_ENUM.each do |e_fee|
    define_method "is_of_#{ e_fee }?" do
      self.entry_fee == e_fee.to_i
    end
  end



  def is_free?
    entry_fee <= 0
  end

  def is_paid?
    !is_free?
  end

  # Leagues on which referral money is invalid
  def is_referral_free?
    entry_fee < MIN_ENTRY_FEE_FOR_REFERRAL_CREDIT
  end

  def is_referral_eligible?
    entry_fee >= MIN_ENTRY_FEE_FOR_PARENT_REFERRAL_CREDIT
  end

  alias_method :is_free, :is_free?
  alias_method :is_paid, :is_paid?

  def users_playing_imgs
    users.limit(8).pluck(:image_url).uniq
  end

  def instamojo_link
    # embed=form
    if contest.answer_type=='batsman'
      return "http://imojo.in/13jycz/?embed=form&data_hidden=data_Field_90582&data_Field_90582="
    end
    if contest.answer_type=='bowler'
      return "http://imojo.in/vkquq/?embed=form&data_hidden=data_Field_63837&data_Field_63837="
    end
  end

  def open?
    !is_closed
  end

  def is_vacant?
    spots_left > 0
  end

  def is_closed
    spots_left.zero? || match.ongoing? || match.completed?
  end

  def spots_left
    limit - user_teams.count
  end

  def all_ranks
    @all_ranks ||= begin
      _all_ranks = user_teams.order(:rank).pluck(:rank)
      if _all_ranks == [0]
        [0] * limit
      else
        _all_ranks
      end
    end
  end

  def tied_ranks
    @tied_ranks ||= all_ranks.select{|rank| all_ranks.count(rank) > 1 }.uniq
  end

  def eligible_winner_ranks
    if league_type == 'odd'
      @eligible_winner_ranks ||= (all_ranks[0..limit/3 - 1] || [])
    else
      @eligible_winner_ranks ||= (all_ranks[0..limit/2 - 1] || [])
    end
  end

  def eligible_loser_ranks
    if league_type == 'odd'
      @eligible_loser_ranks ||= (all_ranks[limit/3..limit - 1] || [])
    else
      @eligible_loser_ranks ||= (all_ranks[limit/2..limit - 1] || [])
    end
  end

  def process_league_teams_score
    user_teams.each do |ut|
      ut.update_score_from_players_score
    end
  end

  def process_winners_and_losers
    @refundable_ranks, @certain_winner_ranks, @certain_loser_ranks = [], [], []

    ## Processing TIED RANKS for winners and losers (if any)
    tied_ranks.each do |rank|
      if eligible_winner_ranks.include?(rank) && eligible_loser_ranks.include?(rank)
        @refundable_ranks << rank
      elsif eligible_winner_ranks.include?(rank)
        @certain_winner_ranks << rank
      elsif eligible_loser_ranks.include?(rank)
        @certain_loser_ranks << rank
      end
    end

    ## Processing ranks other than tied ranks
    #    Eligible winner ranks processing:
    (eligible_winner_ranks - tied_ranks).each do |rank|
      (certain_winner_ranks << rank) unless tied_ranks.include?(rank)
    end

    #    Eligible losers ranks processing:
    (eligible_loser_ranks - tied_ranks).each do |rank|
      (certain_loser_ranks << rank) unless tied_ranks.include?(rank)
    end

    ## Finalize winners/losers based on special case i.e.
    #    When no. of certain winners exceed no. of certain losers.
    #    It's overall loss for corresponding league to give prize to winners.
    #    We refund amount to every user of that particular league in that case.
    if all_ranks.select { |rank| certain_winner_ranks.include?(rank) }.length >
        all_ranks.select { |rank| certain_loser_ranks.include?(rank) }.length
      @refundable_ranks += certain_winner_ranks
      @certain_winner_ranks = []
      # @certain_loser_ranks = []
    end

    self.update(
      winner_ranks: certain_winner_ranks.to_a,
      loser_ranks: certain_loser_ranks.to_a,
      tied_team_ranks: refundable_ranks.to_a
    )
    user_teams.where(rank: winner_ranks).update_all(has_won: true, has_tied: false)
    user_teams.where(rank: tied_team_ranks).update_all(has_won: false, has_tied: true)
    user_teams.where(rank: loser_ranks).update_all(has_won: false, has_tied: false)
  end

  def eligible_odd_winner_ranks
    @eligible_winner_ranks ||= (all_ranks[0..limit/3 - 1] || [])
  end

  def eligible_odd_loser_ranks
    @eligible_loser_ranks ||= (all_ranks[limit/3..limit - 1] || [])
  end

  def process_odd_winners_and_losers
    @refundable_ranks, @certain_winner_ranks, @certain_loser_ranks = [], [], []

    ## Processing TIED RANKS for winners and losers (if any)
    tied_ranks.each do |rank|
      if eligible_odd_winner_ranks.include?(rank) && eligible_odd_loser_ranks.include?(rank)
        @refundable_ranks << rank
      elsif eligible_odd_winner_ranks.include?(rank)
        @certain_winner_ranks << rank
      elsif eligible_odd_loser_ranks.include?(rank)
        @certain_loser_ranks << rank
      end
    end

    ## Processing ranks other than tied ranks
    #    Eligible winner ranks processing:
    (eligible_odd_winner_ranks - tied_ranks).each do |rank|
      (certain_winner_ranks << rank) unless tied_ranks.include?(rank)
    end

    #    Eligible losers ranks processing:
    (eligible_odd_loser_ranks - tied_ranks).each do |rank|
      (certain_loser_ranks << rank) unless tied_ranks.include?(rank)
    end

    ## Finalize winners/losers based on special case i.e.
    #    When no. of certain winners exceed no. of certain losers.
    #    It's overall loss for corresponding league to give prize to winners.
    #    We refund amount to every user of that particular league in that case.
    if 2*all_ranks.select { |rank| certain_winner_ranks.include?(rank) }.length >
      ( all_ranks.select { |rank| certain_loser_ranks.include?(rank) }.length )
      @refundable_ranks += certain_winner_ranks
      @certain_winner_ranks = []
      # @certain_loser_ranks = []
    end

    self.update(
      winner_ranks: certain_winner_ranks.to_a,
      loser_ranks: certain_loser_ranks.to_a,
      tied_team_ranks: refundable_ranks.to_a
    )
    user_teams.where(rank: winner_ranks).update_all(has_won: true, has_tied: false)
    user_teams.where(rank: tied_team_ranks).update_all(has_won: false, has_tied: true)
    user_teams.where(rank: loser_ranks).update_all(has_won: false, has_tied: false)
  end

  def winners
    if is_vacant?
      @winners ||= []
    else
      # process_winners_and_losers
      @winners ||= user_teams.where(rank: winner_ranks)
    end
  end

  def losers
    if is_vacant?
      @losers ||= []
    else
      # process_winners_and_losers
      @losers ||= user_teams.where(rank: loser_ranks)
    end
  end

  def tied_user_teams
    if is_vacant? || (tied_team_ranks.blank? && winner_ranks.blank?)
      @tied_user_teams ||= user_teams.where(rank: all_ranks.uniq)
    else
      # process_winners_and_losers
      @tied_user_teams ||= user_teams.where(rank: tied_team_ranks)
    end
  end

  def joined
    user_teams_count
  end

  def referral_value
    Rails.application.config.settings.referral_credit
  end

  def notify_user(user_transaction)
    # LeagueMailer.added_to_league(self, user_transaction).deliver_now

    # NotificationWorker.new.perform(
    #   {
    #     channels: (is_free? ? [:email, :push] : nil),
    #     recipient: user_transaction.user,
    #     content_plain: ActionController::Base.new.render_to_string(
    #       template: 'sms_templates/added_to_league.txt',
    #       locals: { '@league': self, '@match': self.match }
    #     ),
    #     mailer_options: {
    #       mailer: 'LeagueMailer',
    #       mail_method: 'added_to_league',
    #       mail_params: [self, user_transaction]
    #     }
    #   }
    # )

    NotificationWorker.perform_at(
      Time.current + 2.seconds,
      {
        channels: (is_free? ? [:email, :push] : nil),
        recipient_id: user_transaction.user_id,
        content_plain: ActionController::Base.new.render_to_string(
          template: 'sms_templates/added_to_league.txt',
          locals: { '@league': self, '@match': self.match }
        ),
        mailer_options: {
          mailer: 'LeagueMailer',
          mail_method: 'added_to_league',
          mail_params: [self.id, user_transaction.id]
        }
      }
    )
  end

  def notify_users_about_free_league
    LeagueMailer.notify_users_about_free_league(self).deliver_now
  end

  def notify_user_teams_about_rankings
    winners.each do |winner|
      # next if winner.user.email.blank?
      # LeagueMailer.notify_user_about_winning(winner, self).deliver_now
      trigger_ranking_notifications(winner.user, 'notify_user_about_winning', [ winner.id, self.id ])
    end

    losers.each do |loser|
      # next if loser.user.email.blank?
      # LeagueMailer.notify_user_about_losing(loser, self).deliver_now
      trigger_ranking_notifications(loser.user, 'notify_user_about_losing', [ loser.id, self.id ])
    end

    tied_user_teams.each do |tied|
      # next if tied.user.email.blank?
      # LeagueMailer.notify_user_about_tie(tied, self).deliver_now
      if tied.rank == 0
        trigger_ranking_notifications(tied.user, 'notify_about_league_refund', [ tied.id, self.id ])
      else
        trigger_ranking_notifications(tied.user, 'notify_user_about_tie', [ tied.id, self.id ])
      end
    end
  end


  def self.process_faulty_leagues(faulty_league_ids)
    f_ids = faulty_league_ids
    f_ids.each do |id|
      league = League.find id
      amount = league.entry_fee
        league.user_teams.where('rank <= ?', league.limit).each do |ut|
          utxn = ut.user_transactions.create(
          params: {purpose: "Tie refund due to wrong number of people in league."},
          user: ut.user,
          match: league.match,
          league: league,
          balance_amount: amount,
          transaction_category: 'tie_refund',
          state: 'completed'
          )
          p utxn.errors if utxn.errors.present?
        end
    end
  end

  def trigger_ranking_notifications(recipient,template_name, mail_params)

    NotificationWorker.perform_at(
      Time.current + 1.minutes,
      {
        recipient_id: recipient.id,
        content_plain: ActionController::Base.new.render_to_string(
          template: "sms_templates/#{ template_name }.txt",
          locals: { '@league': self, '@match': self.match }
        ),
        channels: (is_free? ? [:email, :push] : nil),
        mailer_options: {
          mailer: 'LeagueMailer',
          mail_method: template_name,
          mail_params: mail_params
        }
      }
    )

    #   mail_params: mail_params
    # )
  end

  def set_invite_code
    begin
      self.invite_code = SecureRandom.hex(3)
    end while self.class.where(invite_code: invite_code).exists?
  end

end







# leage tag