# == Schema Information
#
# Table name: matches
#
#  id            :integer          not null, primary key
#  home_team_id  :integer
#  away_team_id  :integer
#  created_at    :datetime
#  updated_at    :datetime
#  start_time    :datetime
#  end_time      :datetime
#  tournament_id :integer
#  is_live       :boolean          default(FALSE)
#  title         :string
#  api_match_id  :string
#  mtype         :string
#  team_names    :string           default("")
#  ms            :string           default("Match is yet to start")
#  api_provider  :string
#

class Match < ActiveRecord::Base
  #Constants
  PAGINATED_LIMIT = 10
  MATCH_FORMATS = {
    test: :test, odi: :odi, t20: :t20, list_a: :list_a, first_class: :first_class
  }
  MATCH_STATUSES = {
    upcoming: :upcoming, finished: :finished, live: :live
  }

  # scope :upcoming, lambda { where("start_time >= ?", Date.today).order("start_time").limit(20) }
  scope :upcoming, lambda { where("start_time >= ? and start_time < ? ", (Time.zone.now + 5.minutes), (Time.zone.now + 5.day)).order("start_time ASC").limit(20) }
  scope :completed, lambda { where("end_time < ?", Time.zone.now).order(end_time: :desc).limit(PAGINATED_LIMIT) }
  scope :recent, lambda { where("end_time < ?", Time.zone.now).order(end_time: :desc).limit(2) } # last 2 match
  scope :next, lambda { upcoming.first }
  scope :starts_within, ->(min) { where("start_time <= ?", (Time.zone.now + min.minutes)).where("start_time >= ?", Time.zone.now) }
  scope :ongoing, lambda { where("start_time <= ? and (? <= end_time OR end_time IS NULL)",(Time.zone.now + 5.minutes),Time.zone.now).order("start_time ASC") }
  # default_scope { includes(:user_teams, :home_team, :away_team, :tournament) }

  has_many :leagues, dependent: :destroy
  has_many :player_scores, dependent: :destroy
  has_many :players, through: :player_scores
  has_many :user_teams, dependent: :destroy
  has_many :users, through: :user_teams
  has_many :user_transactions

  belongs_to :tournament

  belongs_to :home_team, :foreign_key => :home_team_id, :class_name => 'ParticipantTeam'
  belongs_to :away_team, :foreign_key => :away_team_id, :class_name => 'ParticipantTeam'

  validates :home_team_id, :away_team_id, presence: true

  # validates_inclusion_of :start_time, in: ->(g){ (Date.today..Float::INFINITY) } , message: 'should be greater than current_time'
  # validates_inclusion_of :end_time, in: ->(g){ (Date.today..Float::INFINITY) } , message: 'should be greater than current_time'

  # set 3 states
  after_save :add_match_to_queue
  # after_create :process_league_rankings  --TOREMOVE
  after_save :notify_about_ms

  validate :home_and_away_team_validations

  attr_accessor :is_payable

  def self.display_matches
    unique_t_ids = select('DISTINCT ON (matches.tournament_id) matches.tournament_id').
      where("start_time >= ? ", (Time.zone.now + 5.minutes)).pluck(:tournament_id).uniq

    next_matches = []
    unique_t_ids.each do |t_id|
      next_matches += where(tournament_id: t_id).where("start_time >= ?", (Time.zone.now + 5.minutes)).
        order(:start_time).limit(2)
    end

    next_matches += where('start_time >= ?', Time.current).where('start_time < ?', Time.current + 3.days)

    # unless Rails.env.development?
    #   ## Showing more IPL2017 matches for now
    #   # ipl2017_id = 54
    #   # next_matches += Tournament.find(ipl2017_id).matches.upcoming.first(5)
    #   ## TOREMOVE - After IPL2017
    # end

    next_matches.uniq.sort { |m_1, m_2| m_1.start_time <=> m_2.start_time }
  end

  def title_color
    Tournament::TITLE_COLOR[tournament.region.try(:to_sym)] || '#008c00'
  end

  def user_playing_imgs
    users.limit(10).pluck(:image_url).uniq + RandomFbId.render
  end

  def is_live?
    ongoing?
  end

  def default_fake_count
    id + 100
  end

  def ongoing?
    if start_time
      start_time <= Time.zone.now && (end_time.blank? || Time.zone.now <= end_time)
    else
      false
    end
  end

  def starting?
    if start_time
      start_time <= (Time.zone.now + 5.minutes) && (end_time.blank? || end_time > Time.zone.now)
    else
      false
    end
  end

  def completed?
    if end_time
      end_time < Time.zone.now
    else
      false
    end
  end

  def has_squad_data?
    home_team.current_squad.present? &&  away_team.current_squad.present?
  end

  def all_players
    player_ids = home_team.current_squad + away_team.current_squad
    Player.where(id: player_ids)
  end

  def all_batsmen
    all_players.batsman
  end

  def open_leagues
    leagues.where(is_fake: false)
  end

  def all_batsman
    home_team.batsmans + away_team.batsmans
  end

  def all_bowlers
    home_team.bowlers + away_team.bowlers
  end

  def set_team_names
    self.team_names = "#{home_team.name} v/s #{away_team.name}" if team_names.blank?
    self.save(validate: false)
  end

  def display_name
    "#{team_names}, #{title}"
  end

  def match_detail
    title
  end

  def match_start_time
    (start_time - 5.minutes)
    # YYYY/MM/DD hh:mm:ss
  end

  def human_start_time
    start_time.strftime('%a, %b %d, %I:%M %p')
    # YYYY/MM/DD hh:mm:ss
  end

  def match_end_time
    end_time.strftime('%a, %b %d, %I:%M %p')
  end

  def starts_within_one_hour?
    (start_time - Time.zone.now ) <= 1.hour
  end

  def starting_in_5?
    (start_time - Time.zone.now ) <= 5.minutes
  end

  def bid_closed?
    (start_time - Time.zone.now ) <= 5.minutes
  end

  def allocate_league_rankings(options = {})
    leagues.order(entry_fee: :desc).each do |league|
      league.process_rankings(options[:nullify_league])
      if league.league_type == "even" || league.league_type == nil
        league.process_winners_and_losers
      else
        league.process_odd_winners_and_losers
      end
      ## Process Payments
      # if league.is_paid?
      #   if api_provider.blank? || options[:force_pay]
      #     PaymentService.pay_for_league(league.id)
      #   end
      # end
      ## Send notifications
      league.notify_user_teams_about_rankings #unless Rails.env.development?
    end
  end

  def process_leagues
    # process_free_leagues
    process_paid_leagues if Rails.application.config.settings.league_splitting_allowed
    # Expires Referal credits
    expire_stale_referral_credits unless Rails.env.development?
  end

  def create_dummy_team
    create_dummy_free_league_team
  end

  def process_league_rankings(options = {})
    MatchWorker.perform_at(self.end_time + 2.minutes, :allocate_league_rankings, self.id, options)
  end

  def transaction_total
    # user_transactions.where("params -> 'entry' != '0'").where(state: 'completed').sum(:total_amount)
    # uts = user_transactions.where("params -> 'is_' != '0'").where(state: 'completed')
    uts = user_transactions.where("params -> 'entry' != '0'").where(state: 'completed')
    "#{uts.count}, #{uts.sum("(payment_gateway_response -> 'TXNAMOUNT')::float")}"
  end

  def is_paid
    user_transactions.pluck(:transaction_category).uniq.count > 1
  end

  def expire_stale_referral_credits
    ####### Referral expiry is changed to 1 month for now and not 10 matches #######
    # last_valid_match = Match.where('start_time <= ?', Time.current).order(start_time: :desc).limit(ReferralCredit::VALIDITY_AS_PER_MATCH_COUNT).last
    # ReferralCredit.before_match(last_valid_match).non_expired.expire
    ReferralCredit.non_expired.where('created_at < ?', (Time.current - 15.days)).expire
  end

  def queue_for_payments(options = {})
    GenericWorker.perform_at(
      Time.current,
      {
        class_name: 'Match',
        method_name: :process_payments,
        class_method: true,
        instance_arguments: {},
        method_arguments: {
          match_id: options[:match_id],
          match_status: options[:match_status]
        }
      }
    )
  end


  def self.process_payments(options = {})
    # options = {"match_id"=>"136", "match_status"=>"tie"} //reference
    mids = Array.wrap(options['match_id'].to_i)
      #prize_money_hash = League::PRIZE_MONEY_HASH
      mids.each do |mid|
        m = Match.find mid
          # return if (m.user_transactions.pluck(:transaction_category).uniq.count > 1)
          if options["match_status"] == 'win'
            options = { force_pay: true }
          elsif options["match_status"] == 'tie'
            options = { force_pay: true , nullify_league: true} # if Abandoned
          else
            raise Exception.new('Match Status is not there in parameters.')
          end

        faulty_leagues = []
        m.leagues.order(entry_fee: :asc, limit: :desc).each do |league|
          league.process_league_teams_score
          if league.league_type == 'even' || league.league_type == nil
            prize_money_hash = League::PRIZE_MONEY_HASH
          elsif league.league_type == 'odd'
            prize_money_hash = League::ODD_PRIZE_MONEY_HASH
          end
          league.process_rankings(options[:nullify_league])
          league.process_winners_and_losers
          # Process Payments
           if (league.is_paid?)
              if (league.user_teams.count > league.limit)
                faulty_leagues << league.id
                next
              end
              if m.api_provider.blank? || options[:force_pay]
                # WInning amount for for Winners
                if league.winners.present?
                  league.winners.each do |user_team|
                    ut = UserTransaction.create(
                      user: user_team.user,
                      match: league.match,
                      user_team: user_team,
                      league: league,
                      balance_amount: prize_money_hash["#{league.entry_fee}"],
                      transaction_category: 'match_win',
                      state: 'completed'
                    )
                    # p ut.errors if ut.errors.present?
                  end
                end

               # tied_user_ids = []
                ## Tied Refund for Tie users
                if league.tied_user_teams.present?
                  league.tied_user_teams.each do |user_team|

                    user = user_team.user
                    user_coupon = UsersCoupon.find_by(
                      league: league,
                      user: user,
                      user_transaction_id: user_team.user_transactions.where(
                        transaction_category: 'join_league'
                      ).pluck(:id)
                    )

                    if user_coupon
                      # p user_coupon
                      coupon_amount = user_coupon.amount
                      actual_amount = league.entry_fee - coupon_amount
                    else
                      actual_amount = league.entry_fee
                    end
                    ref_bal_used = user_team.referral_wallet_balance_used
                    actual_main_balance_used = actual_amount - ref_bal_used


                    ut = UserTransaction.create(
                      user: user_team.user,
                      match: league.match,
                      user_team: user_team,
                      league: league,
                      balance_amount: actual_main_balance_used,
                      transaction_category: 'tie_refund',
                      state: 'completed'
                    ) if (actual_main_balance_used > 0)

                    if (ref_bal_used > 0) || (actual_main_balance_used == 0)
                      u = user_team.user
                      u.user_transactions.find_or_create_by(
                        match: league.match,
                        # params: {
                        #   purpose: "Refund for #{ user_team.id }"
                        # },
                        user_team: user_team,
                        state: "completed",
                        transaction_category: "referral",
                        credit_amount: ref_bal_used,
                        debit_amount: 0
                      )
                    end
                    # if  ut.errors.present?
                    #   p ut.errors
                    # end
                  end
                end
              end
           end

          league.notify_user_teams_about_rankings
        end
        m.update_column(:is_payment_processed, true)
        League.process_faulty_leagues(faulty_leagues)

      end

  end


  def is_payable
    ['Match Ended', 'Match abandoned', 'Finished', 'Match Abandoned', 'Finish', 'Abandoned', 'Fin', 'Abd'].include?(ms)
  end

  private

    def add_match_to_queue
      if start_time_changed?
        MatchWorker.perform_at(self.start_time - 30.minutes, :process_leagues, self.id)
      end
    end

    def track_live_score
      # ScoreWorker.perform_at(self.start_time - 30.minutes, :process_leagues, self.id)
    end

    def home_and_away_team_validations
      if self.home_team_id == self.away_team_id
        errors.add :base, "Match participant team(s) are similar.It should be different"
      end
      # errors.add(:start_time, "can't be in the past") if !start_time.blank? and start_time.to_date < Date.today
      # errors.add(:end_time, "can't be in the past") if !end_time.blank? and end_time.to_date < Date.today

      errors.add :base, "End_time should be greater than start_time" if self.start_time.present? and self.end_time.present? and ( self.end_time <= self.start_time )
    end

    def process_paid_leagues
      process_even_paid_leagues
      process_odd_paid_leagues
    end

    def process_even_paid_leagues
      League::PAID_LEAGUE_ENUM.each do |entry_fee|
        _old_leagues,_user_teams = [],[]
        # Here we collect all user_teams from VACANT leagues of ENTRY_FEE category
        leagues.vacant.where(entry_fee: entry_fee, league_type: ["even", nil] ).each do |league|
          _user_teams += league.user_teams
          _old_leagues << league
        end

        _user_teams_hash = Hash.new { |hash, key| hash[key] = [] }
        _user_teams.each { |ut| _user_teams_hash[ut.user_id.to_s] << ut }

        # _user_teams = [1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 5, 6, 6, 7, 8, 9, 10, 11]
        # _user_teams_hash = {
        #   "1"=>[1],
        #   "2"=>[2],
        #   "3"=>[3, 3, 3, 3, 3, 3, 3, 3, 3],
        #   "4"=>[4],
        #   "5"=>[5],
        #   "6"=>[6, 6],
        #   "7"=>[7],
        #   "8"=>[8],
        #   "9"=>[9],
        #   "10"=>[10],
        #   "11"=>[11]
        # }

        # Making highest possible size of leagues if possible
        league_sizes = League::EVEN_LEAGUE_SIZE.sort.reverse

        league_sizes.each do |league_size|
          league_size = league_size.to_i

          while _user_teams_hash.size >= league_size
            _league = leagues.create(
              name: "#{ entry_fee }/#{ League::PRIZE_AGAINST_FEE[entry_fee.to_s.to_sym] }",
              limit: league_size,
              entry_fee: entry_fee,
              prize_money: League::PRIZE_AGAINST_FEE[entry_fee.to_s.to_sym],
              contest_id: Contest::TOP_5_CONTEST.id,
              league_type: "even"
            )

            _user_teams_hash.each do |key, value|

              # Taking out user team from each group and putting it into the newly created league
              ut = value.shift

              ut.update_column(:league_id, _league.id)
              ut.user_transactions.update_all(league_id: _league.id)
              ut.delay.notify_about_demotion

              League.reset_counters(_league.id, :user_teams)
              _league.reload
              break if _league.limit == _league.user_teams_count
            end

            _user_teams_hash.delete_if{ |k,v| v.blank? }
          end

        end
        _old_leagues.each do |l|
          UsersCoupon.consumed.where(league: l).each { |uc| uc.update_column(:league_id, uc.user_transaction.league_id) }
          # if l.user_teams.count <= 0
            # Not destroying now
            # l.destroy
          # else
            League.reset_counters(l.id, :user_teams)
          # end
        end

      end
    end

    def process_odd_paid_leagues
      League::ODD_PAID_LEAGUE_ENUM.each do |entry_fee|
        _old_leagues,_user_teams = [],[]
        # Here we collect all user_teams from VACANT leagues of ENTRY_FEE category
        leagues.vacant.where(entry_fee: entry_fee, league_type: "odd").each do |league|
          _user_teams += league.user_teams
          _old_leagues << league
        end

        _user_teams_hash = Hash.new { |hash, key| hash[key] = [] }
        _user_teams.each { |ut| _user_teams_hash[ut.user_id.to_s] << ut }

        # _user_teams = [1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 5, 6, 6, 7, 8, 9, 10, 11]
        # _user_teams_hash = {
        #   "1"=>[1],
        #   "2"=>[2],
        #   "3"=>[3, 3, 3, 3, 3, 3, 3, 3, 3],
        #   "4"=>[4],
        #   "5"=>[5],
        #   "6"=>[6, 6],
        #   "7"=>[7],
        #   "8"=>[8],
        #   "9"=>[9],
        #   "10"=>[10],
        #   "11"=>[11]
        # }

        # Making highest possible size of leagues if possible
        league_sizes = League::ODD_LEAGUE_SIZE.sort.reverse

        league_sizes.each do |league_size|
          league_size = league_size.to_i

          while _user_teams_hash.size >= league_size
            _league = leagues.create(
              name: "#{ entry_fee }/#{ League::ODD_PRIZE_AGAINST_FEE[entry_fee.to_s.to_sym] }",
              limit: league_size,
              entry_fee: entry_fee,
              prize_money: League::ODD_PRIZE_AGAINST_FEE[entry_fee.to_s.to_sym],
              contest_id: Contest::TOP_5_CONTEST.id,
              league_type: "odd"
            )

            _user_teams_hash.each do |key, value|

              # Taking out user team from each group and putting it into the newly created league
              ut = value.shift

              ut.update_column(:league_id, _league.id)
              ut.user_transactions.update_all(league_id: _league.id)
              ut.delay.notify_about_demotion

              League.reset_counters(_league.id, :user_teams)
              _league.reload
              break if _league.limit == _league.user_teams_count
            end

            _user_teams_hash.delete_if{ |k,v| v.blank? }
          end

        end
        _old_leagues.each do |l|
          UsersCoupon.consumed.where(league: l).each { |uc| uc.update_column(:league_id, uc.user_transaction.league_id) }
          # if l.user_teams.count <= 0
            # Not destroying now
            # l.destroy
          # else
            League.reset_counters(l.id, :user_teams)
          # end
        end

      end
    end

    def process_free_leagues
      create_dummy_free_league_team if user_teams.count.odd?
      #user_teams.joins(:league).where('user_teams.league_id=?', nil).where('leagues.league_type=?', 'odd')
      #free_league_user_teams = user_teams.where(league_id: nil).where(league_type: "even")
      free_league_user_teams = user_teams.joins(:league).where('user_teams.league_id=?', nil).where('leagues.league_type=? OR leagues.league_type IS NULL', 'even')

      split_into_leagues(free_league_user_teams)

      process_odd_free_leagues
    end

    def create_dummy_free_league_team
      existing_team = League.user_team_account(self.id)
      if existing_team.present?
        randomly_selected_players = existing_team.players
        captain_id = existing_team.captain_id
      else
        randomly_selected_players = all_batsmen.sample(5)
        captain_id = randomly_selected_players.sample.id
      end
      _ut = user_teams.build(contest_id: Contest::TOP_5_CONTEST.id, user_id: League.dummy_user_account_id, captain_id: captain_id)
      _ut.players = randomly_selected_players
      _ut.save(validate: false)
    end

    def split_into_leagues(free_league_user_teams)
      split_into_magic_numbers(free_league_user_teams.count).each do |limit, number_of_occurences|
        number_of_occurences.times do |i|
          _league = League.create(limit: limit, match_id: id, prize_money: 0, entry_fee: 0,
            name: "Free")

          free_league_user_teams.reload
                                .limit(limit)
                                .update_all(league_id: _league.id)
          _league.notify_users_about_free_league
          League.reset_counters(_league.id, :user_teams)
        end
      end
    end

    def split_into_magic_numbers(no_of_entries)
      eight_member_leagues, four_member_league_remainder = no_of_entries.divmod(8)
      four_member_leagues, two_member_league_remainder = four_member_league_remainder.divmod(4)
      two_member_leagues = two_member_league_remainder.div(2)

      {
        8 => eight_member_leagues,
        4 => four_member_leagues,
        2 => two_member_leagues
      }
    end

    def process_odd_free_leagues
      free_league_user_teams = user_teams.joins(:league).where('user_teams.league_id=?', nil).where('leagues.league_type=?', 'odd')
      split_into_odd_leagues(free_league_user_teams)
    end

    def split_into_odd_leagues(free_league_user_teams)
      split_into_magic_numbers_odd(free_league_user_teams.count).each do |limit, number_of_occurences|
        number_of_occurences.times do |i|
          _league = League.create(limit: limit, match_id: id, prize_money: 0, entry_fee: 0,
            name: "Free")

          free_league_user_teams.reload
                                .limit(limit)
                                .update_all(league_id: _league.id)
          _league.notify_users_about_free_league
          League.reset_counters(_league.id, :user_teams)
        end
      end
    end

    def split_into_magic_numbers_odd(no_of_entries)
      nine_member_leagues, six_member_league_remainder = no_of_entries.divmod(9)
      six_member_leagues, three_member_league_remainder = six_member_league_remainder.divmod(6)
      three_member_leagues = three_member_league_remainder.div(3)

      {
        9 => nine_member_leagues,
        6 => six_member_leagues,
        3 => three_member_leagues
      }
    end


# Match Status Notification
    def notify_about_ms
      # api_cred = {
      #   user_id: ApplicationYML['env']['SMS_NOTIFICATION']['user_id'],
      #   sender_id: ApplicationYML['env']['SMS_NOTIFICATION']['sender_id'],
      #   password: ApplicationYML['env']['SMS_NOTIFICATION']['password']
      # }
      # if ms_changed? && ["Match Ended","Match abandoned", "Finished", "Match Abandoned", "Finish", "Abandoned", "Fin"].include?(ms)
      #   recipients = ['8765587934', '7503578995', '8802053264']
      #   content_plain = "Recently  #{team_names} match status has been changed to #{ms}. Please remind Concerned Person ASAP for payments."
      #   recipients.each do |phone|
      #     HTTParty.get('http://49.50.77.216/API/SMSHttp.aspx',
      #         query: {
      #           UserId: api_cred[:user_id],
      #           pwd: api_cred[:password],
      #           SenderId: api_cred[:sender_id],
      #           Message: content_plain,
      #           Contacts: phone
      #         }
      #       )
      #   end
      # end
    end
end
