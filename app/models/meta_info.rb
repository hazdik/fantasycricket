class MetaInfo < ActiveRecord::Base

  # Validations
  validates :name, presence: true
  # validates :name, uniqueness: true

  # Scopes
  scope :blocked_users, -> { where(name: 'blocked_user', block: true) }
end
