# == Schema Information
#
# Table name: referral_credits
#
#  id               :integer          not null, primary key
#  amount_used      :float            default(0.0)
#  amount_remaining :float            default(0.0)
#  expired          :boolean          default(FALSE)
#  wallet_id        :integer
#  expired_at       :datetime
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class ReferralCredit < ActiveRecord::Base

  # Constants
  VALIDITY_AS_PER_MATCH_COUNT = 10

  # Associations
  belongs_to :wallet
  delegate :user, :user_id, to: :wallet

  # Validations
  validates :wallet, presence: true
  validates :amount_used, :amount_remaining, numericality: true #{ greater_than_or_equal_to: 0.0 }

  # Callbacks
  after_create :credit_wallet_balance

  # Scopes
  scope :expired, -> { where(expired: true) }
  scope :redeemed, -> { where('amount_remaining <= 0') }
  scope :non_expired, -> { where(expired: false) }
  scope :available, -> { non_expired.where('amount_remaining > 0') }
  scope :before_match, ->(m) { where('created_at < ?', m.start_time) }

  # Alias Methods
  alias_attribute :amount, :amount_remaining

  # Class Methods
  def self.expire
    all.map(&:expire)
  end

  def self.unexpire
    all.map(&:unexpire)
  end

  # Public methods
  def expire
    transaction do
      puts 'Expiring ...'
      update(expired: true, expired_at: Time.current)
      puts 'Debitting from wallet after referral expire ...'
      # Create User Transactions
      user.user_transactions.create!(
        params: {
          purpose: "referral_expiry",
          associated_id: self.id
        },
        state: "completed",
        transaction_category: "referral_expiry",
        credit_amount: 0,
        debit_amount: amount
      )
      puts 'Wallet updated ...'
    end
  end

  def unexpire
    transaction do
      puts 'Un-Expiring ...'
      update(expired: false, expired_at: nil)
      puts 'Creditting to wallet after referral un-expire ...'
      wallet.update!(referral_balance: wallet.referral_balance + amount)
      puts 'Wallet updated ...'
    end
  end

  def redeem(value)
    transaction do
      update(amount_remaining: amount_remaining - value, amount_used: value)
      wallet.update!(referral_balance: wallet.referral_balance - value)
    end
  end

  private

  def credit_wallet_balance
    wallet.update!(referral_balance: wallet.referral_balance + amount)
  end

end
