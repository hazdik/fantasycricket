# == Schema Information
#
# Table name: players
#
#  id                  :integer          not null, primary key
#  name                :string
#  created_at          :datetime
#  updated_at          :datetime
#  player_category     :string
#  price               :integer          default(0)
#  participant_team_id :integer
#  player_xp           :integer          default(0)
#  photo_url           :string
#  api_player_id       :string
#  image               :string
#  odi_career          :hstore
#  ipl_career          :hstore
#  t20_career          :hstore
#  date_of_birth       :datetime
#  test_career         :hstore
#  api_provider        :string
#

class Player < ActiveRecord::Base

  PLAYER_CATEGORIES = %w(batsman bowler all_rounder wicket_keeper wicket_keeper_batsman)


  SCORES_PRO_PLAYER_CATEGORY =  {"bowler"=> "bowler", "batsman"=> "batsman", "allrounder"=> "all_rounder", "wicketkeeper"=> "wicket_keeper" }
  WICKET_TYPES = {"1"=>"CLEAN BOWLED", "2"=>"LBW", "3"=>"CATCH OUT", "4"=>"OUT STUMPED", "5"=>"RUN OUT", "6"=>"HIT WICKET", "7"=>"HANDLED THE BALL", "8"=>"NOT OUT", "9"=>"DID NOT BAT", "10"=>"RETIRED", "11"=>"ABSENT", "12"=>"BAT & PAD", "13"=>"HIT WICKET", "14"=>"CATCH OUT(SUB)", "15"=>"RUN OUT(SUB)", "16"=>"OBSTRUCTING THE FIELD", "17"=>"WIDE+OBS", "18"=>"OUT STUMPED(SUB)", "19"=>"HIT THE BALL TWICE", "20"=>"1 WIDE+HIT WICKET", "21"=>"YET TO BAT"}
  attr_accessor :image_cache

  mount_uploader :image, ImageUploader
  # has_and_belongs_to_many :user_teams, join_table: :players_teams
  has_many :player_scores
  has_many :match, through: :player_scores
  has_many :career_stats

  belongs_to :participant_team

  validates :name, presence: true, length: { maximum: 100 }
  validates :participant_team, presence: true
  validates :player_category, inclusion: { in: PLAYER_CATEGORIES, allow_blank: true }


  scope :batsman, -> { where(player_category: ['batsman']) }
  scope :bowler, -> { where(player_category: 'bowler') }
  scope :wicketkeeper, -> { where(player_category: ['wicket_keeper']) }
  scope :allrounder, -> { where(player_category: 'all_rounder') }
  # scope :wicketkeeper_batsman, -> { where(player_category: 'wicket_keeper_batsman') }

  # Overridden accessors
  ## Below is ruby meta programming for overridding following methods:
  #   :odi_career, :ipl_career, t20_career, :test_career
  # [:odi_career, :ipl_career, :t20_career, :test_career].each do |method_name|
  #   define_method method_name do
  #     # debugger
  #
  #       # debugger
  #       if self.api_provider.blank?
  #         [key, value.blank? ? nil : JSON.parse(value.to_s.gsub('=>', ':').gsub('nil', '""'))]
  #       elsif self.api_provider == 'goalserve'
  #         career_stat = career_stats.first
  #         # debugger
  #         if career_stat
  #           if method_name == :odi_career
  #             career_stats_per_type = career_stat.batting['ODIs']
  #           elsif method_name == :t20_career
  #             career_stats_per_type = career_stat.batting['T20Is']
  #           elsif method_name == :test_career
  #             career_stats_per_type = career_stat.batting['Tests']
  #           # else
  #           #   return {}
  #           end

  #           batting_stats(career_stats_per_type)
  #         else
  #           {}
  #         end
  #       end
  #     end.to_h
  #   end
  # end

  def image
    if ['batsman','wicket_keeper'].include?(player_category)
      {url: 'https://s3.ap-south-1.amazonaws.com/fantoss.com/img/icon_batsman.jpg'}
    else
      {url: 'https://s3.ap-south-1.amazonaws.com/fantoss.com/img/icon_allrounder.jpg'}
    end
  end

  def odi_career
    # debugger
    if self.api_provider.blank?
      self[:odi_career].to_h.map do |key, value|
        [key, value.blank? ? nil : JSON.parse(value.to_s.gsub('=>', ':').gsub('nil', '""'))]
      end.to_h
    elsif self.api_provider == 'goalserve'
      career_stat = career_stats.first
      if career_stat
        batting_stats(career_stat.batting.try(:[], 'ODIs'))
      else
        {}
      end
    end
  end

  def ipl_career
    if self.api_provider.blank?
      self[:odi_career].to_h.map do |key, value|
        [key, value.blank? ? nil : JSON.parse(value.to_s.gsub('=>', ':').gsub('nil', '""'))]
      end.to_h
    elsif self.api_provider == 'goalserve'
      {}
    end
  end

  def t20_career
    if self.api_provider.blank?
      self[:t20_career].to_h.map do |key, value|
        [key, value.blank? ? nil : JSON.parse(value.to_s.gsub('=>', ':').gsub('nil', '""'))]
      end.to_h
    elsif self.api_provider == 'goalserve'
      career_stat = career_stats.first
      if career_stat
        batting_stats(career_stat.batting.try(:[], 'T20Is')) || batting_stats(career_stat.batting.try(:[], 'T20s'))
      else
        {}
      end
    end
  end

  def test_career
    if self.api_provider.blank?
      self[:test_career].to_h.map do |key, value|
        [key, value.blank? ? nil : JSON.parse(value.to_s.gsub('=>', ':').gsub('nil', '""'))]
      end.to_h
    elsif self.api_provider == 'goalserve'
      career_stat = career_stats.first
      if career_stat
        batting_stats(career_stat.batting.try(:[], 'Tests'))
      else
        {}
      end
    end
  end

  def batting_stats(career_stats_per_type)
    if career_stats_per_type
      { "Batting":
        {
          "type": "against",
          "value": "all",
          "Matches": career_stats_per_type['matches'],
          "Innings": career_stats_per_type['innings'],
          "NotOuts": career_stats_per_type['not_outs'],
          "Runs": career_stats_per_type['runs'],
          "Highest": career_stats_per_type['highest_inning_score'],
          "Average": career_stats_per_type['batting_average'],
          "StrikeRate": career_stats_per_type['sr'],
          "Hundreds": career_stats_per_type['hundreds_scored'],
          "Fifties": career_stats_per_type['fifties_scored'],
          "Fours": career_stats_per_type['boundary_fours'],
          "Sixes": career_stats_per_type['boundary_sixiers'],
        }
      }
    else
      {}
    end
  end

  # Callbacks
  # before_save :upload_image, if: -> { photo_url && photo_url_changed? }

  # before_save :update_player_scores
  # after_save :update_parent_team_scores

  # def +(ps)
  #   raise ArgumentError unless ps.is_a? PlayerScore
  #   result = clone
  #   apply_ps_to_p(result, ps, false)
  #   return result
  # end

  # def -(ps)
  #   raise ArgumentError unless ps.is_a? PlayerScore
  #   result = clone
  #   apply_ps_to_p(result, ps, true)
  #   return result
  # end

  private
    def upload_image
      self.remote_image_url = photo_url
    end

    # # Apply a player_score to a player
    # def apply_ps_to_p(p, ps, neg = false)
    #   pm = neg ? -1 : 1
    #   runs = ps.bat_runs_scored.to_i
    #   centuries = runs / 100
    #   fifties = (runs - centuries * 100) / 50
    #   p.bat_hundreds = p.bat_hundreds.to_i + pm*centuries
    #   p.bat_fifties = p.bat_fifties.to_i + pm*fifties
    #   p.bat_ducks = p.bat_ducks.to_i + pm*1 if (!ps.bat_runs_scored.nil?) and (ps.bat_runs_scored.to_i == 0) and (ps.bat_not_outs.to_i == 0)
    #   p.bat_runs_scored = p.bat_runs_scored.to_i + pm*ps.bat_runs_scored.to_i
    #   p.bat_not_outs = p.bat_not_outs.to_i + pm*ps.bat_not_outs.to_i
    #   p.bat_fours = p.bat_fours.to_i + pm*ps.bat_fours.to_i
    #   p.bat_sixes = p.bat_sixes.to_i + pm*ps.bat_sixes.to_i
    #   p.bat_innings = p.bat_innings.to_i + pm*1 if
    #     ps.bat_runs_scored.to_i + ps.bat_balls.to_i + ps.bat_not_outs.to_i > 0 or !ps.bat_how.nil?

    #   wickets = ps.bowl_wickets.to_i
    #   six_wickets = wickets / 6
    #   four_wickets = (wickets - six_wickets * 6) / 4
    #   p.bowl_overs = p.bowl_overs.to_i + pm*ps.bowl_overs.to_i
    #   p.bowl_maidens = p.bowl_maidens.to_i + pm*ps.bowl_maidens.to_i
    #   p.bowl_runs = p.bowl_runs.to_i + pm*ps.bowl_runs.to_i
    #   p.bowl_wickets = p.bowl_wickets.to_i + pm*wickets
    #   p.bowl_4_wickets = p.bowl_4_wickets.to_i + pm*four_wickets
    #   p.bowl_6_wickets = p.bowl_6_wickets.to_i + pm*six_wickets

    #   p.field_catches = p.field_catches.to_i + pm*ps.field_catches.to_i
    #   p.field_runouts = p.field_runouts.to_i + pm*ps.field_runouts.to_i
    #   p.field_stumpings = p.field_stumpings.to_i + pm*ps.field_stumpings.to_i
    #   p.field_drops = p.field_drops.to_i + pm*ps.field_drops.to_i

    #   p.field_mom = p.field_mom.to_i + pm*1 if ps.innings.match.mom == p.id
    # end
end
