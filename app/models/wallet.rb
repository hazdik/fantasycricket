# == Schema Information
#
# Table name: wallets
#
#  id               :integer          not null, primary key
#  main_balance     :float            default(0.0)
#  referral_balance :float            default(0.0)
#  user_id          :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Wallet < ActiveRecord::Base

  # Associations
  belongs_to :user
  has_many :referral_credits, dependent: :destroy
  has_many :wallet_transactions, dependent: :destroy
  has_many :wallet_snaps, dependent: :destroy

  # Validations
  validates :user, presence: true
  validates :main_balance, :referral_balance, numericality: true #{ greater_than_or_equal_to: 0.0 }

  # Callbacks
  after_create :create_first_snap


  def total_balance
    main_balance + referral_credits.available.sum(:amount_remaining)
  end

  def debit_referral_balance(amount)
    referral_credits.available.order(created_at: :desc).each do |referral_credit|
      if amount <= referral_credit.amount_remaining
        referral_credit.redeem(amount)
        break
      else
        amount -= referral_credit.amount
        referral_credit.redeem(referral_credit.amount)
      end
    end
  end

  def debit_full_referral_balance
    debit_referral_balance(referral_balance)
  end

  def credit_referral_balance(amount)
    referral_credits.create(amount: amount)
  end

  private

  def create_first_snap
    wallet_snaps.create(
      main_balance: main_balance,
      referral_balance: referral_balance,
      user: user
    )
  end

end
