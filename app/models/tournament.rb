# == Schema Information
#
# Table name: tournaments
#
#  id            :integer          not null, primary key
#  name          :string
#  start_time    :datetime
#  end_time      :datetime
#  created_at    :datetime
#  updated_at    :datetime
#  api_series_id :string
#  api_provider  :string
#  details       :json
#

class Tournament < ActiveRecord::Base

  # Constants
  TITLE_COLOR = {
    pakistan: '#008c00',
    india: '#2255a4',
    bangladesh: 'rgb(191, 22, 24)',
    australia: 'dark yellow',
    west_indies: 'brown',
    england: '#008c00',
    south_africa: '#008c00'
  }

  has_many :matches, dependent: :destroy

  scope :upcoming, lambda { where("start_time >= ?", (Time.zone.now + 1.hour)).order("start_time ASC") }
  scope :ongoing, lambda { where("start_time <= ? and ? <= end_time",Time.zone.now,Time.zone.now) }

end
