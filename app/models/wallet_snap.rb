class WalletSnap < ActiveRecord::Base

  # Associations
  belongs_to :user
  belongs_to :wallet

  # Validations
  validates :user, :wallet, presence: true
  validates :main_balance, :referral_balance, numericality: true

end
