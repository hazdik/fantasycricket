# == Schema Information
#
# Table name: user_teams
#
#  id                           :integer          not null, primary key
#  user_id                      :integer
#  totalscore                   :integer          default(0)
#  has_won                      :boolean          default(FALSE)
#  created_at                   :datetime
#  updated_at                   :datetime
#  match_id                     :integer
#  league_id                    :integer
#  rank                         :integer
#  token                        :string
#  transaction_data             :hstore
#  captain_id                   :integer
#  contest_id                   :integer
#  captain_id1                  :integer
#  has_tied                     :boolean          default(FALSE)
#  total_wallet_balance_used    :float
#  referral_wallet_balance_used :float
#  main_wallet_balance_used     :float
#

class UserTeam < ActiveRecord::Base
  include Concerns::HashGenerator
  include HTTParty
  scope :played, lambda { |user_id| where("updated_at < ?", Date.today).order("updated_at DESC").limit(10) }

  attr_accessor :ut_token

  BATSMAN_COUNT = 4
  BOWLER_COUNT = 4
  ALL_ROUNDER_COUNT = 2
  WICKET_KEEPER_COUNT = 1
  CAPTAIN_SCORE_MULTIPLIER = 2.0
  VICE_CAPTAIN_SCORE_MULTIPLIER = 1.5
  # Assocaitions
  has_one :money_transaction
  has_many :user_transactions
  # has_and_belongs_to_many :players, join_table: :players_teams
  # has_and_belongs_to_many :players, after_add: :update_teamscore, after_remove: :update_teamscore
  # has_and_belongs_to_many :player_scores
  belongs_to :match
  belongs_to :contest
  belongs_to :user
  belongs_to :league, counter_cache: true
  store_accessor :transaction_data, :transaction_provider_id, :transaction_provider_name, :transaction_status

  # Callbacks
  before_create :generate_token

  # Alias Methods
  alias_attribute :vice_captain_id, :captain_id1

  # Validations
  validates :match, :contest, :user, :captain_id, :vice_captain_id, presence: true
  validates :batsman_ids, length: { is: BATSMAN_COUNT }
  validates :bowler_ids, length: { is: BOWLER_COUNT }
  validates :allrounder_ids, length: { is: ALL_ROUNDER_COUNT }
  validates :wicketkeeper_ids, length: { is: WICKET_KEEPER_COUNT }
  # validate :meets_composition_rules
  validate :validate_time_limitation, if: -> { match.present? }

  # Callbacks
  # after_update :manage_referral_credit
  # after_update :manage_paytm_credit
  # after_update :manage_optimise_pingback

  def players
    Player.where(id: batsman_ids + bowler_ids + allrounder_ids + wicketkeeper_ids)
  end

  def batsmen
    Player.where(id: batsman_ids)
  end

  def bowlers
    Player.where(id: bowler_ids)
  end

  def allrounders
    Player.where(id: allrounder_ids)
  end

  def wicketkeepers
    Player.where(id: wicketkeeper_ids)
  end

  def update_transaction(payment_id, status)
    self.transaction_provider_id = payment_id if payment_id.present?
    self.transaction_status = status if status.present?
    self.save!(:validate => false)
  end

  def allow_edit?
    # !match.starts_within_one_hour?
    !match.bid_closed?
  end

  def can_join_league?
    !league and allow_edit?
    # !league and !match.starting_in_45?
  end

  def payment_url
    league.instamojo_link + token.to_s
  end

  def update_score_from_players_score
    self.update_column(:totalscore, calculate_total_score)
  end

  def calculate_total_score
    # self.totalscore=player_scores.sum(:total)
    t_score = player_scores.sum(:total)
    # self.totalscore += captain_score.sum(:total) if captain_score.present?
    t_score -= cap_score if cap_score.present?
    t_score -= vice_cap_score if vice_cap_score.present?
    t_score += (cap_score * CAPTAIN_SCORE_MULTIPLIER).round(3) if cap_score.present?
    t_score += (vice_cap_score * VICE_CAPTAIN_SCORE_MULTIPLIER).round(3) if vice_cap_score.present?
    # self.save!(:validate => false)
    t_score
  end

  # def totalscore
  #   calculate_total_score
  # end

  def player_ids
    batsman_ids + bowler_ids + wicketkeeper_ids + allrounder_ids
  end

  def player_scores
    match.player_scores.where(player_id: player_ids)
  end
  def captain_score
    # player_scores.find_by(player_id: captain_id)
    c_ids = [captain_id, captain_id1]
    player_scores.where(player_id: c_ids)
  end

  def cap_score
    player_scores.where(player_id: captain_id).first.try(:total)
  end

  def vice_cap_score
    player_scores.where(player_id: captain_id1).first.try(:total)
  end

  def player_with_scores
    # players.includes(:player_scores).where("player_scores.match_id" => [match.id,nil] )
    players.joins("LEFT OUTER JOIN player_scores ON player_scores.match_id = #{match.id}")
  end

  def notify_about_demotion

    NotificationWorker.new.perform(
      {
        recipient: user,
        content_plain: ActionController::Base.new.render_to_string(
          template: 'sms_templates/notify_about_demotion.txt',
          locals: { '@match': league.match }
        ),
        mailer_options: {
          mailer: 'LeagueMailer',
          mail_method: 'notify_about_demotion',
          mail_params: [self]
        }
      }
    )
  end

  def notify_about_league_refund

    NotificationWorker.new.perform(
      {
        recipient: user,
        content_plain: ActionController::Base.new.render_to_string(
          template: 'sms_templates/notify_about_league_refund.txt',
          locals: { '@match': league.match }
        ),
        mailer_options: {
          mailer: 'LeagueMailer',
          mail_method: 'notify_about_league_refund',
          mail_params: [self]
        }
      }
    )
  end

  private

    def manage_paytm_credit
      if league_id && league_id_changed? && user.leagues.paid.count==1
        user.send_paytm_cashback(league.entry_fee)
      end
    end

    def manage_referral_credit
      #if league_id && league_id_changed? && user.leagues.referral_eligible.count==1 && league.is_referral_eligible?
      if league_id && league_id_changed? && league_id_was==nil && user.leagues.referral_eligible.count==1 && league.is_referral_eligible?
        if ((user.leagues.paid.count == 1 ))
          @entry_fee = user.leagues.paid.first.entry_fee
          @entry_fee_credit = (@entry_fee.to_i * 10)/100
          @referral_credit = @entry_fee_credit < 20 ? Rails.application.config.settings.referral_credit : @entry_fee_credit
        end
        user.award_referral_credit_to_referrer(@referral_credit)
      end
    end

    def manage_optimise_pingback
      if user.leagues.paid.count==1 && user.referral_code == User::AFFILIATE_DETAILS[:optimise][:code_string] && league_id && league_id_changed?
        join_league_user_transaction = user_transactions.where(transaction_category: 'join_league').first
        url = "#{ User::AFFILIATE_DETAILS[:optimise][:pingback_url] }?APPID=#{ join_league_user_transaction.try(:id) }&MID=1058670&PID=30492&status=1&EX1=SALE&SSKEY=#{ user.meta_info['optimisekey'] }"
        # "https://track.in.omgpm.com/apptag.asp?APPID=orderid&MID=1058670&PID=30492&status=1&EX1=SALE&SSKEY=SOME_KEY"
        response = HTTParty.post(url)
      end
    end

    def a_type
      league.contest.answer_type
    end

    def generate_token
      self.token = loop do
        random_token = generate_for(self)
        break random_token unless UserTeam.exists?(token: random_token)
      end
    end

    def meets_composition_rules
      if contest.top_11_players?
        validates_for_top_11_players
      elsif contest.top_5_batsmen?
        validates_for_top_5_batsmen
      end
    end

    def validates_for_top_11_players
      players = Player.where(id: player_ids)
      validate_players_count(players, 11)
      validate_max_player_count_from_one_team(players, 7)
      validate_players_team(players)
      validate_team_selection(players)
      validate_captain(players)
    end

    def validates_for_top_5_batsmen
      # validate_all_batsmen(players)
      validate_players_count(players, 5)
      # validate_players_team(players)
      # validate_max_player_count_from_one_team(players, 3)
      validate_captain(players)
    end

    def validate_players_count(players, count)
      if players.length != count
        errors.add(:player_ids, "You have to select top #{ count } players.")
      end
    end

    def validate_all_batsmen(players)
      # should be included as batsman ['batsman', 'all_rounder', 'keeper']
      if players.collect(&:player_category).uniq != ["batsman"]
        errors.add(:base, "You can only select batsmen to play this contest.")
      end
    end

    def validate_max_player_count_from_one_team(players, max_from_a_team)
      players_team_data = players.group_by(&:participant_team_id)

      players_team_data.each do |id, ptd|
        if ptd.count > max_from_a_team
          errors.add(:base, "You can not select more than #{ max_from_a_team } player from one team.")
        end
      end
    end

    def validate_players_team(players)
      team_ids = players.collect(&:participant_team_id).uniq.sort

      if (team_ids - [match.home_team_id, match.away_team_id]).any?
        errors.add(:base, 'You can not select players from these teams.')
      end
    end

    def validate_team_selection(players)
      players_category_data = players.group_by(&:player_category)
      if players_category_data["batsman"].try(:size) != BATSMAN_COUNT
        errors.add(:base, "You have to select #{ BATSMAN_COUNT } batsmen")
      end
      if players_category_data["bowler"].try(:size) != BOWLER_COUNT
        errors.add(:base, "You have to select #{ BOWLER_COUNT } bolwers")
      end
      if players_category_data['all_rounder'].try(:size) != ALL_ROUNDER_COUNT
        errors.add(:base, "You have to select #{ ALL_ROUNDER_COUNT } all rounders")
      end
    end

    def validate_captain(players)
      if (captain_id && captain_id1) && (player_ids.exclude? captain_id) &&  (player_ids.exclude? captain_id1)
        errors.add(:base, "Captain must be from the participating teams only.")
      end
    end

    def validate_time_limitation
      unless allow_edit?
        errors.add(:base, "OOPS! You can not create/update user team now.")
      end
    end


  # private
  #   def update_teamscore(p)
  #     $stderr.puts "+++Player #{p.name} added/removed to/from team #{self.name}"
  #     # Team scores are unaffected by adding or removing players.
  #     # Only the changes in a team's players' scores change the team score.
  #     #self.totalscore = self.players.sum(:total)
  #     #self.save
  #   end

  #     def update_parent_user_total
  #     $stderr.puts "+++User #{self.user.name} totalscore updated because team #{self.name} updated"
  #     # This is fine, but most of the code assumes that a user can only have one team.
  #         self.user.totalscore = self.user.teams.sum(:totalscore)
  #         self.user.save!
  #     end

end

