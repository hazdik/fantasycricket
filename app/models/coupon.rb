# == Schema Information
#
# Table name: coupons
#
#  id                            :integer          not null, primary key
#  code                          :string
#  discount_value                :float
#  discount_type                 :string
#  maximum_discount_value        :float
#  start_date                    :datetime
#  end_date                      :datetime
#  maximum_redeem_count          :integer
#  current_redeem_count          :integer
#  user_validation_eval_string   :string
#  league_validation_eval_string :string
#  tournament_id                 :integer
#  match_id                      :integer
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  per_user_count                :integer          default(1)
#  concession_type               :string           default("LEAGUE_ENTRY_FEE")
#  coupon_message                :string
#

class Coupon < ActiveRecord::Base

  # Constants
  DISCOUNT_TYPES = ['FLAT', 'PERCENTAGE']
  CONCESSION_TYPES = ['LEAGUE_ENTRY_FEE', 'PAYTM_CASHBACK']
  DISCOUNTED_LLEAGUES = ['even', 'odd']

  # Associations
  belongs_to :tournament
  belongs_to :match

  has_many :users_coupons
  has_many :users, through: :users_coupons

  # Validations
  validates :code, uniqueness: true
  validates :discount_value, numericality: { greater_than_or_equal_to: 0.0 }
  validates :discount_type, inclusion: { in: DISCOUNT_TYPES }
  validates :concession_type, inclusion: { in: CONCESSION_TYPES }
  validates :maximum_discount_value, numericality: { allow_nil: true, greater_than_or_equal_to: 0.0 }
  validates :maximum_redeem_count, numericality: { only_integer: true, allow_nil: true, greater_than_or_equal_to: 0.0 }
  validates :current_redeem_count, numericality: { only_integer: true, allow_nil: true, greater_than_or_equal_to: 0.0 }
  validates :per_user_count, numericality: { only_integer: true, greater_than_or_equal_to: 1.0 }
  validates :tournament, absence: true, if: :match
  validates :match, absence: true, if: :tournament
  validate :leagues_validations
  validate :users_validations, if: :user_validation_eval_string?

  def applicable_league_prices
    @applicable_league_prices ||= eval("%w(#{ league_validation_eval_string })")
  end

  def applicable_user_ids
    begin
      @applicable_user_ids ||= eval("User.#{ user_validation_eval_string }.pluck('users.id')")
    rescue Exception => e
      errors.add(:user_validation_eval_string, 'Invalid data! Please fill with correct details')
    end
  end

  def applicable_match_ids
    @applicable_match_ids ||= eval("Match.#{ match_validation_eval_string }.pluck('matches.id')")
  end

  def applicable_league_types
    @applicable_league_types ||= [league_type]
  end

  private

  def leagues_validations
    leagues_prices_array = applicable_league_prices
    if !leagues_prices_array.is_a?(Array) || ((League::ENTRY_FEE_ENUM & leagues_prices_array) != leagues_prices_array )
      errors.add(:league_validation_eval_string, 'Invalid data! Please fill with correct details')
    end
  end

  def users_validations
    unless applicable_user_ids.is_a?(Array)
      errors.add(:user_validation_eval_string, 'Invalid data! Please fill with correct details')
    end
  end

end




#coupon tag




