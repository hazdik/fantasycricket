# == Schema Information
#
# Table name: participant_teams
#
#  id            :integer          not null, primary key
#  name          :string
#  created_at    :datetime
#  updated_at    :datetime
#  captain_id    :integer
#  keeper_id     :integer
#  api_team_id   :string
#  current_squad :text             default([]), is an Array
#  short_name    :string           default("")
#  api_provider  :string
#

class ParticipantTeam < ActiveRecord::Base

  has_many :players
  has_many :batsmans, -> { batsman }, class_name: 'Player'
  has_many :bowlers, -> { bowler }, class_name: 'Player'
  has_many :wicketkeepers, -> { wicketkeeper }, class_name: 'Player'
  has_many :allrounders, -> { allrounder }, class_name: 'Player'

  has_many :home_matches, :class_name => 'Match', :foreign_key => 'home_team_id'
  has_many :away_matches, :class_name => 'Match', :foreign_key => 'away_team_id'

  #callback
  before_create :create_short_name

  # Validations
  validates :api_team_id, uniqueness: { scope: :api_provider }


  def current_players
    Player.find(current_squad)
  end

  # def flag_path
  #   # img_name = name
  #   # img_name = name.gsub(' ', '_') if name.split.size > 1
  #   # return "/assets/country_flags/" + img_name.downcase + '.jpg'
  #   if api_team_id.to_i >= 140010 && api_team_id.to_i <= 140018
  #     return "team_logos/#{ name.downcase }"
  #   else
  #     return "team_logos/team_logo_#{ (id%8) + 1 }"
  #   end
  # end

  def flag_path
    read_attribute(:flag_path) || "team_logos/team_logo_#{ (id%8) + 1 }"
  end

  private

  def create_short_name
    self.short_name = (self.name.split.count == 1) ?  (self.name[0..2].upcase) :  (self.name.split.map(&:first).join.upcase)
  end

end
