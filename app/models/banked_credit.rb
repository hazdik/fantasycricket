# == Schema Information
#
# Table name: banked_credits
#
#  id                :integer          not null, primary key
#  value             :integer          default(0)
#  reason            :string
#  user_id           :integer
#  referring_to_id   :integer
#  referring_to_type :string
#  redeemed_on       :datetime
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  transaction_type  :string           default("")
#

class BankedCredit < ActiveRecord::Base
  belongs_to :user
  belongs_to :referring_to, polymorphic: true

  scope :unredeemed, -> { where(redeemed_on: nil, referring_to_type: 'User') }

  scope :credited, lambda { where("transaction_type = ?", "credit" ).where(redeemed_on: nil) }
  scope :debited, lambda { where("transaction_type = ?", "debit" ).where(redeemed_on: nil) }
  scope :all_credited, lambda { where("transaction_type = ?", "credit" ) }
  scope :all_debited, lambda { where("transaction_type = ?", "debit" ) }

  validates :user, :user_id, :value, presence: true

  after_create :update_wallet_credit

  private

  def update_wallet_credit
    case transaction_type
    when 'credit'
      if reason == 'referral' || reason == 'coupon_refund'
        transaction do
          user.wallet.reload.credit_referral_balance(value)
          create_wallet_transactions('credit')
        end
      else
        transaction do
          user.wallet.reload.update!(main_balance: user.wallet.reload.main_balance + value)
          create_wallet_transactions('credit')
        end
      end

    when 'debit'
      transaction do
        if reason == 'withdraw_to_bank' || reason == 'deduction'
          user.wallet.reload.update!(main_balance: user.wallet.reload.main_balance - value)
          create_wallet_transactions('debit')
        elsif reason == 'referral_expiry'
          user.wallet.update!(referral_balance: user.wallet.referral_balance - value)
          create_wallet_transactions('debit')
        else
          referral_balance = referring_to.try(:league).try(:is_referral_free?) ? 0 : user.wallet.referral_credits.available.sum(:amount_remaining)
          main_balance = user.wallet.reload.main_balance

          if (referral_balance > 0)
            if (referral_balance >= value)
              user.wallet.reload.debit_referral_balance(value)
              create_wallet_transactions('debit', category: 'debit_from_referral')
            else
              referral_balance = user.wallet.referral_credits.available.sum(:amount_remaining)
              if referral_balance > 0
                user.wallet.reload.debit_referral_balance(referral_balance)
                # user.wallet.reload.debit_full_referral_balance
                create_wallet_transactions('debit', category: 'debit_from_referral', value: referral_balance)
              end
              user.wallet.reload.update!(main_balance: main_balance - (value - referral_balance))
              create_wallet_transactions('debit', category: 'debit_from_main_wallet', value: (value - referral_balance))
            end

          else
            user.wallet.reload.update!(main_balance: main_balance - value)
            create_wallet_transactions('debit', category: 'debit_from_main_wallet')
          end
        end

      end

    end
  end

  def create_wallet_transactions(transaction_type, options = {})
    # debugger
    WalletTransaction.create!(
      user_transaction: referring_to,
      user: user,
      wallet: user.wallet.reload,
      amount: options[:value] || value,
      transaction_type: transaction_type,
      current_wallet_amount: {
        main_balance: user.wallet.reload.main_balance,
        referral_balance: user.wallet.reload.referral_balance
      },
      category: options[:category] || reason,
      banked_credit: self
    )
  end

end
