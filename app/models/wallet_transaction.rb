class WalletTransaction < ActiveRecord::Base

  # Associations
  belongs_to :wallet
  belongs_to :user
  belongs_to :banked_credit
  belongs_to :user_transaction

  # Validations
  validates :wallet, :amount, :transaction_type, :current_wallet_amount, :category, presence: true

  # Callbacks
  after_create :join_league

  private

  def join_league
    user_team = user_transaction.user_team
    if user_team
      unless  ['match_win', 'tie_refund', 'coupon_refund'].include?(user_transaction.transaction_category)

        # user_team.update_column(:league_id, user_transaction.league.id)
        user_team.update!(league: user_transaction.league)
        League.reset_counters(user_transaction.league.id, :user_teams)
      end
    end
  end

end
