class LeagueMailer < ActionMailer::Base
  default from: Rails.application.config.settings.mail.from
  # layout 'emails/email2'
  layout 'application_mailer'

  LeagueMailer.raise_delivery_errors do
    LeagueMailer.mail_delivery_unsuccessfull
  end

  def mail_delivery_unsuccessfull
    logger.info "=:=:=mail delivery failed==;==must be phone signup=:=:="
  end

  def added_to_league(league_id, user_transaction_id)
    user_transaction = UserTransaction.find_by(id: user_transaction_id)
    league = League.find_by(id: league_id)
    @league = league
    @match = league.match
    @user_transaction = user_transaction
    @user = @user_transaction.user
    # TODO: Un Comment following Line!
    mail(to: @user.email, subject: "Awesome, #{@league.name} league joined for #{@match.team_names} match.") if @user.email.present?
  end

  def notify_about_demotion(user_team)
    @league = user_team.league
    @match = @league.match
    @user_team = user_team
    @user = @user_team.user

    mail(to: user_team.user.email, subject: "Update: #{@league.name} League details changed for #{@match.team_names} match.") if user_team.user.email.present?
  end

  def notify_users_about_free_league(league)
    @league = league
    @match = league.match
    user_emails = @league.user_teams.includes(:user).collect { |ut| ut.user.email }

    mail(to: user_emails, subject: "You are added to free league for #{@match.team_names} match.") if user_emails.present?
  end

  def notify_user_about_winning(user_team_id, league_id)
    user_team = UserTeam.find_by(id: user_team_id)
    league = League.find_by(id: league_id)
    @league = league
    @match = league.match
    @user = user_team.user
    subject  = "Wohoo!!! You just won #{league.name} league for #{@match.team_names} match."
    subject  = "Hello!! You are a equal winner in #{league.name} league for #{@match.team_names} match." if @league.is_tied
    mail(to: @user.email, subject: subject) if @user.email
  end

  def notify_user_about_losing(user_team_id, league_id)
    user_team = UserTeam.find_by(id: user_team_id)
    league = League.find_by(id: league_id)
    @league = league
    @match = league.match
    @user = user_team.user
    mail(to: @user.email, subject: "Well played, Results are out for #{@match.team_names} match.") if @user.email.present?
  end

  def notify_user_about_tie(user_team_id, league_id)
    user_team = UserTeam.find_by(id: user_team_id)
    league = League.find_by(id: league_id)
    @league = league
    @match = league.match
    @user = user_team.user
    mail(to: @user.email, subject: "Well played, Results are out for #{@match.team_names} match.") if @user.email.present?
  end

  def notify_about_league_refund(user_team_id, league_id)
    user_team = UserTeam.find_by(id: user_team_id)
    league = League.find_by(id: league_id)
    @league = league
    @match = @league.match
    @user = user_team.user

    mail(to: @user.email, subject: "Oops! Your league #{@league.name} for #{@match.team_names} match, got Cancelled!") if @user.email.present?
  end

end
