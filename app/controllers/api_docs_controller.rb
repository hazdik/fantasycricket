class ApiDocsController < ApplicationController
  layout false

  http_basic_authenticate_with name: "fm_dev", password: "fm_d3v"

  skip_before_action :authenticate_user_from_token!
  skip_before_action :authenticate_user!

  def index
  end
end
