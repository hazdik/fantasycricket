class Admin::EntityApiProviderController < Admin::ApplicationController

  def seasons
    @seasons = Entity::Cricket::Season.all.parsed_response
  end

  def seasons_competitions
    @seasons = Entity::Cricket::Season.new('sid' => params[:sid])
    @competitions = @seasons.competitions
  end

  def competition_show
    @competition = Entity::Cricket::Competition.get(params[:cid])
  end

  def competition_squads_show
    @competition = Entity::Cricket::Competition.get(params[:cid])
    @competition.set_squads
    @squads = @competition.squads
  end

end
