module Admin
  class MatchesController < Admin::ApplicationController
    skip_before_filter :authenticate_super_admin!, only: [:index, :show, :winners]

    def winners
      @match   = requested_resource
      @leagues = @match.leagues.paid.order(entry_fee: :desc, limit: :desc).includes(user_teams: :user).page(params[:page]).per(20)
      #@total_transactions = @match.user_transactions.where("params -> 'entry' != '0'").where("params -> 'entry' != 'free'").where(state: 'completed')
      @total_transactions_even = @match.leagues.even.joins(:user_transactions).where("params -> 'entry' != '0'").where("params -> 'entry' != 'free'").where('user_transactions.state': 'completed')
      @total_transactions_odd = @match.leagues.odd.joins(:user_transactions).where("params -> 'entry' != '0'").where("params -> 'entry' != 'free'").where('user_transactions.state': 'completed')
      @stats_even = _all_leagues_stats(@match)
      @stats_odd = _all_leagues_odd_stats(@match)
      @user_teams = UserTeam.where(league: @match.leagues.paid)
      respond_to do |format|
        format.html
        format.csv { send_data(::Matches::WinnersExporter.export(@match), filename: "match-#{@match.id}-winner.csv") }
      end
    end

    def send_money
      match = Match.find(params[:id])
      if match
        options = {}
        if match.user_transactions.pluck(:transaction_category).uniq.count > 1
          msg = 'Payment for this match has already been done.(Please ask concerned developer if any query)'
        elsif !match.is_payable
          msg = 'Come back later when match will be finished'
        else
          options[:match_status] = params[:status]
          options[:match_id] = params[:id]
          msg = 'Queued for payments.'
          match.queue_for_payments(options)
        end
      else
        msg = 'No such match exists, please ask the concerned person.'
      end
      flash[:notice] = msg
      redirect_to :back
    end

    private

      def _all_leagues_stats(m)
        leagues_and_teams = {}
        @total_coupon_money_used = 0
        League::PAID_LEAGUE_ENUM.each do |l_entry_fee|

          l_of_l_entry_fees = m.leagues.even.paid.where(entry_fee: l_entry_fee)

          leagues_and_teams["#{l_entry_fee}"] = {
             leagues_count: l_of_l_entry_fees.count,
             ut_count: 0,
             amount_paid: 0,
             winners_count: 0,
             tied_count: 0,
             coupon_money_used: 0
          }

          l_user_teams = UserTeam.where(league: l_of_l_entry_fees)
          l_user_teams_count = l_user_teams.count
          leagues_and_teams["#{l_entry_fee}"][:ut_count] = l_user_teams_count
          leagues_and_teams["#{l_entry_fee}"][:amount_paid] = l_user_teams_count * l_entry_fee.to_f
          leagues_and_teams["#{l_entry_fee}"][:winners_count] = l_user_teams.where(has_won: true).count
          leagues_and_teams["#{l_entry_fee}"][:tied_count] = l_user_teams.where(has_tied: true).count
          coupon_money_used = UsersCoupon.consumed.where(league: l_of_l_entry_fees).sum(:amount)

          leagues_and_teams["#{l_entry_fee}"][:coupon_money_used] = coupon_money_used
          @total_coupon_money_used += coupon_money_used
        end
        leagues_and_teams

      end

      def _all_leagues_odd_stats(m)
        leagues_and_teams = {}
        @total_odd_coupon_money_used = 0
        League::ODD_PAID_LEAGUE_ENUM.each do |l_entry_fee|

          l_of_l_entry_fees = m.leagues.odd.paid.where(entry_fee: l_entry_fee)

          leagues_and_teams["#{l_entry_fee}"] = {
             leagues_count: l_of_l_entry_fees.count,
             ut_count: 0,
             amount_paid: 0,
             winners_count: 0,
             tied_count: 0,
             coupon_money_used: 0
          }

          l_user_teams = UserTeam.where(league: l_of_l_entry_fees)
          l_user_teams_count = l_user_teams.count
          leagues_and_teams["#{l_entry_fee}"][:ut_count] = l_user_teams_count
          leagues_and_teams["#{l_entry_fee}"][:amount_paid] = l_user_teams_count * l_entry_fee.to_f
          leagues_and_teams["#{l_entry_fee}"][:winners_count] = l_user_teams.where(has_won: true).count
          leagues_and_teams["#{l_entry_fee}"][:tied_count] = l_user_teams.where(has_tied: true).count
          coupon_money_used = UsersCoupon.consumed.where(league: l_of_l_entry_fees).sum(:amount)

          leagues_and_teams["#{l_entry_fee}"][:coupon_money_used] = coupon_money_used
          @total_odd_coupon_money_used += coupon_money_used
        end
        leagues_and_teams

      end

  end
end
