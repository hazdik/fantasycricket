# == Schema Information
#
# Table name: challenges
#
#  id                     :integer          not null, primary key
#  fb_request_id          :string
#  user_id                :integer
#  league_id              :integer
#  challenged_count       :integer
#  friends_challenged_ids :string           default([]), is an Array
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  invite_code            :string
#

class ChallengesController < ApplicationController
  skip_authorization_check
  skip_before_action :authenticate_user!
  before_filter :load_challenge
  before_filter :ensure_can_accept_challenge

  def show
    @league = @challenge.league
  end

  def accept
    session["user_return_to"]           = accept_challenge_path(@challenge)
    session["user_return_to_timestamp"] = Time.now.utc.to_i
    @league = @challenge.league
  end

private
  def load_challenge
    @challenge = Challenge.find_by(invite_code: params[:invite_code])
    unless @challenge
      flash[:error] = "No challenge found."
      redirect_to root_path
    end
  end

  def ensure_can_accept_challenge
    unless @challenge.can_be_accepted?
      flash[:error] = "You can not accept this challenge."
      redirect_to root_path
    end
  end
end
