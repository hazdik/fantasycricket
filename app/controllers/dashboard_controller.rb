class DashboardController < ApplicationController
  before_action :authenticate_user!
  before_action :is_admin?
  before_action :set_date_params

  def index
    redirect_to databuddy_users_dashboard_index_path(year: Time.current.year)
  end

  User::AFFILIATE_CODE_STRINGS.keys.each do |affiliate|
    define_method affiliate.to_s+'_users' do
      @affiliate= affiliate
      @val = params[:val]
      # year = params[:year] || Date.current.year
      params[:year] = params[:year].presence || Date.current.year.to_s
      @val = 'total' if params[:month].blank?

      if @val == 'total'
        @all_months = []
        params[:start_date] = nil
        params[:end_date] = nil
        (1..12).each do |month_num|
          @all_months << "#{params[:year]}-#{month_num}-01".to_date
        end
      else
        @days_from_selected_month = (params[:start_date]..params[:end_date])
      end
      if @val == 'total'
        @month_user_count = User.where(referral_code: affiliate.to_s).group("date(date_trunc( 'month', created_at ))").count
        @month_price_count = UserTeam.joins(:user).joins(:league).
                              where(users: { referral_code: affiliate.to_s }).
                              group("date(date_trunc( 'month', user_teams.created_at ))").
                              group(:entry_fee).
                              count
        month_price_total = @month_price_count
      else
        params[:end_date] = params[:end_date].end_of_day
        @user_count = User.where(referral_code: affiliate.to_s).group('date(created_at)').count
        @price_count = UserTeam.joins(:user).joins(:league).
          where(users: { referral_code: affiliate.to_s }).
          where(
            'user_teams.created_at > ? AND user_teams.created_at < ?',
            params[:start_date],
            params[:end_date]
          ).
          group('date(user_teams.created_at)').group(:entry_fee).
          order('date(user_teams.created_at)', 'leagues.entry_fee').count
        month_price_total = @price_count
      end
      @month_price_count_total = month_price_count_total_calc(month_price_total)
      render 'affiliate_users'
    end
  end

  private

  def set_date_params
    if params[:month].present?
      month = params[:month]
      params[:year] = params[:year].presence || Date.current.year.to_s
      params[:start_date] = "#{params[:year]}-#{month}-01".to_date
      params[:end_date] = params[:start_date].end_of_month.end_of_day.to_date
    else
      params[:start_date] = Date.today.beginning_of_month
      params[:end_date] = Date.today.end_of_day
    end
  end

  def month_price_count_total_calc(month_price_total)
    new_h = Hash.new(0)
    month_price_total.each do |k, v|
      new_h[k[0].to_date] += v*k[1]
    end
    return new_h
  end

  def is_admin?
    redirect_to 'https://fantoss.com' unless current_user.is_admin?
  end

end
