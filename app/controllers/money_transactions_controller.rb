class MoneyTransactionsController < ApplicationController
  before_action :find_money_transaction, only: :update
  before_action :load_amount, only: :create
  before_action :ensure_wallet_balance, only: :create
  before_action :ensure_minimum_amount, only: :create
  before_action :ensure_maximum_amount, only: :create
  before_action :ensure_request_count, only: :create

  def create
    # if Rails.env.development? || Rails.env.staging?
    #   redirect_to account_path, notice: "Oops! This is #{ Rails.env } environment!"
    #   return
    # end

    last_user_transanction = current_user.user_transactions.last
    if last_user_transanction && last_user_transanction.created_at > (Time.current - 15.seconds)
      redirect_to account_path, alert: 'User last transaction ocurred within 15 seconds. Please wait a little for this transaction.'
      return
    end

    if current_user.failed_attempts == 0
      current_user.update(failed_attempts: 1)
      money_transaction = MoneyTransferService.transfer_for(current_user.id, { amount: @amount })

      unless money_transaction
        redirect_to account_path, alert: 'Something went wrong. Please try again later or contact customer care.'
        return
      end
    else
      redirect_to account_path, alert: 'Something went wrong. Please try again later or contact customer care.'
      return
    end

    redirect_to account_path, notice: 'Your transfer has been initiated. You can see it\'s status from dashboard.'
  end

  def update
    # if Rails.env.development? || Rails.env.staging?
    #   redirect_to account_path, notice: "Oops! This is #{ Rails.env } environment!"
    #   return
    # end

    MoneyTransferService.check_transaction_status_for(@money_transaction)
    redirect_to account_path
  end

  private

  def ensure_request_count
    last_money_transaction = current_user.money_transactions.last
    if last_money_transaction && last_money_transaction.created_at > (Time.current - 1.day)
      flash[:error] = "You can request for transfer only once everyday! Your last withdrawal request was on #{ last_money_transaction.created_at.strftime('%B %d, %Y %I:%M %p') }"
      redirect_to account_path
    end
  end

  def ensure_maximum_amount
    if @amount > User::MAXIMUM_WITHDRAWL_AMOUNT
      flash[:error] = "You can withdraw maxmimum upto Rs #{ User::MAXIMUM_WITHDRAWL_AMOUNT } at a time!"
      redirect_to account_path
    end
  end

  def ensure_minimum_amount
    if @amount < User::MINIMUM_WITHDRAWL_AMOUNT
      flash[:error] = "Minimum withdraw amount is Rs #{ User::MINIMUM_WITHDRAWL_AMOUNT }"
      redirect_to account_path
    end
  end

  def ensure_wallet_balance
    if current_user.wallet.reload.main_balance.to_f < @amount
      flash[:error] = "Oops! The amount exceeds your wallet's main balance!"
      redirect_to account_path
    end
  end

  def find_money_transaction
    @money_transaction = current_user.money_transactions.find_by(id: params[:id])
    unless @money_transaction
      flash[:error] = 'Transaction not found!'
      redirect_to account_path
    end
  end

  def load_amount
    if params[:amount]
      @amount = params[:amount].to_i
    else
      @amount = current_user.wallet.reload.main_balance.to_f
    end
  end

end
