class DatabuddyController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index]
  before_action :set_date_params
  http_basic_authenticate_with name: "databuddy", password: "databuddy@fantoss"

  def index
    @val = params[:val]
    year = params[:year] || Date.current.year
    if @val == "total"
      @all_months = []
      (1..12).each do |month_num|
        @all_months << "#{year}-#{month_num}-01".to_date
      end
    else
      @days_from_selected_month = (params[:start_date]..params[:end_date])
    end
    if @val == "total"
      @month_user_count = User.where(referral_code: "databuddy").group("date(date_trunc( 'month', created_at ))").count
      @month_price_count = UserTeam.joins(:user).joins(:league).
                            where(users: { referral_code: 'databuddy' }).
                            group("date(date_trunc( 'month', user_teams.created_at ))").
                            group(:entry_fee).
                            count
      month_price_total = @month_price_count
    else
      @user_count = User.where(referral_code: "databuddy").group('date(created_at)').count
      @price_count = UserTeam.joins(:user).joins(:league).
                      where(users: { referral_code: 'databuddy' }).
                      where('user_teams.created_at > ? AND user_teams.created_at < ?',
                      params[:start_date], params[:end_date]).
                      group('date(user_teams.created_at)').group(:entry_fee).
                      order('date(user_teams.created_at)', 'leagues.entry_fee').count
      month_price_total = @price_count
    end
    @month_price_count_total = month_price_count_total_calc(month_price_total)
  end

  private

  def set_date_params
    if params[:month].present?
      month = params[:month]
      year = Date.current.year
      params[:start_date] = "#{year}-#{month}-01".to_date
      params[:end_date] = params[:start_date].end_of_month.to_date
    else
      params[:start_date] = Date.today.beginning_of_month
      params[:end_date] = Date.today
    end
   end

  def month_price_count_total_calc(month_price_total)
    new_h = Hash.new(0)
    month_price_total.each do |k, v|
      new_h[k[0].to_date] += v*k[1]
    end
    return new_h
  end

end
