module LoadResourceConcern
  private
    def load_match
      @match = Match.find_by(id: params[:match_id])
      unless @match
        flash[:error] = "No match found."
        redirect_to root_path
      end
    end

    def load_contest
      @contest = Contest.find_by(id: params[:contest_id])
      unless @contest
        flash[:error] = "No contest found."
        redirect_to root_path
      end
    end
    
    def load_user_team
      # return current_user.present? && current_user.user_teams.present? && current_user.user_teams.find_by_match_id_and_league_id(params[:match_id], params[:id])
      @user_team = @match.user_teams.find_by(id: params[:user_team_id])
      # @user_team = current_user.user_teams.find_by_match_id_and_league_id(params[:match_id], params[:id])
      unless @user_team
        flash[:error] = "No User team found."
        redirect_to root_path
      end
    end

end
