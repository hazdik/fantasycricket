module ApiResourceLoaderConcern
  private

  def load_match
      @match = Match.find_by(id: params[:user_team][:match_id])
      unless @match
       return render json: {responseCode: 500 , responseMessage: "No match present."}
      end
  end

  def load_contest
    @contest = Contest.find_by(id: params[:user_team][:contest_id])
    unless @contest
      return render json: {responseCode: 500 , responseMessage: "No contest present."}
    end
  end

  def load_players
    if @contest.top_11_players?
      @players = @match.all_players.includes(:participant_team).order(:participant_team_id)
    else
      @players = @match.all_batsmen.includes(:participant_team, :player_scores).order(:participant_team_id)
    end
  end

  def load_user_team
    # return current_user.present? && current_user.user_teams.present? && current_user.user_teams.find_by_match_id_and_league_id(params[:match_id], params[:id])
    # @user_team = @match.user_teams.find_by(id: params[:user_team][:team_id], user_id: current_user.id)
    @user_team = current_user.user_teams.find_by(id: params[:user_team][:team_id], match: @match)
    unless @user_team
      return render json: {responseCode: 500 , responseMessage: "No user team found."}
    end
  end

  def ensure_editability
    unless @user_team.allow_edit?
      return render json: {responseCode: 405, responseMessage: "You can not edit this team.", match_id: @match, contest_id: @contest}
    end
  end


end
