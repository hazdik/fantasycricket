class PagesController < ApplicationController
  include LoadAdminConcern
  skip_authorization_check
  skip_before_action :authenticate_user!
  before_action :authenticate_admin!, only: ['upcomingMatches']
  http_basic_authenticate_with name: "fm_admin", password: "fm_adam1n", only: ['upcomingMatches']

  before_action :load_referrer, only: :ref
  # before_action :allow_page_caching, only: ['intro']
  # layout false, only: ['he']

  def intro
    # redirect_to root_path if user_signed_in?
    # @match = Match.next
    # @top_5_contest = Contest.find_by(team_type: 'top_5_batsmen')
    # render layout: false
  end

  def landing
    redirect_to root_path if user_signed_in?
  end

  def home
    # redirect_to "/intro" unless user_signed_in?
    @recent_matches = Match.recent
    @live_now_matches = Match.ongoing
    # @upcoming_matches = Match.upcoming
    @upcoming_matches = Match.display_matches
    @top_5_contest = Contest.find_by(team_type: 'top_5_batsmen')
    @match = @upcoming_matches.first
    if @match
      @page_title = "#{@match.tournament.try(:name)} - #{@match.team_names}, #{@match.title} live match leaderboard"
      prepare_meta_tags(title: @page_title, og: {title: @page_title})
    end
  end


  # Preview html email template
  def email
    tpl = (params[:layout] || 'hero').to_sym
    tpl = :hero unless [:email, :hero, :simple, :email2].include? tpl
    file = 'user_mailer/welcome_email'
    @user = User.first
    render file, layout: "emails/#{tpl}"
    if params[:premail] == 'true'
      puts "\n!!! USING PREMAILER !!!\n\n"
      pre = Premailer.new(response_body[0],  warn_level: Premailer::Warnings::SAFE, with_html_string: true)
      reset_response
      # pre.warnings
      render text: pre.to_inline_css, layout: false
    end
  end

  def error
    redirect_to root_path if flash.empty?
  end

  def upcomingMatches
    @json = ScoreAPI.getUpcomingSeries
    @json = ScoreAPI.getSeries(params[:series_id]) if params[:series_id].present?
  end

  def location_disabled
  end

  def hello
  end

  def update_score
    message = params[:score]
    ac = WebsocketRails[:update].trigger(:update, message)
    render nothing: true
  end

  def ref
    redirect_to(new_user_registration_path, notice: @notice)
  end

  def privacy_policy
    render text: render_to_string(template: 'pages/privacy', layout: false)
  end

  private

    def load_referrer
      invite_code = params[:invite_code]
      if User::AFFILIATE_CODE_STRINGS.keys.include?(invite_code.to_sym)
        session[:referral_code] = invite_code
        @notice = "You are referred as #{ invite_code } promotional user."
      # if invite_code == User::SPORTZWIKI_REFERRAL_CODE_STRING
      #   session[:referral_code] = User::SPORTZWIKI_REFERRAL_CODE_STRING
      #   @notice = 'You are referred as sportzwiki promotional user.'
      else
        @referrer = User.find_by(id: invite_code)
        redirect_to(root_path, error: 'Referrer not found') and return unless @referrer
        session[:referrer_id] = @referrer.id
        @notice = "You are referred by #{ @referrer.username }."
      end
    end

    def allow_page_caching
      expires_in(5.minutes) unless Rails.env.development?
    end

end

