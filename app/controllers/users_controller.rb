# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  first_name             :string
#  last_name              :string
#  image_url              :string
#  email                  :string           default("")
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string
#  failed_attempts        :integer          default(0), not null
#  unlock_token           :string
#  locked_at              :datetime
#  created_at             :datetime
#  updated_at             :datetime
#  is_admin               :boolean
#  username               :integer
#  credits                :integer
#  phone                  :string           not null
#  date_of_birth          :date
#  state                  :string
#  country                :string           default("IN")
#  meta_info              :hstore
#  referred_by_id         :integer
#  referral_code          :string
#  is_spam_referrer       :boolean          default(FALSE)
#

class UsersController < ApplicationController
  load_and_authorize_resource

  def show
    redirect_to root_path
  end

  def account
    logger.info "---------eeeeeeeeeee------#{Rails.env}----------"
    @credits = current_user.banked_credits.order('created_at DESC').limit(10)
    @last_money_txn = current_user.money_transactions.order(created_at: :desc).limit(1).first
  end

  def past_games
    # match_ids = current_user.user_teams.played(current_user.id).pluck(:match_id).uniq
    # @matches = Match.where(id: match_ids).order('end_time DESC')
    # @matches = Match.completed
    @matches = current_user.matches.completed.limit(10)
    @top_5_contest = Contest.find_by(team_type: 'top_5_batsmen')
  end

  def update
    if current_user.update(user_params)
      redirect_to account_path, notice: 'Details updated successfully'
    else
      @credits = current_user.banked_credits.order('created_at DESC').limit(10)
      @last_money_txn = current_user.money_transactions.order(created_at: :desc).limit(1).first
      render :account
    end
  end

  private

  def user_params
    params.require(:user).permit(:first_name, :last_name, :username)
  end
end










