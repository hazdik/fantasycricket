class PaymentController < ApplicationController
  skip_authorization_check
  skip_before_action :verify_authenticity_token, only: [:callback, :success, :failure, :mock_callback, :mobile_callback]
  skip_before_action :authenticate_user!, only: [:mobile, :mobile_callback, :mock_callback]
  skip_before_action :system_down

  before_action :load_user_from_token, only: [:mobile]
  before_action :load_user_transaction, only: [:new, :wallet, :failure]
  before_action :check_validity, only: :add_to_wallet

  def add_to_wallet
    # check_validity
    credit_amount = params[:amount].to_i
    transaction_credit(0, credit_amount, credit_amount, "initiated")
    redirect_to new_payment_path(transaction_id: @user_transaction.token, gateway: 1)
  end

  def new
    @user_transaction.set_state
    request_params = Rails.application.config.settings.env.paytm.to_hash.symbolize_keys!
    request_params = Rails.application.config.settings.env.mock_paytm.to_hash.symbolize_keys! if params[:mode]=="test"
    @request_params = prepare_request_params(request_params)
    render :layout => false
  end

  def mobile
    # identify user from token
    @user_transaction.set_state
    # we can add mobile_paytm params setting here
    request_params = Rails.application.config.settings.env.mobile_paytm.to_hash.symbolize_keys!
    request_params = Rails.application.config.settings.env.mock_paytm.to_hash.symbolize_keys! if params[:mode]=="test"
    @request_params = prepare_request_params(request_params)
    render :layout => false
  end

  def callback
    is_healthy_response = verify_paytm_response(params)
    @user_transaction = UserTransaction.find_by(token: params[:ORDERID])
    @user_transaction.update_column(:payment_gateway_response, params)
    logger.info "response_params => #{params}"
    logger.info "is_healthy_response => #{is_healthy_response}"
    if is_healthy_response && params[:RESPCODE].eql?('01')
      # if params[:RESPCODE].eql?('01')
      # if @user_transaction.initiated?
      #   @league = League.from_transaction(@user_transaction)
      # else
      #   @league = @user_transaction.try(:league)
      # end
      @user_transaction.state = 'completed'
      @user_transaction.save!
      redirect_to account_path, notice: 'Money added successfully'
      # redirect_to success_payment_index_path(transaction_id: @user_transaction.token, league_id: @league.try(:id))
    else
      @user_transaction.update_column(:state, 'failed')
      redirect_to account_path, notice: 'Something went wrong. Please try again!'
      # redirect_to failure_payment_index_path(transaction_id: @user_transaction.token)
    end
  end

  def mobile_callback
    logger.info "params => #{params}"
    is_healthy_response = verify_paytm_response(params)
    @user_transaction = UserTransaction.find_by(token: params[:ORDERID])
    @user_transaction.update_column(:payment_gateway_response, params)
    logger.info "is_healthy_response => #{is_healthy_response}"
    if is_healthy_response && params[:RESPCODE].eql?('01')
      # if params[:RESPCODE].eql?('01')
      if @user_transaction.initiated?
        @league = League.from_transaction(@user_transaction)
      end
      if @league.try(:is_private).present?
        redirect_to "https://fantoss.com/success/#{ @user_transaction.user_team.try(:token) }"

      elsif @user_transaction.transaction_category == 'add_to_wallet'
        redirect_to "https://fantoss.com/redirectFromPaytm"

      else
        # redirect_to "https://fantoss.com/successJoined"
        redirect_to "https://fantoss.com/matches/#{@user_transaction.match_id}/viewteam?tid=#{params[:ORDERID]}"
      end
    else
      @user_transaction.update_column(:state, 'failed')
      redirect_to "https://fantoss.com/join/failed"
    end
  end

  def mock_callback
    is_healthy_response = verify_paytm_response(params)
    render :text => is_healthy_response
    # ut = UserTransaction.find_by(token: params[:ORDERID])
    # logger.info "response_params => #{params}"
    # logger.info "is_healthy_response => #{is_healthy_response}"
    # if is_healthy_response && params[:RESPCODE].eql?('01')
    #   ut.delete if ut.present?
    #   redirect_to "https://fantoss.com/successJoined?id=#{params[:ORDERID]}"
    # else
    #   redirect_to "https://fantoss.com/join/failed"
    # end
  end

  def wallet
    wallet_balance = 0
    if @user_transaction.params['entry'].to_i < League::MIN_ENTRY_FEE_FOR_REFERRAL_CREDIT
      wallet_balance = current_user.wallet.main_balance
    else
      wallet_balance = current_user.wallet.total_balance
    end
    ActiveRecord::Base.transaction do
      if wallet_balance >= @user_transaction.params['entry'].to_i
        @user_transaction.state = "initiated"
        @user_transaction.payment_gateway_response = { payment_mode: 'wallet', RESPCODE: '01'}
        @user_transaction.save!
        @league = League.from_transaction(@user_transaction)
        redirect_to success_payment_index_path(transaction_id: @user_transaction.token, league_id: @league.id)
      else
        @user_transaction.payment_gateway_response = { payment_mode: 'wallet balance insufficient', RESPCODE: '14112'}
        @user_transaction.save!
        redirect_to failure_payment_index_path(transaction_id: @user_transaction.token)
      end
    end
  end

  def success
    @user_transaction = UserTransaction.find_by(token: params[:transaction_id])
    @league = League.find(params['league_id'])
    if @user_transaction.state != 'completed'
      redirect_to root_path, alert: 'Transaction is not completed. Please try again.'
    end
  end

  def failure
    if @user_transaction.failed?
      redirect_to match_contest_user_team_league_index_path(match_id: @user_transaction.match, contest_id: Contest::TOP_5_CONTEST, user_team_id: @user_transaction.user_team)
    end
    @user_transaction.set_state('failed')
    # @user_transaction.notify_admin_about_failed_payment
    if( @user_transaction.payment_gateway_response['RESPCODE']== "142" ||  @user_transaction.payment_gateway_response['RESPCODE']=="14112")
      flash[:error] = 'Transaction failed. please try again.'
      redirect_to match_contest_user_team_league_index_path(match_id: @user_transaction.match, contest_id: Contest::TOP_5_CONTEST, user_team_id: @user_transaction.user_team)
    end
  end

  private

  def check_validity
    if (params[:amount].to_i == 0) || (params[:amount].to_i < 0) || (params[:amount].to_i > 10000)
      #Bad Request
      redirect_to account_path, alert: 'Invalid amount!'
    end
  end

  def transaction_credit(total_amount, credit_amount, txn_amount, state)
    params[:txn_amount] = txn_amount
    @user_transaction = current_user.user_transactions.create!(total_amount: total_amount.to_i,
                                                         balance_amount: credit_amount.to_i,
                                                         state: state,
                                                         transaction_category: "add_to_wallet",
                                                         params: params
                                                        )
  end

  def load_user_transaction
    @user_transaction = UserTransaction.find_by(token: params[:transaction_id])
    if @user_transaction.state=='completed'
      redirect_to match_url(@user_transaction.match)
    end
    unless @user_transaction
      redirect_to root_path, alert: 'Transaction is not completed. Please try again.'
    end
  end

  def load_user_from_token
    @user_transaction = UserTransaction.find_by(token: params[:transaction_id])
    # user       = @user_transaction.user
    if @user_transaction.state=='completed'
      render json: {responseCode: 404, responseMessage:  "Transaction is already completed" }
    end
    unless @user_transaction
      render json: {responseCode: 404, responseMessage:  "Transaction is not found. Please try all over again." }
    end
  end

  def prepare_request_params(request_params = {})
    merchant_key = request_params.delete(:merchant_key)
    @gateway_url = request_params.delete(:gateway_url)
    user         = @user_transaction.user
    request_params.merge!({
      ORDER_ID: @user_transaction.token,
      CUST_ID: @user_transaction.user_id,
      TXN_AMOUNT: @user_transaction.params['txn_amount'],
      MSISDN: user.phone,
      EMAIL: user.email
    })
    checksum = generate_hash(merchant_key, request_params)
    checksum_verify = verify_hash(merchant_key, checksum, request_params)
    request_params.merge!({CHECKSUMHASH: checksum})
    logger.info "request_params => #{request_params.sort_by{|k,v| k}.to_h}"
    logger.info "checksum_verify => #{checksum_verify}"
    request_params.sort_by{|k,v| k}.to_h
  end


  def verify_paytm_response(paytmresponse={})
    mkey = Rails.application.config.settings.env.paytm.merchant_key.to_s
    response_params = paytmresponse.symbolize_keys
    response_params.delete(:action)
    response_params.delete(:controller)
    checksum = response_params.delete(:CHECKSUMHASH)
    logger.info "mkey -> #{mkey}"
    logger.info "response_params -> #{response_params}"
    logger.info "checksum -> #{checksum}"
    SecureHash.new_pg_verify_checksum(response_params, checksum, mkey)
  end


  def generate_hash(key, params={})
    SecureHash.new_pg_checksum(params, key).gsub("\n",'')
  end

  def verify_hash(key, checksum, params={})
    SecureHash.new_pg_verify_checksum(params, checksum, key)
  end

  # def resolve_payment_gateway
  #   @payment_gateway = UserTransaction::PAYMENT_GATEWAYS.key(params[:gateway].to_i)
  #   unless @payment_gateway
  #     redirect_to root_path, alert: 'Invalid Payment Gateway'
  #   end
  # end

end
