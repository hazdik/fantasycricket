class Api::V1::UsersController < ApplicationController
  # skip_before_action :authenticate_user!, except: ['show']
  skip_before_action :authenticate_user_from_token!, only: [:send_otp, :verify_user_otp, :signin_fb, :getTestingResults]


  def show
  	@user = current_user
  end

  def send_otp
  	# send otp and respond with VerificationId
    @response = FoneVerifyAPI.send_verification_code(params[:phone])
  end

  def getTestingResults
    user = User.find_by_phone(params[:m_no])
    if user.present?
      return render json: {"result": "success", "data": user.registration_info }
    else
      return render json: {"result": "failure", "data": "Please enter a valid mobile number" }
    end
  end

  # Sign in using phone
  def verify_user_otp
    # @user = User.find_by(phone: params['user']['phone'])
    # if @user
    #   @is_valid = true
    #   sign_in(@user)
    # end
    # return

    verify_otp(params[:user][:verificationId],params[:user][:otp])
  	if @is_valid
      if params[:user][:provider] == "phone"
        @phone ||= params[:user]["phone"]
        @user = check_existence(@phone)
        @new_user = true
	  		if @user.present?
	  			# send response with token
	  			sign_in @user
          @new_user = false
	  		else
	  			# create user and send access token
          return error_message("Please accept terms of services") if params[:user][:terms_of_service] != "on"
          @user = User.new(phone: @phone, terms_of_service: params[:user][:terms_of_service], params: params)
          #transaction of money to referrer and current user
          find_referrer(params[:user][:referrer_id]) if params[:user][:referrer_id].present?
          @user.save!
          sign_in @user
	  		end
  	  elsif params[:user][:provider] == "facebook"
  	    	# create user(with email and phone number)
          @phone = params[:user][:phone]
          @user = check_existence(@phone)
          @new_user = true
            if @user.present?
              @user.update(email: params[:user][:email] , image_url: params[:user][:image_url])
              @user.authentications.create(authentication_params) unless Authentication.find_by(proid: params[:user][:proid]).present?
              sign_in @user
              @user.update_login_counter(params[:login_variation])
              @new_user = false
              return current_user
            end
          return error_message("Please accept terms of services") if params[:user][:terms_of_service] != "on"
          @user = User.new(phone: @phone , email: params[:user][:email], terms_of_service: params[:user][:terms_of_service], params: params)
          find_referrer(params[:user][:referrer_id]) if params[:user][:referrer_id].present?
          @user.save!
  	    	# create user Authentication
          @user.authentications.create(authentication_params)
          @user.update_registration_counter(params[:login_variation])
          sign_in @user


  	    	# send access_token

      elsif params[:user][:provider] == "google_oauth2"
          # create user(with email and phone number)
          @phone = params[:user][:phone]
          @user = check_existence(@phone)
          @new_user = true
            if @user.present?
              unless @user.image_url.present?
                @user.image_url = params[:user][:image_url]
              end

              @user.update(email: params[:user][:email])
              @user.authentications.create(authentication_params) unless Authentication.find_by(proid: params[:user][:proid]).present?
              @user.update_login_counter(params[:login_variation])
              sign_in @user
              @new_user = false
              return current_user
            end
          return error_message("Please accept terms of services") if params[:user][:terms_of_service] != "on"
          @user = User.new(phone: @phone , email: params[:user][:email], terms_of_service: params[:user][:terms_of_service], params: params)
          find_referrer(params[:user][:referrer_id]) if params[:user][:referrer_id].present?
          @user.save!
          # create user Authentication
          @user.authentications.create(authentication_params)
          @user.update_registration_counter(params[:login_variation])
          sign_in @user

          # send access_token

      else
        render json: {responseCode: 501 , responseMessage: "Provider not present"}
      end
    end

  end

  # Signup/Signin using Facebook
  def signin_fb
    return render json: {responseCode: 501, responseMessage: "Facebook provider id not present!!!"} unless params[:user][:proid].present?
  	if user_auth(params[:user][:proid], 'facebook').present?
      @user.update_login_counter(params[:login_variation])
      sign_in @user
  	end
  end

  def user_details
    render json: {
      responseCode: 200,
      responseMessage: "user details found",
      won: current_user.total_won ,
      total_matches_played: current_user.leagues.count ,
      wallet_balance: current_user.wallet.total_balance,
      main_balance: current_user.wallet.main_balance,
      referral_balance: current_user.wallet.referral_balance,
      user_image: current_user.avatar_image,
      username: current_user.display_name,
      user: current_user,
      wallet: current_user.wallet,
      referred: current_user.referred_by.present?,
      paid_leagues_count: current_user.leagues.paid.count
    }
  end

  def user_referral_details
    referrals = []
    date = Date.new(2017,12,28).beginning_of_day

    current_user.referrals.where('created_at > ?', date).each do |referral_user|
      referral_obj = {}
      referral_obj[:mobile] = referral_user.phone
      if referral_user.leagues.paid.count >= 0
        referral_singup_money = 10
      end
      if referral_user.leagues.paid.count >= 1
        @entry_fee_credit = (referral_user.leagues.paid.first.entry_fee * 10)/100
        @referral_credit = @entry_fee_credit < 10 ? Rails.application.config.settings.referral_credit : @entry_fee_credit
        referral_league_money = @referral_credit.to_i
      elsif referral_user.leagues.paid.count == 0
        referral_league_money = 0
      end
      referral_obj[:refer_money_signup] = referral_singup_money
      referral_obj[:refer_money_paid] = referral_league_money
      referrals << referral_obj
    end
    render json: { referrals: referrals , responseCode: 200 }
  end

  def update
    if current_user.update(user_params)
      render json: {
        responseCode: 200,
        responseMessage: 'Details updated successfully',
        won: current_user.total_won ,
        total_matches_played: current_user.leagues.count ,
        wallet_balance: current_user.wallet.total_balance,
        main_balance: current_user.wallet.main_balance,
        referral_balance: current_user.wallet.referral_balance,
        user_image: current_user.avatar_image,
        username: current_user.display_name,
        user: current_user,
        wallet: current_user.wallet
      }
    else
      render json: {
        responseCode: 403,
        responseMessage: 'Some error occurred',
        errors: current_user.errors,
        won: current_user.total_won ,
        total_matches_played: current_user.leagues.count ,
        wallet_balance: current_user.wallet.total_balance,
        main_balance: current_user.wallet.main_balance,
        referral_balance: current_user.wallet.referral_balance,
        user_image: current_user.avatar_image,
        username: current_user.display_name,
        user: current_user,
        wallet: current_user.wallet
      }
    end
  end


private

  def user_params
    params.require(:user).permit(:username, :image_url)
  end

  def error_message(message)
    render json: {responseCode: 501 , responseMessage: message }
  end



protected

  def authentication_params
    params.require(:user).permit(:provider ,:proid ,:token ,:expires_at ,:image_url ,:username )
  end

  def find_referrer(referrer_id)
    # if User::AFFILIATE_CODE_STRINGS.values.include?(referrer_id)
    #   @user.referral_code = User::AFFILIATE_CODE_STRINGS.key(referrer_id)
    # else
      # referrer = User.find_by(phone: referrer_id
      referrer = User.find_by(id: referrer_id)
      if referrer.present?
        @user.referred_by_id = referrer.id
      end
    # end
  end

  def user_auth(fb_uid, provider)
   auth =  Authentication.find_by(proid: fb_uid, provider: provider)
   @user =  auth.user if auth
  end

  def verify_otp(ver_id, otp)
    response = FoneVerifyAPI.verify_number(ver_id,otp)
    set_message(response)
  end

  def set_message(response)
    @is_valid   = ( %w(200 703).include?(response["responseCode"]) ) ? true : false
    @retry_flag = (response["responseCode"] == "705") ? true : false
    @wrong_otp = (response["responseCode"] == "702") ? true : false
    @phone = response["phone"] if @is_valid
  end


  def check_existence(phone)
    User.find_by(phone: phone)
  end

end
