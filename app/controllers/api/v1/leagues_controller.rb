class Api::V1::LeaguesController < ApplicationController
  skip_before_action :authenticate_user_from_token!, only: :show
  before_action :find_league, only: :show

  def show
    render json: {
      responseCode: '200',
      responseMessage: 'Success',
      league: @league,
      user_teams: @league.user_teams,
      users: @league.users,
      private_user: @user_team.try(:user),
      match: @league.match.as_json.merge(
        teams: {
          home_team: @league.match.home_team.as_json.merge(flag_path: @league.match.home_team.flag_path),
          away_team: @league.match.away_team.as_json.merge(flag_path: @league.match.away_team.flag_path)
        }
      )
    }.to_json
  end

  private

  def find_league
    if params[:id].present?
      if params[:id].integer?
        @league = League.find_by(id: params[:id])
      else
        @user_team = UserTeam.find_by(token: params[:id])
        @league = @user_team.try(:league)
      end
    end

    unless @league
      render(json: { responseCode: '404', responseMessage: 'Not Found' }.to_json) and return
    end
    if @league && (@league.match.ongoing? || @league.match.completed?)
      render(json: { responseCode: '403', responseMessage: 'Oops! Link expired!' }.to_json)
    end
  end

end
