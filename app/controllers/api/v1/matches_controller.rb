class Api::V1::MatchesController < ApplicationController
  skip_before_action :authenticate_user_from_token!, except: [:old_matches]
  before_filter :load_current_user,  only: [:upcoming_matches, :live_matches, :leaderboards]
  before_filter :load_match,         only: [:players, :leaderboards]

  def upcoming_matches
    @upcoming_matches = Match.display_matches
  end

  def live_matches
    @live_matches = Match.ongoing
  end

  def players
    @players    = @match.all_batsmen.where(participant_team_id: [@match.home_team_id, @match.away_team_id]) if @match.present?
    @contest_id = Contest.find_by(team_type: 'top_5_batsmen').try(:id)
  end

  def leaderboards
    user_teams_per_page = 50
    if @match
      @user_teams = @match.user_teams.order(totalscore: :desc, created_at: :desc)
      # @user_teams = @match.user_teams.order(totalscore: :desc, created_at: :desc).page(params[:page]).per(user_teams_per_page)
      # current_page_top_rank = params[:page].to_i * user_teams_per_page
      @user_teams_data = @user_teams.map.with_index do |ut, index|
        # ut.attributes.merge(user_details: ut.user.as_json(only: [:username, :image_url]), position: (current_page_top_rank + index))
        ut.attributes.merge(user_details: ut.user.as_json(only: [:username, :image_url]), position: index + 1)
      end
      if @current_user
        @current_user_teams_data = @user_teams_data.select { |ut| ut['user_id'] == @current_user.id }
      else
        @current_user_teams_data = []
      end
    end
  end


  def old_matches
    matches = current_user.matches.completed.limit(10)
    if matches.present?
      render json: {responseCode: 200,
                    responseMessage:  "Showing your old matches",
                    matches: matches.map{|m|  m.attributes.merge(
                      is_upcoming: !(m.has_squad_data?) ,
                      is_team_present: ( current_user.blank? ? false : (m.user_ids.include?(current_user.id.to_i)) ),
                      teams: {
                        home_team: {name: m.home_team.name, short_name: m.home_team.short_name },
                        away_team: {name: m.away_team.name, short_name: m.away_team.short_name }
                        },
                      playing_members: {
                        count: m.user_teams.count + m.id + rand(100),
                        images: m.user_playing_imgs
                        } )
                    } }
    else
      render json: {responseCode: 404, responseMessage:  "No match found", matches: matches}
    end

  end


  private

  def load_match
    @match = Match.find_by(id: params[:match_id])
  end

  def load_current_user
    @current_user = User.find_by(authentication_token: request.headers[:TOKEN])
  end

end
