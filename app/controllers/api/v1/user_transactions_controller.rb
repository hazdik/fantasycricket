class Api::V1::UserTransactionsController < ApplicationController
  before_action :avoid_fraud_users, only: [:join_league]
  before_action :ensure_maximum_add_to_wallet_amount, only: :add_to_wallet


  def add_to_wallet
    check_validity
    credit_amount = params[:amount].to_i
    transaction_credit(0, credit_amount, credit_amount, "initiated")
  end

  def join_league
    @joined_league = false
    @users_coupon = current_user.users_coupons.applied.find_by(id: params[:users_coupon_id])
    coupon_value = @users_coupon ? @users_coupon.value_for(params[:entry].to_i) : 0

    if coupon_value <= params[:entry].to_i
      total_amount = params[:entry].to_i - coupon_value
    else
      total_amount = 0
    end
    if total_amount < League::MIN_ENTRY_FEE_FOR_REFERRAL_CREDIT
      wallet_balance = 0
      if current_user.wallet.main_balance > 0
        wallet_balance = current_user.wallet.main_balance
      end
    else
      wallet_balance = current_user.wallet.total_balance
    end

    if wallet_balance >= total_amount
      resp = { payment_mode: 'wallet', RESPCODE: '01' }
      create_user_transaction(total_amount, 0, 0, "initiated", resp)
      calculate_wallet_usage_for(wallet_balance, total_amount)
      League.from_transaction(@user_transaction)
      @joined_league = true
    else
      @wallet_insufficient = true
    #   #if wallet_balance < league total amount
    #   resp = { payment_mode: 'initiate pay from paytm', RESPCODE: '-' }
    #   credit_amount = (total_amount - wallet_balance)
    #   create_user_transaction(total_amount, credit_amount, credit_amount, "initiated", resp)
    #   calculate_wallet_usage_for(wallet_balance, total_amount)
    end

  end

  def transaction_history
    @transactions = current_user.banked_credits.order('created_at DESC').limit(20)
  end

  def gettransactions
    all_transactions = current_user.banked_credits.order('created_at DESC')
    total_trans_count = all_transactions.count
    transactions = all_transactions.offset(params[:offset]).limit(15)
    render json: {responseCode: 200 ,
                  responseMessage: "Transactions result",
                  transactions: transactions,
                  total_trans_count: total_trans_count,
                  start: params[:start]
                }
  end

private

  def ensure_maximum_add_to_wallet_amount
    todays_add_to_wallet_amount = current_user.user_transactions.completed.where(transaction_category:  'add_to_wallet'
                                                                                ).where(created_at: Date.today.beginning_of_day..Date.today.end_of_day
                                                                                ).sum(:balance_amount)

    todays_add_to_wallet_amount_after_this_transaction = todays_add_to_wallet_amount + params[:amount].to_i.abs
    if todays_add_to_wallet_amount_after_this_transaction > User::MAXIMUM_ADD_TO_WALLET_AMOUNT
      render json: {
        responseCode: '500',
        responseMessage: "You are exceeding daily add to wallet amount limits, kindly ensure that your daily added money to wallet should not exceed #{User::MAXIMUM_ADD_TO_WALLET_AMOUNT}.",
      }.to_json
      return
    end

  end

  def create_user_transaction(total_amount ,credit_amount, txn_amount, state, response)
    params[:txn_amount] = txn_amount
    @user_transaction = current_user.user_transactions.where(
      state: state,
      transaction_category: 'join_league',
      user_team_id: params[:user_transaction][:user_team_id],
      match_id: params[:user_transaction][:match_id],
    ).first

    if @user_transaction
      @user_transaction.total_amount = total_amount
      @user_transaction.balance_amount = credit_amount
      @user_transaction.params = params
      @user_transaction.payment_gateway_response = response
    else
      @user_transaction = current_user.user_transactions.create!(total_amount: total_amount ,
                                                         balance_amount: credit_amount,
                                                         state: state,
                                                         transaction_category: "join_league",
                                                         params: params,
                                                         user_team_id: params[:user_transaction][:user_team_id],
                                                         match_id: params[:user_transaction][:match_id],
                                                         payment_gateway_response: response)
    end
  end

  def transaction_credit(total_amount, credit_amount, txn_amount, state)
    params[:txn_amount] = txn_amount
    @user_transaction = current_user.user_transactions.create!(total_amount: total_amount.to_i,
                                                         balance_amount: credit_amount.to_i,
                                                         state: state,
                                                         transaction_category: "add_to_wallet",
                                                         params: params
                                                        )
  end


  def check_validity
    if (params[:amount].to_i == 0) || (params[:amount].to_i < 0) || (params[:amount].to_i > 10000)
      #Bad Request
      return render json: {responseCode: 404 , responseMessage: "Please enter a valid amount."}
    else
      true
    end
  end

  def calculate_wallet_usage_for(wallet_balance, transaction_amount)
    # Calculating Wallet Balance usage details per join league
    if transaction_amount <= 0
      @user_transaction.params["total_wallet_balance_used"] = 0
      @user_transaction.params["referral_wallet_balance_used"] = 0
      @user_transaction.params["main_wallet_balance_used"] = 0
      return
    end

    if transaction_amount < League::MIN_ENTRY_FEE_FOR_REFERRAL_CREDIT
      # Leagues in which referral money is not allowed
      referral_wallet_balance_used = 0

      if current_user.wallet.main_balance >= transaction_amount
        main_wallet_balance_used = transaction_amount
      else
        main_wallet_balance_used = current_user.wallet.main_balance
      end

    else
      if current_user.wallet.referral_balance >= transaction_amount
        referral_wallet_balance_used = transaction_amount
        main_wallet_balance_used = 0
      else
        referral_wallet_balance_used = current_user.wallet.referral_balance
        main_wallet_balance_used = wallet_balance - referral_wallet_balance_used
      end
    end
    @user_transaction.params["total_wallet_balance_used"] = referral_wallet_balance_used + main_wallet_balance_used
    @user_transaction.params["referral_wallet_balance_used"] = referral_wallet_balance_used
    @user_transaction.params["main_wallet_balance_used"] = main_wallet_balance_used
    @user_transaction.save!
  end


end
