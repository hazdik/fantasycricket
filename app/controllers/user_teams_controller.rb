# == Schema Information
#
# Table name: user_teams
#
#  id               :integer          not null, primary key
#  user_id          :integer
#  totalscore       :integer          default(0)
#  has_won          :boolean          default(FALSE)
#  created_at       :datetime
#  updated_at       :datetime
#  match_id         :integer
#  league_id        :integer
#  rank             :integer
#  token            :string
#  transaction_data :hstore
#  captain_id       :integer
#  contest_id       :integer
#

class UserTeamsController < ApplicationController
  include LoadResourceConcern

  # load_and_authorize_resource
  skip_authorization_check

  before_filter :load_match, except: [:error]
  before_filter :load_contest, except: [:error]
  before_filter :load_user_team, only: [:edit, :update]
  before_filter :ensure_editability, only: [:edit, :update]
  before_filter :load_players, only: [:new, :create, :edit, :update]

  def index
    @user_teams = @match.user_teams.where(user_id: current_user.id)
    # @user_teams = @match.user_teams
    redirect_to action: 'new' if @user_teams.empty?
  end

  def new
    # @user_transaction = UserTransaction.find(params[:user_transaction_id]) if params[:user_transaction_id]
    @user_team = @match.user_teams.build
  end

  def create
    @user_team = @match.user_teams.build(user_team_permitted_params)

    if @user_team.save
      flash[:notice] = "You are now all set to play."
      # redirect_to match_contest_user_teams_path(match_id: @match, contest_id: @contest)
      update_user_transaction_and_redirect_to_payment_page!
    else
      flash[:error] = @user_team.errors.full_messages.join("<br>").html_safe
      render action: 'n'
    end
  end

  def edit
  end

  def update
    if @user_team.update(user_team_permitted_params)
      if @user_team.league_id.present?
        redirect_to leaderboard_match_path(@user_team.match), notice: 'User Team updated successfully'
      else
        redirect_to match_contest_user_team_league_index_path(match_id: @match, contest_id: @contest, user_team_id: @user_team)
      end
    else
      flash[:error] = @user_team.errors.full_messages.join("<br>").html_safe
      render action: 'edit'
    end
  end

  def n
    @user_team = @match.user_teams.build
  end

  def error
    redirect_to root_path if flash.empty?
  end

private
  def load_players
    if @contest.top_11_players?
      @players = @match.all_players.includes(:participant_team).order(:participant_team_id)
    else
      @players = @match.all_batsmen.includes(:participant_team, :player_scores).order(:participant_team_id)
    end
  end

  def user_team_permitted_params
    _params = params.require(:user_team)
      .permit(:match_id, :captain_id, :vice_captain_id, batsman_ids: [],
        bowler_ids: [], allrounder_ids: [], wicketkeeper_ids: [])
      .merge({ contest_id: @contest.id, user_id: current_user.id })
    _params.merge!({ ut_token: params[:ut_token] }) if params[:ut_token].present?
    _params
  end

  def ensure_editability
    unless @user_team.allow_edit?
      flash[:error] = "You can not edit this user team."
      redirect_to match_contest_user_teams_path(match_id: @match, contest_id: @contest)
    end
  end

  def load_contest
    @contest = Contest.find_by(id: params[:contest_id])
    unless @contest
      flash[:error] = "No Contest found."
      redirect_to after_sign_in_path_for(current_user)
    end
  end

  def load_user_team
    # return current_user.present? && current_user.user_teams.present? && current_user.user_teams.find_by_match_id_and_league_id(params[:match_id], params[:id])
    @user_team = @match.user_teams.find_by(id: params[:id], user_id: current_user.id)
    # @user_team = current_user.user_teams.find_by_match_id_and_league_id(params[:match_id], params[:id])
    unless @user_team
      flash[:error] = "No User team found."
      redirect_to after_sign_in_path_for(current_user)
    end
  end

  def update_user_transaction_and_redirect_to_payment_page!
    begin
      @user_transaction = current_user.user_transactions.find_by(token: user_team_permitted_params["ut_token"])
      league = League.find(@user_transaction.params["league_id"])
      @user_transaction.update_columns(match_id: @match.id, user_team_id: @user_team.id, state: 'created',
                                       params: {
                                        match_id: @match.id,
                                        contest_id: @contest.id,
                                        league_id: league.id,
                                        entry: league.entry_fee,
                                        prize: league.prize_money,
                                        user_team_id: @user_team.id,
                                        member: league.limit,
                                        is_private: league.is_private,
                                      })
      if(league.entry_fee==0)
        redirect_to join_for_free_match_contest_user_team_league_index_path(@match, @contest, @user_team, prize: league.prize_money, entry: league.entry_fee, member: league.limit, is_private: league.is_private, new_team: 'challenge')
      else
        redirect_to join_match_contest_user_team_league_index_path(@match, @contest, @user_team, prize: league.prize_money, entry: league.entry_fee, member: league.limit, is_private: league.is_private , new_team: 'challenge')
      end
    rescue
      redirect_to match_contest_user_team_league_index_path(match_id: @match, contest_id: @contest, user_team_id: @user_team, new_team: '1')
    end
  end
end
