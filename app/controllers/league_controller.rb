class LeagueController < ApplicationController
  include LoadResourceConcern

  # load_and_authorize_resource
  skip_authorization_check
  skip_before_action :authenticate_user!, except: [:index, :join, :join_for_free, :create]
  before_filter :load_match, except: [:create, :error, :join_via_code]
  # before_filter :load_contest, except: [:create, :error, :payment_status]
  before_filter :load_user_team, only: [:index, :join, :invite_friends, :play_with_friends, :join_for_free]
  before_filter :ensure_can_join_league, only: [:index, :join, :join_for_free]
  before_filter :ensure_free_league, only: [:join_for_free]
  before_filter :ensure_valid_league, only: [:join]
  # before_filter :get_ut, only: [:payment_status]

  before_filter :find_or_create_user_transaction, only: [:join, :join_for_free]
  before_filter :adjust_league_joining_amount, only: :join
  before_filter :load_transaction, only: :create
  before_action :load_league_via_code, only: :join_via_code
  before_filter :load_user_transaction, only: :join_via_code
  before_filter :load_user_by_referral_code, only: :join_via_code

  def index
    @leagues_created = @match.leagues
    # @invitable_friends = FacebookService.new(current_user).invitable_friends
  end

  def join
  end

  def join_for_free
    # raise
    @league = League.from_transaction(@user_transaction)
    debugger
  end

  def play_with_friends
    @invitable_friends = FacebookService.new(current_user).invitable_friends
  end

  def invite_friends
    ActiveRecord::Base.transaction do
      league = @match.leagues.create!({ name: 'private', contest: @contest,
        is_private: true, is_multiple_allowed: true })
      league.challenges.create!(challenge_params)
    end
    flash[:notice] = "Friends invited successfully."
    redirect_to match_contest_user_teams_path(match_id: @match, contest_id: @contest)
  end

  def show
    @ut = get_ut
    if @ut.present?
      redirect_to match_league_user_team_path(@match, @league, @ut)
    else
      redirect_to new_match_league_user_team_path(@match, @league)
    end
  end

  def error
    redirect_to root_path if flash.empty?
  end

  def create
    @league = League.from_transaction(@user_transaction)
    respond_to do |format|
      format.js
    end
  end

  def join_via_code
    session["referring_user"]           = @referring_user.try(:id)
    if @referring_user
      session["referal_reward_value"]     = @league.referral_value
      # session["referal_reward_value"]     = 0  if @referring_user.is_spam_referrer
    end
    # session["referal_reward_value"]     = 0  if @league.match.start_time.past?

    session["user_return_to"]           = join_via_code_path(invite_code: @league.invite_code)
    session["user_return_to_timestamp"] = Time.now.utc.to_i

    if @league.entry_fee==0
      page_title = "Invitation to join my free league for #{@league.match.team_names} match on fantoss."
    else
      page_title = "Invitation to join my #{@league.entry_fee} rs. league, in #{@league.match.team_names} match, for #{@league.prize_money} Rs. prize money on fantoss."
    end
    prepare_meta_tags(title: page_title, og: {title: page_title})
  end

private

  def find_or_create_user_transaction
    @user_transaction = UserTransaction.where(user_team_id: params[:user_team_id])
                                       .where(match_id: params[:match_id])
                                       .where(user_id: current_user.id)
                                       .where(payment_gateway_response: nil)
                                       .where(state: 'created')
                                       .where(transaction_category: 'join_league')
                                       .where("params -> 'entry' = '#{params[:entry]}' and
      params -> 'prize' = '#{params[:prize]}' and
      params -> 'member' = '#{params[:member]}'").first

    unless @user_transaction
      @user_transaction = current_user.user_transactions
                                      .where(match_id: params[:match_id],
                                              user_team_id: params[:user_team_id],
                                              state: 'created', transaction_category: 'join_league').
                                      first_or_create
      @user_transaction.params = params
    end
  end

  def load_user_transaction
    return if !current_user
    @user_transaction = current_user.user_transactions.where(state: 'initiated', match_id: @league.match_id).where("params -> 'league_id' = '#{@league.id}'").first
    unless @user_transaction
      @user_transaction = current_user.user_transactions.create(state: 'initiated', match_id: @league.match_id, params: { league_id: @league.id })
    end
  end

  def ensure_can_join_league
    unless @user_team.can_join_league?
      flash[:error] = "You can not select league now for this user team."
      redirect_to match_url(@match)
    end
  end

  def ensure_valid_league
    logger.info "league prize money : #{League::PRIZE_AGAINST_FEE[params["entry"].to_s.to_sym].to_i} == #{params["prize"].to_i}"
    entry = params['entry'].to_s
    prize = params['prize'].to_i
    member = params['member'].to_s
    unless (
      (
        League::PAID_LEAGUE_ENUM.include?(entry) &&
        League::PRIZE_AGAINST_FEE[entry.to_sym] == prize &&
        League::EVEN_LEAGUE_SIZE.include?(member)
      ) ||
      (
        League::ODD_PAID_LEAGUE_ENUM.include?(entry) &&
        League::ODD_PRIZE_AGAINST_FEE[entry.to_sym] == prize &&
        League::ODD_LEAGUE_SIZE.include?(member)
      )
    )
      flash[:error] = "There is no such league with prize amount #{params["prize"]}. Please don't play around."
      redirect_to match_contest_user_team_league_index_path(match_id: @match, contest_id: Contest::TOP_5_CONTEST, user_team_id: @user_team)
    end
  end

  def ensure_free_league
    unless params["entry"].to_i==0 &&  params["prize"].to_i==0
      flash[:error] = "There is no such league with prize amount #{params["prize"]}. Please don't play around."
      redirect_to match_contest_user_team_league_index_path(match_id: @match, contest_id: Contest::TOP_5_CONTEST, user_team_id: @user_team)
    end
  end

  def get_ut
    return current_user.present? && current_user.user_teams.present? && current_user.user_teams.find_by_match_id_and_league_id(params[:match_id], params[:id])
  end

  def challenge_params
    {
      user: current_user,
      fb_request_id: params[:request_id],
      challenged_count: params[:request_to].split(",").count,
      friends_challenged_ids: params[:request_to].split(","),
    }
  end

  def load_transaction
    @user_transaction = UserTransaction.find_by(token: params[:transaction_id])
    unless @user_transaction
      redirect_to root_path, alert: 'User Transaction not found.'
    end
  end

  def load_league_via_code
    @league = League.find_by(invite_code: params[:invite_code])
    redirect_to root_path, alert: 'League not found.' unless @league
  end

  def load_user_by_referral_code
    @referring_user = User.find_by(referral_code: params[:ref]) if params[:ref]
  end

  def adjust_league_joining_amount
    transaction_amount = @user_transaction.params['entry'].to_i

    if params['entry'].to_i < League::MIN_ENTRY_FEE_FOR_REFERRAL_CREDIT
      wallet_balance, ref_bal  = 0, 0
      if current_user.wallet.main_balance > 0
        wallet_balance = current_user.wallet.main_balance
      end
    else
      wallet_balance = current_user.wallet.total_balance
      ref_bal = current_user.wallet.referral_balance
    end

    ################################################################
    ### Calculating Wallet Balance usage details per join league ###
    ################################################################
    if transaction_amount <= 0
      @user_transaction.params["total_wallet_balance_used"] = 0
      @user_transaction.params["referral_wallet_balance_used"] = 0
      @user_transaction.params["main_wallet_balance_used"] = 0
    else
      if params['entry'].to_i < League::MIN_ENTRY_FEE_FOR_REFERRAL_CREDIT
        # Leagues in which referral money is not allowed
        referral_wallet_balance_used = 0

        if current_user.wallet.main_balance >= transaction_amount
          main_wallet_balance_used = transaction_amount
        else
          main_wallet_balance_used = current_user.wallet.main_balance
        end

      else
        if current_user.wallet.referral_balance >= transaction_amount
          referral_wallet_balance_used = transaction_amount
          main_wallet_balance_used = 0
        else
          referral_wallet_balance_used = current_user.wallet.referral_balance
          main_wallet_balance_used = wallet_balance - referral_wallet_balance_used
        end
      end
      @user_transaction.params["total_wallet_balance_used"] = referral_wallet_balance_used + main_wallet_balance_used
      @user_transaction.params["referral_wallet_balance_used"] = referral_wallet_balance_used
      @user_transaction.params["main_wallet_balance_used"] = main_wallet_balance_used
    end
    #################################################################

    if wallet_balance >= params['entry'].to_i
      @user_transaction.debit_amount = params['entry'].to_i
      transaction_amount = 0
    else
      transaction_amount -= wallet_balance
      @user_transaction.debit_amount = wallet_balance
    end
    # @user_transaction.params["is_credits_used"] = transaction_amount.zero?
    @user_transaction.params["has_sufficient_balance"] = transaction_amount.zero?
    @user_transaction.params["wallet_balance"] = wallet_balance
    @user_transaction.params["referral_balance"] = ref_bal
    @user_transaction.params["txn_amount"] = transaction_amount
    @user_transaction.credit_amount = 0
    @user_transaction.save!
    # if current_user.is_eligible_for_promo(@match)
    #   transaction_amount -= current_user.credits
    #   @user_transaction.params["promo_code"] = "25"
    # end
  end
end
