module UserTeamHelper
  def players_for_select(ut)
    ut.players.collect { |p| [p.decorate.name_with_details, p.id] }
  end
end
