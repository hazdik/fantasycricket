module OperaHelper
  def total_transaction(total_opera_transaction, no_user_for_month, l, start_date, end_date)
    @hash_total = {}
    if l == "first"
      (@start_date..@end_date).each do |date|
        total_that_month = 0
        League::ENTRY_FEE_ENUM.each do |entry_fee|
          total_that_month += (@total_opera_transaction[[date, entry_fee.to_i]].to_f * entry_fee.to_i)
          @hash_total[date] = total_that_month
        end
      end
    else
      no_user_for_month.each do |key, val|
        total_that_month = 0
        League::ENTRY_FEE_ENUM.each do |entry_fee|
          total_that_month += (@league_played_month[[key, entry_fee.to_i]].to_f * entry_fee.to_i)
          @hash_total[key] = total_that_month
        end
      end
    end
    return @hash_total
  end
end

