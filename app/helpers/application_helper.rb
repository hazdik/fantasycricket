
module ApplicationHelper
  include CommonHelper

  # Render a partial only one time.
  #
  # Useful for rendering partials that require JavaScript like Google Maps
  # where other views may have also included the partial.
  def render_once(view, *args, &block)
    @_render_once ||= {}
    if @_render_once[view]
      nil
    else
      @_render_once[view] = true
      render(view, *args, &block)
    end
  end

  def user_avatar user
    if user.image.present?
      image_tag user.image_url :thumbnail
    else
      # Assuming you have a default.jpg in your assets folder
      image_tag 'default.jpg'
    end
  end

  def test_selection(ut, player)
    if ut.present? && ut[:answer1].present?
      return ut[:answer1].include? (player.id).to_s
    end
    return false
  end

  def active_class?(test_path)
    return 'active' if request.request_uri == test_path
    ''
  end

  def nav_link(link_path, no_turbolink=false, &block)
    class_name = current_page?(link_path) ? 'active col-md-6 col-xs-6' : 'col-md-6 col-xs-6'

    content_tag(:li, :class => class_name) do
      if no_turbolink
        link_to link_path, :"data-no-turbolink" => true do
          block.call
        end
      else
        link_to link_path do
          block.call
        end
      end
    end
  end

  def get_ut(match)
    return match.user_teams.where(user_id: current_user.id).count if current_user.present?
    0
    # return current_user.present? && current_user.user_teams.present? && current_user.user_teams.find_by_match_id_and_league_id(match_id, league_id)
  end

  def get_league(user_team)
    if user_team.league.blank?
      return "No league joined yet!"
    else
      return "Playing #{user_team.league.entry_fee}/#{user_team.league.prize_money}"
    end
  end

  def set_meta_tag(title='Fantoss.com | Win cash. Daily!', url=request.original_url, image="https://fantoss.com/meta_assets/og_image.jpg")
    set_meta_tags :og => {
      :title    => title,
      :type     => 'website',
      :url      => url,
      :image    => image,
      :description  => "fantoss invites you to be a part of India's easiest & trusted cricket contest game. Join hundreds of members competing in 2, 4 and 8 leagues to win double."
    }
  end

end
