# == Schema Information
#
# Table name: challenges
#
#  id                     :integer          not null, primary key
#  fb_request_id          :string
#  user_id                :integer
#  league_id              :integer
#  challenged_count       :integer
#  friends_challenged_ids :string           default([]), is an Array
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  invite_code            :string
#

module ChallengesHelper
end
