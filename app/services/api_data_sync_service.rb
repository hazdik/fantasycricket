class ApiDataSyncService

  attr_reader :tournament, :series_data, :team_data, :schedule_data

  def initialize(data)
    @series_data   = data["Series"]
    @team_data     = series_data["Participant"]["Team"]
    @schedule_data = series_data["Schedule"]["Match"]
    @tournament    = get_tournament
  end

  def call
    sync_participant_team_data
    sync_schedule_data
  end

  private
    def get_tournament
      ::Tournament.where(api_series_id: series_data["SeriesId"]).first_or_create do |t|
        t.name       = series_data["SeriesName"]
        t.start_time = series_data["StartDate"]
        t.end_time   = series_data["EndDate"]
      end
    end

    def sync_participant_team_data
      team_data.each do |td|
        team = ::ParticipantTeam.where(api_team_id: td["teamid"]).first_or_create do |t|
          t.name = td["Name"]
        end
        team.update(current_squad: []) if td["Squad"].blank?
        sync_players_data(td["Squad"]["Player"], team) if td["Squad"].present?
      end
    end

    def sync_players_data(data, team)
      captain_id = nil
      current_squad = []
      data.each do |pd|
        player = team.players.where(api_player_id: pd["personid"]).first_or_create do |p|
          p.name = [pd["FirstName"], pd["LastName"]].join(' ')
        end
        captain_id = player.id if (pd["captain"] == "yes")
        current_squad << player.id
        ::PlayersWorker.perform_async(player.id)
      end
      team.update(captain_id: captain_id)
      team.update(current_squad: current_squad)
    end

    def sync_schedule_data
      if schedule_data.instance_of? Array
        schedule_data.each do |sd|
          update_match_data(sd)
        end
      else
        update_match_data(schedule_data)
      end
    end

    def update_match_data(data)
      tournament.matches.where(api_match_id: data["matchid"]).first_or_create do |m|
        m.title      = [data["MatchNo"], data["Venue"]["content"]].join(', ')
        m.start_time = data["StartDate"]
        m.end_time   = data["EndDate"]
        m.mtype      = data["mtype"]
        m.home_team  = find_team(data["Team"][0]["teamid"])
        m.away_team  = find_team(data["Team"][1]["teamid"])
        m.team_names = "#{m.home_team.name} v/s #{m.away_team.name}" if (m.home_team.present? && m.away_team.present?)
      end
    end

    def find_team(api_team_id)
      ::ParticipantTeam.find_by(api_team_id: api_team_id)
    end
end
