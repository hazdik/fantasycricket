class ScoresPro::Squad < ScoresPro::Base
  include HTTParty

  def self.sync(options = {})
    begin
      url = destination_url({sync_squad: true, league_id: options[:league_id]})
      response = HTTParty.get(url)
    rescue Errno::ETIMEDOUT, Net::ReadTimeout
      retry
    end
    parsed_response = response.parsed_response['Export']

    teams = parsed_response["Team"]

    if teams
      teams.each do |team|
        pt_obj = ::ParticipantTeam.where(api_team_id: team['id'], api_provider: options[:api_provider] ).first_or_initialize do |pt|
          pt.name = team['name']
        end
        pt_obj.save
        # Syncing Players - START
        if team['Player']
          team['Player'].each do |player|
            pl = Player.find_or_create_by(
              api_player_id: player['id'],
              name: player['name'],
              player_category: Player::SCORES_PRO_PLAYER_CATEGORY[player['type']],
              api_provider: options[:api_provider],
              participant_team_id: pt_obj.id
            )
            pl.save
          end
          unless pt_obj.update( current_squad: Player.where(api_provider: 'scorespro', api_player_id: team['Player'].map { |pl| pl['id'] }).pluck(:id))
            puts "-----------------"
            p pt_obj.errors
            puts "-----------------"
          end
        end
      end
    end
  end
end
