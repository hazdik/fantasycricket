class UrlShortnerService
  include HTTParty

  # Constants
  API_KEY = 'AIzaSyAjFY1N0dE8LJ4Mq29Xlmbo8PaZYu_mMiA'

  def self.shorten(url)
    response = post(
      'https://www.googleapis.com/urlshortener/v1/url',
      query: { 'key' => API_KEY },
      body: { 'longUrl' => url }.to_json,
      headers: { 'Content-Type' => 'application/json' }
    )
    response.parsed_response.try(:[], 'id')
  end

end
