class Entity::Cricket::Match::Inning

  attr_accessor :iid, :number, :name, :short_name, :status, :result, :batting_team_id,
    :fielding_team_id, :scores, :scores_full, :batsmen, :bowlers, :fows, :last_wicket,
    :extra_runs, :equations, :current_partnership

  def initialize(options = {})
    @iid = options['iid']
    @number = options['number']
    @name = options['name']
    @short_name = options['short_name']
    @status = options['status']
    @result = options['result']
    @batting_team_id = options['batting_team_id']
    @fielding_team_id = options['fielding_team_id']
    @scores = options['scores']
    @scores_full = options['scores_full']
    @fows = options['fows']
    @last_wicket = options['last_wicket']
    @extra_runs = options['extra_runs']
    @equations = options['equations']
    @current_partnership = options['current_partnership']

    if options['batsmen'].present? && options['batsmen'].is_a?(Array)
      @batsmen = []
      options['batsmen'].each do |bt|
        @batsmen << Entity::Cricket::Match::Batsman.new(bt)
      end
    end

    if options['bowlers'].present? && options['bowlers'].is_a?(Array)
      @bowlers = []
      options['bowlers'].each do |bw|
        @bowlers << Entity::Cricket::Match::Bowler.new(bw)
      end
    end

  end
end
