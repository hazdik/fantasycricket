class Entity::Cricket::Match::Toss

  # Constants
  DECISION_VALUES = {
    batting: 1,
    fielding: 2
  }

  attr_accessor :text, :winner, :decision

  def initialize(options = {})
    @text = options['text']
    @winner = options['winner']
    @decision = options['decision']
  end
end
