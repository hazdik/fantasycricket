class Entity::Cricket::Squad < Entity::Cricket::Base

  ATTRIBUTES = [:team_id, :title, :gmdate, :players, :team]

  attr_accessor *ATTRIBUTES

  # Public instance methods
  def initialize(options = {})
    @team_id = options['team_id']
    @title = options['title']
    @gmdate = options['gmdate']
    if options['players'].is_a?(Array)
      @players = options['players'].inject([]) { |memo, el| memo << Entity::Cricket::Player.new(el) }
    end
  end

end
