class Entity::Cricket::Match < Entity::Cricket::Base

  # Constants
  STATUS_FILTERS = {
    upcoming: 1,
    completed: 2,
    live: 3
  }
  FORMAT_FILTERS = {
    odi: 1,
    test: 2,
    t20i: 3,
    lista: 4,
    first_class: 5,
    t20d: 6,
    wodi: 7,
    wt20: 8,
    yodi: 9,
    yt20: 10,
    others: 11
  }

  ATTRIBUTES = [:match_id, :title, :subtitle, :format, :format_str, :status,
    :status_str, :status_note, :game_state, :game_state_str, :domestic, :competition,
    :teama, :teamb, :date_start, :date_end, :timestamp_start, :timestamp_end,
    :umpires, :referee, :equation, :live, :result, :win_margin, :commentary, :wagon,
    :latest_inning_number, :toss, :innings]

  attr_accessor *ATTRIBUTES

  # Public instance methods
  def initialize(options = {})
    @match_id = options['match_id']
    @title = options['title']
    @subtitle = options['subtitle']
    @format = FORMAT_FILTERS.key(options['format'])
    @format_str = options['format_str']
    @status = STATUS_FILTERS.key(options['status'])
    @status_str = options['status_str']
    @status_note = options['status_note']
    @game_state = options['game_state']
    @game_state_str = options['game_state_str']
    @domestic = options['domestic']
    @date_start = options['date_start']
    @date_end = options['date_end']
    @timestamp_start = options['timestamp_start']
    @timestamp_end = options['timestamp_end']
    @venue = options['venue']
    @umpires = options['umpires']
    @referee = options['referee']
    @equation = options['equation']
    @live = options['live']
    @result = options['result']
    @win_margin = options['win_margin']
    @commentary = options['commentary']
    @wagon = options['wagon']
    @latest_inning_number = options['latest_inning_number']

    @venue = Entity::Cricket::Match::Venue.new(options['venue'])
    @toss = Entity::Cricket::Match::Venue.new(options['toss'])
    @competition = Entity::Cricket::Match::Competition.new(options['competition'])
    @teama = Entity::Cricket::Match::Team.new(options['teama'])
    @teamb = Entity::Cricket::Match::Team.new(options['teamb'])

    if options['innings'].present? && options['innings'].is_a?(Array)
      @innings = []
      options['innings'].each do |inn|
        @innings << Entity::Cricket::Match::Inning.new(inn)
      end
    end
  end

  def attributes
    attributes_pair = {}
    ATTRIBUTES.each do |attribute|
      attributes_pair[attribute] = send(attribute)
    end
    attributes_pair
  end

  def set_squad
    response = HTTParty.get(
      "#{ URLS[:matches] }/#{ match_id }/squads",
      query: {
        token: CREDENTIALS[:access_token]
      }
    )

    set_match_team_squads_from_response(response)
  end

  def sync_squad!
    match_obj = ::Match.find_by(api_provider: API_PROVIDER, api_match_id: match_id)

    if match_obj.present?
      set_squad

      home_team_lineup = []
      away_team_lineup = []

      teama.squads.each do |pl|
        player_obj = ::Player.find_by(api_provider: API_PROVIDER, api_player_id: pl.player_id,
          participant_team_id: match_obj.home_team.id)
        if player_obj.present?
          # match_obj.home_team_lineup << player_obj.id.to_s
          home_team_lineup << player_obj.id.to_s
        else
          player_obj = ::Player.new(api_provider: API_PROVIDER, api_player_id: pl.player_id,
            participant_team_id: match_obj.home_team.id)
          player_obj.name = pl.title
          player_obj.save!
          # match_obj.home_team_lineup << player_obj.id.to_s
          home_team_lineup << player_obj.id.to_s
        end
      end

      teamb.squads.each do |pl|
        player_obj = ::Player.find_by(api_provider: API_PROVIDER, api_player_id: pl.player_id,
          participant_team_id: match_obj.away_team.id)
        if player_obj.present?
          # match_obj.away_team_lineup << player_obj.id.to_s
          away_team_lineup << player_obj.id.to_s
        else
          player_obj = ::Player.new(api_provider: API_PROVIDER, api_player_id: pl.player_id,
            participant_team_id: match_obj.away_team.id)
          player_obj.name = pl.title
          player_obj.save!
          # match_obj.away_team_lineup << player_obj.id.to_s
          away_team_lineup << player_obj.id.to_s
        end
      end

      # match_obj.home_team_lineup.uniq!
      # match_obj.away_team_lineup.uniq!
      match_obj.home_team.update!(current_squad: home_team_lineup.uniq)
      match_obj.away_team.update!(current_squad: away_team_lineup.uniq)
      match_obj.save!
    else
      # Send error
    end
  end

  def set_scorecard
    response = HTTParty.get(
      "#{ URLS[:matches] }/#{ match_id }/scorecard",
      query: {
        token: CREDENTIALS[:access_token]
      }
    )

    set_match_scorecard_from_response(response)
  end


  # Public class methods
  def self.sync_matches!(matches, options = {})
    matches.each do |mtch|
      tournament_obj = ::Tournament.find_by(api_provider: API_PROVIDER, api_series_id: mtch.competition.cid)
      unless tournament_obj.present?
        tournament_obj = ::Tournament.new(
          api_provider: API_PROVIDER,
          api_series_id: mtch.competition.cid,
          name: mtch.competition.title
        )
        tournament_obj.save!
      end

      match_obj = ::Match.find_by(api_provider: API_PROVIDER, api_match_id: mtch.match_id)

      if match_obj.present?
        match_obj.ms = mtch.status
        match_obj.mtype = set_match_obj_format(mtch.format)
        match_obj.ms = set_match_obj_status(mtch.status)
        match_obj.save!
      else
        match_obj = ::Match.new(api_provider: API_PROVIDER, api_match_id: mtch.match_id)

        match_obj.title = mtch.title
        match_obj.tournament_id = tournament_obj.id
        match_obj.mtype = set_match_obj_format(mtch.format)
        match_obj.ms = set_match_obj_status(mtch.status)
        match_obj.start_time = mtch.date_start

        home_team_obj = ::ParticipantTeam.find_by(api_provider: API_PROVIDER, api_team_id: mtch.teama.team_id)
        if home_team_obj.present?
          home_team_obj.name = mtch.teama.name
          home_team_obj.save!
        else
          home_team_obj = ::ParticipantTeam.new(api_provider: API_PROVIDER, api_team_id: mtch.teama.team_id)
          home_team_obj.name = mtch.teama.name
          home_team_obj.save!
        end

        away_team_obj = ::ParticipantTeam.find_by(api_provider: API_PROVIDER, api_team_id: mtch.teamb.team_id)
        if away_team_obj.present?
          away_team_obj.name = mtch.teamb.name
          away_team_obj.save!
        else
          away_team_obj = ::ParticipantTeam.new(api_provider: API_PROVIDER, api_team_id: mtch.teamb.team_id)
          away_team_obj.name = mtch.teamb.name
          away_team_obj.save!
        end

        match_obj.home_team = home_team_obj
        match_obj.away_team = away_team_obj

        match_obj.save!
      end

      if options[:with_squad]
        mtch.sync_squad!
      end

    end
  end

  def self.upcoming(options = {})
    (options[:start] = Time.current) if options[:start].blank?
    (options[:end] = Time.current + 7.days) if options[:end].blank?
    options[:status] = STATUS_FILTERS[:upcoming]

    response = HTTParty.get(URLS[:matches], query: set_query(options))
    set_matches_list_from_response(response)
  end

  def self.completed(options = {})
    (options[:start] = Time.current) if options[:start].blank?
    (options[:end] = Time.current + 7.days) if options[:end].blank?
    options[:status] = STATUS_FILTERS[:completed]

    response = HTTParty.get(URLS[:matches], query: set_query(options))
    set_matches_list_from_response(response)
  end

  def self.live(options = {})
    (options[:start] = Time.current) if options[:start].blank?
    (options[:end] = Time.current + 7.days) if options[:end].blank?
    options[:status] = STATUS_FILTERS[:live]

    response = HTTParty.get(URLS[:matches], query: set_query(options))
    set_matches_list_from_response(response)
  end

  private

  # Private instance methods

  def set_match_scorecard_from_response(response)
    parsed_response = response.parsed_response
    if parsed_response.present?
      if parsed_response['response'].present? && parsed_response['response'].is_a?(Hash)
        if parsed_response['response']['innings'].present? && parsed_response['response']['innings'].is_a?(Array)
          self.innings = []
          parsed_response['response']['innings'].each do |inn|
            self.innings << Entity::Cricket::Match::Inning.new(inn)
          end
        end
      end
    end
  end

  def set_match_team_squads_from_response(response)
    parsed_response = response.parsed_response
    if parsed_response.present?
      if parsed_response['response'].present? && parsed_response['response'].is_a?(Hash)
        _players_hash = {}
        _players = parsed_response['response']['players'].each do |pl|
          _players_hash[pl['pid'].to_s] = pl
        end

        self.teama.squads = []
        _teama = parsed_response['response']['teama']
        _teama['squads'].each do |pl|
          if _players_hash[pl['player_id'].to_s].present?
            self.teama.squads << Entity::Cricket::Match::Player.new(pl.merge(_players_hash[pl['player_id'].to_s]))
          end
        end

        self.teamb.squads = []
        _teamb = parsed_response['response']['teamb']
        _teamb['squads'].each do |pl|
          if _players_hash[pl['player_id'].to_s].present?
            self.teamb.squads << Entity::Cricket::Match::Player.new(pl.merge(_players_hash[pl['player_id'].to_s]))
          end
        end
      end
    end
  end

  # Private class methods

  def self.set_match_obj_status(mtch_status)
    if mtch_status == STATUS_FILTERS[:upcoming]
      return ::Match::MATCH_STATUSES[:upcoming]
    elsif mtch_status == STATUS_FILTERS[:completed]
      return ::Match::MATCH_STATUSES[:finished]
    elsif mtch_status == STATUS_FILTERS[:live]
      return ::Match::MATCH_STATUSES[:live]
    end
  end

  def self.set_match_obj_format(mtch_format)
    if mtch_format == FORMAT_FILTERS[:odi] ||
      mtch_format == FORMAT_FILTERS[:wodi] ||
      mtch_format == FORMAT_FILTERS[:yodi]
      return ::Match::MATCH_FORMATS[:odi]
    elsif mtch_format == FORMAT_FILTERS[:test]
      return ::Match::MATCH_FORMATS[:test]
    elsif mtch_format == FORMAT_FILTERS[:t20i] ||
      mtch_format == FORMAT_FILTERS[:t20d] ||
      mtch_format == FORMAT_FILTERS[:wt20] ||
      mtch_format == FORMAT_FILTERS[:yt20]
      return ::Match::MATCH_FORMATS[:t20]
    elsif mtch_format == FORMAT_FILTERS[:lista]
      return ::Match::MATCH_FORMATS[:list_a]
    elsif mtch_format == FORMAT_FILTERS[:first_class]
      return ::Match::MATCH_FORMATS[:first_class]
    end
  end

  def self.set_matches_list_from_response(response)
    parsed_response = response.parsed_response
    if parsed_response.present?
      if parsed_response['response'].present? && parsed_response['response'].is_a?(Hash)
        if parsed_response['response']['items'].present? && parsed_response['response']['items'].is_a?(Array)
          parsed_response['response']['items'].inject([]) do |memo, el|
            memo << new(el)
          end
        else
          []
        end
      else
        []
      end
    else
      []
    end
  end

  def self.create_start_end_date_param_value(start_date, end_date)
    start_date_string = start_date.strftime("%Y-%m-%d_%H:%M:%S")
    end_date_string = end_date.strftime("%Y-%m-%d_%H:%M:%S")

    "#{ start_date_string }_#{ end_date_string }"
  end

  def self.set_query(options = {})

    query = { token: CREDENTIALS[:access_token] }
    query[:status] = options[:status] if options[:status].present?
    query[:format] = options[:format] if options[:format].present?
    query[:per_page] = options[:per_page] if options[:per_page].present?
    query[:paged] = options[:paged] if options[:paged].present?

    query

    # options[:token] = CREDENTIALS[:access_token]
    # query = options.to_param
    # query += "&date=#{ create_start_end_date_param_value(options[:start], options[:end]) }"
  end

end
