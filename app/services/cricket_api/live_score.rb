class CricketApi::LiveScore < CricketApi::Base

  def self.fetch(options = {})
    begin
      response = HTTParty.get("#{ API_URLS[options[:api_provider]] }/cricket/livescore")
    rescue Errno::ETIMEDOUT, Net::ReadTimeout
      retry
    end
    parsed_response = response.parsed_response
    tournaments = parsed_response['scores']['category']
  end

  def self.sync(options = {})
    tournaments = fetch(options)
    if tournaments
      tournaments = [tournaments] unless tournaments.is_a?(Array)
      tournaments.each do |tournament|

        match = tournament['match']
        @match_obj = ::Match.find_by(
          api_match_id: match['id'].to_i,
          api_provider: options[:api_provider],
          id: options[:match_ids]
        )
        # debugger

        # Syncing batsman stats - START
        if @match_obj.present?
          # debugger
          innings = match['inning']
          # innings = (innings.present? && innings.is_a?(Hash)) ? [innings] : []

          if innings.present?
            if innings.is_a?(Hash)
              innings = [innings]
            end
          else
            innings = []
          end

          innings.each do |inning|
            inning_no = inning['inningnum']
            batsmanstats = inning['batsmanstats']
            batsmen = batsmanstats.present? ? batsmanstats['player'] : []
            batsmen.each do |batsman|
              # debugger
              # {
              #  "batsman"=>"MDN Hansika", "status"=>"lbw b Perry",
              #  "r"=>"0", "b"=>"3", "s4"=>"0", "s6"=>"0", "sr"=>"0.00",
              #  "bat"=>"False", "profileid"=>"342862", "id"=>"14934"
              # }
              players = ::Player.where(
                api_player_id: batsman['profileid'].to_i,
                api_provider: options[:api_provider],
                participant_team_id: [@match_obj.home_team.id, @match_obj.away_team.id]
              )

              if players.count > 1
                pls1 = players.first
                pls2 = players.second
                pt1 = @match_obj.home_team
                pt2 = @match_obj.away_team

                if pt1.current_squad.include?(pls1.id.to_s) || pt2.current_squad.include?(pls1.id.to_s)
                  player = pls1
                elsif pt1.current_squad.include?(pls2.id.to_s) || pt2.current_squad.include?(pls2.id.to_s)
                  player = pls2
                else
                  player = nil
                end
              else
                player = players.first
              end

              if player.present?
                ps = @match_obj.player_scores.where(player_id: player.id, inning_no: inning_no).first_or_initialize
                ps.inning_no       = inning_no.to_i
                ps.bat_runs_scored = batsman["r"].to_i
                ps.bat_balls       = batsman["b"].to_i
                ps.bat_fours       = batsman["s4"].to_i
                ps.bat_sixes       = batsman["s6"].to_i
                ps.bat_status      = batsman['status']
                ps.bat_sr          = batsman["sr"].to_f
                ps.save
              end
            end
          end

          ::UpdateUserTeamScoreWorker.new.perform(@match_obj.id)
          @match_obj.ms = match['status']
          @match_obj.save
          if match['status'] == 'Finished'
            @match_obj.update(end_time: Time.current)
            # @match_obj.process_league_rankings
          end
        end
        # Syncing batsman stats - END

      end
    end

  end

end
