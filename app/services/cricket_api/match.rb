class CricketApi::Match < CricketApi::Base

  def self.upcoming(options = {})
    begin
      response = HTTParty.get("#{ API_URLS[options[:api_provider]] }/cricket/schedule")
    rescue Errno::ETIMEDOUT, Net::ReadTimeout
      retry
    end
    parsed_response = response.parsed_response
    tournaments = parsed_response['fixtures']['category']
  end

  def self.sync_all(options = {})
    # debugger
    tournaments = upcoming(options)
    tournaments.each do |tournament|
      # Conditions based on given tournament ids - If block START
      # debugger
      if options[:api_series_ids].blank? || options[:api_series_ids].include?(tournament['id'])
        # debugger

        # Syncing tournament - START
        tournament_obj = ::Tournament.where(api_series_id: tournament['id'], api_provider: options[:api_provider]).first_or_initialize
        tournament_obj.name = tournament['name']
        tournament_obj.details = { series_file: tournament['series_file'], squads_file: tournament['squads_file'] }
        tournament_obj.save
        # Syncing tournament - END

        # Syncing match - START
        match = tournament['match']
        # Conditions based on given match ids - If block START
        if options[:api_match_ids].blank? || options[:api_match_ids].include?(match['id'])
          # debugger
          match_obj = tournament_obj.matches.where(api_match_id: match['id']).first_or_initialize
          match_obj.home_team = ::ParticipantTeam.where(api_team_id: match['localteam']['id'], api_provider: options[:api_provider]).first_or_create do |pt|
            pt.name = match['localteam']['name']
          end
          match_obj.away_team = ::ParticipantTeam.where(api_team_id: match['visitorteam']['id'], api_provider: options[:api_provider]).first_or_create do |pt|
            pt.name = match['visitorteam']['name']
          end
          if match['date'] != 'TBA' && match['time'] != 'TBA'
            match_obj.start_time = DateTime.strptime("#{ match['date'] } #{ match['time'] }", '%d.%m.%Y %H:%M')
          end
          match_obj.mtype = match['type']
          match_obj.api_provider = options[:api_provider]
          match_obj.title = match['match_num']
          match_obj.team_names = "#{ match_obj.home_team.name } v/s #{ match_obj.away_team.name }"

          match_obj.save
        end
        # Conditions based on given match ids - If block END
        # Syncing matches - END

        # Syncing players - START
        if tournament_obj.details['squads_file'].present?
          squad_obj = CricketApi::Squad.new({ file_path: tournament_obj.details['squads_file'] })
          squad_obj.sync
        end
        # Syncing players - STOP
      end
      # Conditions based on given tournament ids - If block END

    end

  end


  # Instance Methods
  def sync(options = {})
    self.class.sync_all(options)
  end

end
