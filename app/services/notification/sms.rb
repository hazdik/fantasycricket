class Notification::Sms < Notification::Base
  include HTTParty
  # Constants
  API_CREDENTIALS = {
    user_id: ApplicationYML['env']['SMS_NOTIFICATION']['user_id'],
    sender_id: ApplicationYML['env']['SMS_NOTIFICATION']['sender_id'],
    password: ApplicationYML['env']['SMS_NOTIFICATION']['password']
    # password: 'Xul1erig'
  }

  base_uri ApplicationYML['env']['SMS_NOTIFICATION']['base_uri']

  def _send(options = {})
    if recipient.try(:phone).present? && Rails.env.production?
    #if recipient.try(:phone).present?
      begin
        response = self.class.get(
          '/API/SMSHttp.aspx',
          query: {
            UserId: API_CREDENTIALS[:user_id],
            pwd: API_CREDENTIALS[:password],
            SenderId: API_CREDENTIALS[:sender_id],
            Message: content_plain,
            Contacts: recipient.try(:phone),
            ServiceName: options[:sms_type] || 'SMSTRANS'
          }
        )
        # credits_data = check_credits
        # AdminMailer.delay.notify_admin_about_sms_credits(credits_data) if credits_data
      rescue Exception => e
        User.delay.notify_admin(
          intro_text: "SMS request failed | #{ e.message }",
          more_details: e.backtrace,
          subject: 'DigiAlya SMS Service | Exception'
        )
      end
    else
      notify_error
    end
  end

  private

  def check_credits
    self.class.get(
      '/API/creditchecker.aspx',
      query: {
        UserId: API_CREDENTIALS[:user_id],
        pwd: API_CREDENTIALS[:password]
      }
    )
  end

  def notify_error
    Rails.logger.error('Phone not present for sending SMS notification.')
    Rails.logger.error("Recipient Class: #{ recipient.class }, Recipient Id: #{ recipient.try :id }")
  end

  def notify_api_error
    Rails.logger.error('SMS API Problem.')
    Rails.logger.error("Recipient Class: #{ recipient.class }, Recipient Id: #{ recipient.try :id }")
  end

end
