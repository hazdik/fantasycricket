module Matches
  extend ActiveSupport::Concern

  class WinnersExporter
    def self.export(match)
      new(match).to_csv
    end

    def initialize(match)
      @match = match
    end

    def to_csv
      CSV.generate do |csv|
        # csv << ["Order Id", "User Phone", "Prize Money", "Description"]
        match.leagues.paid.each do |league|
          league.winners.each do |winner|
            csv << [user_transaction(winner).try(:token), winner.user.phone, league.prize_money, "fantoss Winner - (#{league.id}) #{match.team_names}"]
          end

          league.tied_user_teams.each do |tied_user|
            csv << [user_transaction(tied_user).try(:token), tied_user.user.phone, league.entry_fee, "fantoss Tie - (#{league.id}) #{match.team_names}"]
          end
        end
      end
    end

    private

    attr_reader :match

    def user_transaction(winner)
      winner.user_transactions
            .completed
            .where(league_id: winner.league_id)
            .first
    end
  end
end
