class PaymentService

  def self.pay_for_league(league_id)
    league = League.find_by(id: league_id)
    if league
      ## WInning amount for for Winners
      league.winners.each do |user_team|
        UserTransaction.create(
          user: user_team.user,
          match: league.match,
          user_team: user_team,
          league: league,
          balance_amount: league.prize_money,
          transaction_category: 'match_win',
          state: 'completed'
        )
      end

      tied_user_ids = []
      ## Tied Refund for Tie users
      league.tied_user_teams.each do |user_team|
        UserTransaction.create(
          user: user_team.user,
          match: league.match,
          user_team: user_team,
          league: league,
          balance_amount: league.entry_fee,
          transaction_category: 'tie_refund',
          state: 'completed'
        )
        tied_user_ids << user_team.user_id
      end

      ## PayTM cashback for paytm-coupon users if any
      league.users_coupons.paytm_consumed.each do |users_coupon|
        # if users_coupon.try(:league) && users_coupon.try(:coupon).try(:concession_type) == 'PAYTM_CASHBACK'
          # skip if user has a tied result
          puts "tied_user_ids #{tied_user_ids}"
          next if tied_user_ids.include?(users_coupon.user_id)

          puts "transfer money for #{users_coupon.user_id}"
          MoneyTransferService.transfer_for(
            users_coupon.user_id,
            {
              amount: users_coupon.worth(users_coupon.league.entry_fee.to_f),
              money_transaction_type: 'PAYTM_CASHBACK',
              metadata: "fantoss | PayTM cashback for coupon #{ users_coupon.coupon.code }",
              details: { league_id: users_coupon.league.id, users_coupon_id: users_coupon.id }
            }
          )
        # end
      end
    end

  end

end
