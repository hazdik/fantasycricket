class ApiLiveScoreService

  attr_reader :match, :score_card, :innings_data, :match_status#, :team_data, :schedule_data

  def initialize(data)
    @score_card = data["Scorecard"]
    @match_status = score_card["ms"]
    @innings_data = score_card["past_ings"]
    @match = ::Match.find_by(api_match_id: score_card["mid"])
  end

  def call
    update_match_status
    refresh_players_score if innings_data
    ::UpdateUserTeamScoreWorker.new.perform(match.id)
  end

  private

    def update_match_status
      match.ms = match_status
      if match_status=="Match Ended" and score_card["result"].present? && score_card["result"]["how"].present? && (score_card["result"]["how"]=="abandoned")
        match.ms = "Match abandoned"
      end
      match.save
      # if match.ms.contain("delayed")
      # end
    end

    def refresh_players_score
      if innings_data.instance_of? Array
        innings_data.each do |bat_data|
          update_batting_data(bat_data["d"]["a"]["t"],bat_data["s"]["a"]["i"],bat_data["s"]["i"])
          # update_bowling_data(bat_data["d"]["o"]["t"])
        end
      else
        update_batting_data(innings_data["d"]["a"]["t"],innings_data["s"]["a"]["i"],innings_data["s"]["i"])
        # update_bowling_data(innings_data["d"]["o"]["t"])
      end
    end

    def update_batting_data(data,api_team_id,inning_no)
      data.each do |bat_data|
        # next if bat_data['dt'].present?
        next if bat_data['c'].blank?
        player = get_player(bat_data["i"],api_team_id)
        if player
          ps = match.player_scores.where(player_id: player.id, inning_no: inning_no).first_or_create
          ps.inning_no = inning_no.to_i
          ps.bat_runs_scored = bat_data["r"].to_i
          ps.bat_balls = bat_data["b"].to_i
          ps.bat_fours = bat_data["four"].to_i
          ps.bat_sixes = bat_data["six"].to_i
          ps.bat_status = bat_data['c']
          ps.bat_sr = bat_data["sr"].to_f
          ps.save
        end
      end
    end

    def update_bowling_data(data)
      data.each do |bowl_data|
        player = get_player(bowl_data["i"])
        if player
          ps = match.player_scores.where(player_id: player.id).first_or_create
          ps.bowl_overs = bowl_data["o"].to_i
          ps.bowl_maidens = bowl_data["mo"].to_i
          ps.bowl_runs = bowl_data["r"].to_i
          ps.bowl_wickets = bowl_data["w"].to_i
          ps.bowl_wides = bowl_data["wd"].to_i
          ps.bowl_noballs = bowl_data["nb"].to_i
          ps.bowl_er = bowl_data["sr"].to_f
          ps.save
        end
      end
    end

    def get_player(api_id,api_team_id)
      pt = ::ParticipantTeam.find_by(api_team_id: api_team_id, api_provider: nil)
      pt.players.find_by(api_player_id: api_id, api_provider: nil)
    end
end


