require "administrate/base_dashboard"

class ParticipantTeamDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    players: Field::HasMany,
    batsmans: Field::HasMany.with_options(class_name: "Player"),
    bowlers: Field::HasMany.with_options(class_name: "Player"),
    home_matches: Field::HasMany.with_options(class_name: "Match"),
    away_matches: Field::HasMany.with_options(class_name: "Match"),
    id: Field::Number,
    name: Field::String,
    short_name: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    captain_id: Field::Number,
    keeper_id: Field::Number,
    api_team_id: Field::String,
    current_squad: Field::String.with_options(searchable: false),
    flag_path: Field::String.with_options(searchable: false)
  }

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :api_team_id,
    :name,
    :players,
    :home_matches,
  ]

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = ATTRIBUTE_TYPES.keys

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :players,
    :batsmans,
    :bowlers,
    :name,
    :short_name,
    :captain_id,
    :keeper_id,
    :api_team_id,
    :current_squad,
    :flag_path
  ]

  # Overwrite this method to customize how participant teams are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(participant_team)
    "#{participant_team.name}"
  end
end
