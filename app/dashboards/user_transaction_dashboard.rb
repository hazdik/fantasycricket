require "administrate/base_dashboard"

class UserTransactionDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    user: Field::BelongsTo,
    id: Field::Number,
    total_amount: Field::Number,
    balance_amount: Field::Number,
    transaction_category: Field::String,
    league: Field::BelongsTo,
    user_team: Field::BelongsTo,
    match: Field::BelongsTo,
    params: Field::String.with_options(searchable: false),
    payment_gateway_response: Field::String.with_options(searchable: false),
    token: Field::String,
    state: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :user,
    :transaction_category,
    :total_amount,
    :balance_amount,
    :state,
    :user_team,
    :league,
  ]

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = ATTRIBUTE_TYPES.keys

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :user,
    :state,
    :transaction_category
  ]

  # Overwrite this method to customize how user transactions are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(user_transaction)
  #   "UserTransaction ##{user_transaction.id}"
  # end
end
