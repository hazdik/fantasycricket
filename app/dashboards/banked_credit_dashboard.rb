require "administrate/base_dashboard"

class BankedCreditDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    user: Field::BelongsTo,
    referring_to: Field::Polymorphic,
    id: Field::Number,
    value: Field::Number,
    reason: Field::String,
    redeemed_on: Field::DateTime,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    transaction_type: Field::String,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :reason,
    :value,
    :user,
    :referring_to,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :user,
    :referring_to,
    :id,
    :value,
    :reason,
    :redeemed_on,
    :created_at,
    :updated_at,
    :transaction_type,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :user,
    :referring_to,
    :value,
    :reason,
    :redeemed_on,
    :transaction_type,
  ].freeze

  # Overwrite this method to customize how banked credits are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(banked_credit)
  #   "BankedCredit ##{banked_credit.id}"
  # end
end
