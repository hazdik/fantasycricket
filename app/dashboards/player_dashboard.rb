require "administrate/base_dashboard"

class PlayerDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    participant_team: Field::BelongsTo,
    id: Field::Number,
    name: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    player_category: Field::String,
    price: Field::Number,
    player_xp: Field::Number,
    photo_url: Field::String,
    api_player_id: Field::String,
    image: Field::String,
    odi_career: Field::String.with_options(searchable: false),
    ipl_career: Field::String.with_options(searchable: false),
    t20_career: Field::String.with_options(searchable: false),
    test_career: Field::String.with_options(searchable: false),
  }

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :name,
    :participant_team
  ]

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = ATTRIBUTE_TYPES.keys

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :participant_team,
    :name,
    :player_category,
    :price,
    :player_xp,
    :photo_url,
    :api_player_id,
    :image,
  ]

  # Overwrite this method to customize how players are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(player)
    "Player ##{player.name}"
  end
end
