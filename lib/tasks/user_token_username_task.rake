# bundle exec rake user:generate_auth_token RAILS_ENV=production
# bundle exec rake user:generate_username RAILS_ENV=production
# bundle exec rake user:transfer_credits RAILS_ENV=production
namespace :user do
  desc "generate auth token for existing users"
  task generate_auth_token: :environment do
    User.all.each do |user|
      user.authentication_token = loop do
        random_token = SecureRandom.urlsafe_base64(nil, false)
        break random_token unless User.exists?(authentication_token: random_token)
      end
      user.save!
      puts "User Token = #{user.authentication_token}"
    end
  end
  task generate_username: :environment do
    User.all.each { |u| u.update_column(:username, POPULAR_PLAYERS.sample + "_#{ u.id }") }
  end
  task transfer_credits: :environment do
    User.all.each { |u| u.transfer_credits_to_wallet; puts "transfer to wallet user_id #{u.id}" }
  end
end
