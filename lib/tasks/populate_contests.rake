namespace :populate do
  desc "Populate contests records if does not exists"
  task contests: :environment do
    Contest::TEAM_TYPE.each do |key, value|
      Contest.where(team_type: key, title: value).first_or_create
    end
    puts "Populating contests stopped at #{ Time.current }."
  end
end
