# Live scorecard :
# https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20cricket.scorecard.live.summary&diagnostics=true&env=store%3A%2F%2F0TxIGQMQbObzvU4Apia0V0
# https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20cricket.scorecard%20where%20match_id%3D192408&format=json&diagnostics=true&env=store%3A%2F%2F0TxIGQMQbObzvU4Apia0V0&callback=

# 1. get all upcoming series
#   http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20cricket.series.upcoming%20where%20output%3D'complete'&format=json&env=store%3A%2F%2F0TxIGQMQbObzvU4Apia0V0&callback=

#   > get id data to get specific series data

# 2. sync specific series to fantoss

#   - get specific series and general squad data

#   http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20cricket.series.upcoming%20where%20series_id%3D12343%20and%20output%3D%27complete%27%0A&format=json&env=store%3A%2F%2F0TxIGQMQbObzvU4Apia0V0&callback=

#   if squad data is present-?

#   - create players if not exist
#   http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20cricket.players%20where%20player_id%3D" + playerId + "&format=json&diagnostics=true&env=store%3A%2F%2F0TxIGQMQbObzvU4Apia0V0&callback=

#   - create players if not exist
#   http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20cricket.players%20where%20player_id%3D3991&format=json&diagnostics=true&env=store%3A%2F%2F0TxIGQMQbObzvU4Apia0V0&callback=

# url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20cricket.player.profile%20where%20player_id%3D3822&format=json&diagnostics=true&env=store%3A%2F%2F0TxIGQMQbObzvU4Apia0V0&callback="

# https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20cricket.scorecard%20where%20match_id%3D190806&format=json&diagnostics=true&env=store%3A%2F%2F0TxIGQMQbObzvU4Apia0V0&callback=
# http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20cricket.series.ongoing&format=json&diagnostics=true&env=store%3A%2F%2F0TxIGQMQbObzvU4Apia0V0&callback=

# Test match scorecard:
# https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20cricket.scorecard%20where%20match_id%3D%20192407&format=json&diagnostics=true&env=store%3A%2F%2F0TxIGQMQbObzvU4Apia0V0&callback=

require 'net/http'

module ScoreAPI
  include HTTParty

  def self.getUpcomingSeries
    url = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20cricket.series.upcoming%20where%20output%3D'complete'&format=json&env=store%3A%2F%2F0TxIGQMQbObzvU4Apia0V0&callback="
    response = get(url)
    return response
  end


  def self.getSeries(series_id)
    url = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20cricket.series.upcoming%20where%20series_id%3D#{series_id}%20and%20output%3D%27complete%27%0A&format=json&env=store%3A%2F%2F0TxIGQMQbObzvU4Apia0V0&callback="
    response = get(url)
    return response
  end

  # For demo only.
  def self.getSachin
    # "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20cricket.player.profile%20where%20player_id%3D2962&format=json&diagnostics=true&env=store%3A%2F%2F0TxIGQMQbObzvU4Apia0V0&callback="
    getPlayer(2962)
  end

  def self.getPlayer(player_id)
    url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20cricket.player.profile%20where%20player_id%3D#{player_id}&format=json&diagnostics=true&env=store%3A%2F%2F0TxIGQMQbObzvU4Apia0V0&callback="
    get(url)
  end

  def self.getLiveScore(match_id,make_url=false)
    url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20cricket.scorecard%20where%20match_id%3D#{ match_id }&format=json&diagnostics=true&env=store%3A%2F%2F0TxIGQMQbObzvU4Apia0V0&callback="
    make_url ? url : get(url)
  end

  def self.getLiveScoreSummary(match_id,make_url=false)
    # summary API which fits the points by @dhirajbajaj
    # url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20cricket.scorecard%20where%20match_id%3D#{ match_id }&format=json&diagnostics=true&env=store%3A%2F%2F0TxIGQMQbObzvU4Apia0V0&callback="
    url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20cricket.scorecard.summary%20where%20match_id%3D#{match_id}&format=json&diagnostics=true&env=store%3A%2F%2F0TxIGQMQbObzvU4Apia0V0&callback="
    make_url ? url : get(url)
  end

  def self.sendPingback(clickMomentId)
    url = "http://app.airloyal.com/airloyal/earn/callback/finemojo?clickMomentId=#{clickMomentId}"
    get(url)
  end

  def self.sendSportzwikiPingback(token)
    _url = "http://sportzwiki.com/pingback_url.php"
    _data = { source: "fantoss", token: token, status: "200", message: "#{token} user ping to sportzwiki" }
    # post( _url, _data )
    post(_url, { body: _data, debug_output: $stdout })
  end

end

