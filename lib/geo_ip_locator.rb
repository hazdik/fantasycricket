require 'net/http'

class GeoIpLocator
  # BASE_URL = "http://ip-api.com/json/"
  # BASE_URL = "http://api.db-ip.com/v2/743149b555076777c2fe2aa217debe11bdd837c2/"
  BASE_URL = "http://freegeoip.net/json/"

  def self.city(ip_address)
    url = BASE_URL + ip_address.to_s
    response = HTTParty.get(url)
    puts response
    response["region_name"]
  end

end



# http://api.db-ip.com/v2/743149b555076777c2fe2aa217debe11bdd837c2/47.29.146.251
# http://api.db-ip.com/v2/743149b555076777c2fe2aa217debe11bdd837c2/171.79.70.119
# http://api.db-ip.com/v2/743149b555076777c2fe2aa217debe11bdd837c2/47.29.156.178
# {
# ipAddress: "47.29.146.251",
# continentCode: "AS",
# continentName: "Asia",
# countryCode: "IN",
# countryName: "India",
# stateProv: "Assam",
# city: "Nagaon district"
# }


# http://freegeoip.net/json/14.139.212.228
# {
# ip: "14.139.212.228",
# country_code: "IN",
# country_name: "India",
# region_code: "OR",
# region_name: "Odisha",
# city: "Bhubaneswar",
# zip_code: "751024",
# time_zone: "Asia/Kolkata",
# latitude: 20.2333,
# longitude: 85.8333,
# metro_code: 0
# }
