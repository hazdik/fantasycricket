class AddConcessionTypeToCoupon < ActiveRecord::Migration
  def change
    add_column :coupons, :concession_type, :string, default: 'LEAGUE_ENTRY_FEE'
    add_column :coupons, :coupon_message, :string
  end
end
