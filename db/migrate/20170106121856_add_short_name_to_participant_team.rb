class AddShortNameToParticipantTeam < ActiveRecord::Migration
  def change
    add_column :participant_teams, :short_name, :string , default: ""
  end
end
