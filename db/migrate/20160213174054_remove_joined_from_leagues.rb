class RemoveJoinedFromLeagues < ActiveRecord::Migration
  def change
    remove_column :leagues, :joined, :integer
  end
end
