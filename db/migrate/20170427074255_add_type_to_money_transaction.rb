class AddTypeToMoneyTransaction < ActiveRecord::Migration
  def change
    add_column :money_transactions, :money_transaction_type, :string
  end
end
