class AddBlockToMetaInfo < ActiveRecord::Migration
  def change
    add_column :meta_infos, :block, :boolean, default: true
  end
end
