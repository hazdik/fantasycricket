class AddCareerDetailsToPlayers < ActiveRecord::Migration
  def change
    add_column :players, :odi_career, :hstore
    add_column :players, :ipl_career, :hstore
    add_column :players, :t20_career, :hstore
    add_column :players, :date_of_birth, :datetime
  end
end
