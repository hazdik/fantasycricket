class CreateParticipantTeam < ActiveRecord::Migration
  def change
    create_table :participant_teams do |t|
      t.string :name
      t.integer :tournament_id

      t.timestamps
    end
  end
end
