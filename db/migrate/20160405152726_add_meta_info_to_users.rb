class AddMetaInfoToUsers < ActiveRecord::Migration
  def change
    add_column :users, :meta_info, :hstore
  end
end
