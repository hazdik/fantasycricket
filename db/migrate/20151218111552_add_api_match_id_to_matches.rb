class AddApiMatchIdToMatches < ActiveRecord::Migration
  def change
    add_column :matches, :api_match_id, :string
  end
end
