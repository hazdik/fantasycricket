class PlayersUserTeamsHabtm < ActiveRecord::Migration
  def change
    create_table :players_teams, :id => false do |t|
      t.references :user_team
      t.references :player
    end
    add_index :players_teams, :user_team_id
    add_index :players_teams, :player_id

  end
end
