class ChangeUsernameColumnInUser < ActiveRecord::Migration
  def up
    change_column :users, :username, :string
  end

  def down
    change_column :users, :username, 'integer USING CAST(username AS integer)'
  end
end
