class StripPlayersModel < ActiveRecord::Migration

  def change
    tbr_players = Player.column_names - ['id', 'name', 'player_category', 'price', 'participant_team_id', 'created_at', 'updated_at']
    tbr_players.each do |player|
      remove_column :players, player
      puts "-- #{player} column"
    end
    add_column :players, :player_xp, :integer, default: 0
    add_column :players, :photo_url, :string
  end

end
