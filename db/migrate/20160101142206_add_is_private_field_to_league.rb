class AddIsPrivateFieldToLeague < ActiveRecord::Migration
  def change
    add_column :leagues, :is_private, :boolean, default: :false
    add_column :leagues, :is_passed, :boolean, default: :false
  end
end
