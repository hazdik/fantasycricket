class UpdateDetailsToUserTeam < ActiveRecord::Migration
  def change
    add_column :user_teams, :contest_id, :integer
    # remove_column :user_teams, :league_id, :integer
    remove_column :user_teams, :answer1, :string
    remove_column :user_teams, :answer2, :string
  end
end
