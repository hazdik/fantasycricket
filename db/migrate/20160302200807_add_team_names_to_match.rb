class AddTeamNamesToMatch < ActiveRecord::Migration
  def change
    add_column :matches, :team_names, :string, default: ''
  end
end
