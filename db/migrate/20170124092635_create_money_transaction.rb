class CreateMoneyTransaction < ActiveRecord::Migration
  def change
    create_table :money_transactions do |t|
      t.json :request_details
      t.json :response_details
      t.json :last_verification_request_details
      t.json :last_verification_response_details
      t.string :provider
      t.string :amount
      t.string :status
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
  end
end
