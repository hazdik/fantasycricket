class AddInviteCodeToLeagues < ActiveRecord::Migration
  def change
    add_column :leagues, :invite_code, :string
    add_index :leagues, :invite_code
  end
end
