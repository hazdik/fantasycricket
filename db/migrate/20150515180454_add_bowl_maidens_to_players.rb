class AddBowlMaidensToPlayers < ActiveRecord::Migration
  def change
    add_column :players, :bat_fours, :integer, :default => 0
    add_column :players, :bat_sixes, :integer, :default => 0
    add_column :players, :bowl_maidens, :integer, :default => 0
    add_column :players, :ts_keeper_decrement, :integer, :default => 0
    add_column :players, :ts_increment, :integer, :default => 0
    add_column :players, :bat_avg_invalid, :boolean
    add_column :players, :bowl_avg_invalid, :boolean
  end
end
