class AddApiSeriesIdToTournaments < ActiveRecord::Migration
  def change
    add_column :tournaments, :api_series_id, :string
  end
end
