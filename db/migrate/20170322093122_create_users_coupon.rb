class CreateUsersCoupon < ActiveRecord::Migration
  def change
    create_table :users_coupons do |t|
      t.belongs_to :user, index: true
      t.belongs_to :coupon, index: true
      t.belongs_to :league, index: true
      t.belongs_to :user_transaction, index: true
      t.string :state

      t.timestamps null: false
    end
  end
end
