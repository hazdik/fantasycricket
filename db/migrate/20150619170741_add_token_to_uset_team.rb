class AddTokenToUsetTeam < ActiveRecord::Migration
  def change
    add_column :user_teams, :token, :string
    add_index :user_teams, :token
  end
end
