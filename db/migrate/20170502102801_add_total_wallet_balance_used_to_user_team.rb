class AddTotalWalletBalanceUsedToUserTeam < ActiveRecord::Migration
  def change
    add_column :user_teams, :total_wallet_balance_used, :float
    add_column :user_teams, :referral_wallet_balance_used, :float
    add_column :user_teams, :main_wallet_balance_used, :float
  end
end
