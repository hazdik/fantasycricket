class ChangeTotalscoreDefault < ActiveRecord::Migration
  def up
    change_column_default :user_teams, :totalscore, 0
    rename_column :user_teams, :validated, :has_won
    change_column_default :user_teams, :has_won, false
  end
  def down
    change_column_default :user_teams, :totalscore, 0
    rename_column :user_teams, :validated, :has_won
    change_column_default :user_teams, :has_won, false
  end
end
