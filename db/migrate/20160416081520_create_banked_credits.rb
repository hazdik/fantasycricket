class CreateBankedCredits < ActiveRecord::Migration
  def change
    create_table :banked_credits do |t|
      t.integer :value, default: 0
      t.string :reason
      t.belongs_to :user
      t.belongs_to :referring_to, polymorphic: true
      t.datetime :redeemed_on

      t.timestamps null: false
    end
  end
end
