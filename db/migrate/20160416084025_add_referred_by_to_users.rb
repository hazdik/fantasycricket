class AddReferredByToUsers < ActiveRecord::Migration
  def change
    add_column :users, :referred_by_id, :integer
    add_column :users, :referral_code, :string

    add_index :users, :referred_by_id
    add_index :users, :referral_code
  end
end
