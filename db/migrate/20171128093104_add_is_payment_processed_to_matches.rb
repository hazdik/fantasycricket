class AddIsPaymentProcessedToMatches < ActiveRecord::Migration
  def change
    add_column :matches, :is_payment_processed, :boolean, default: false
  end
end
