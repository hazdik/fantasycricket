class AddAmountToUsersCoupon < ActiveRecord::Migration
  def change
    add_column :users_coupons, :amount, :float
  end
end
