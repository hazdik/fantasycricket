class IsSpamReferrerToUser < ActiveRecord::Migration
  def change
    add_column :users, :is_spam_referrer, :boolean, default: :false
  end
end
