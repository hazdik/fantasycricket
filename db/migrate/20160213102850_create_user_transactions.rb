class CreateUserTransactions < ActiveRecord::Migration
  def change
    create_table :user_transactions do |t|
      t.hstore :params
      t.hstore :payment_gateway_response

      t.belongs_to :user
      t.string :token

      t.timestamps null: false
    end
  end
end
