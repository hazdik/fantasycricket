class AddBatDoubleHundredsToPlayerScores < ActiveRecord::Migration
  def change
    add_column :player_scores, :bat_double_hundreds, :integer
  end
end
