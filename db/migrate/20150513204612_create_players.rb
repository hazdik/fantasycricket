class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.string :name
      t.string :age_category
      t.integer :bat_innings, :default => 0
      t.integer :bat_runs_scored, :default => 0
      t.integer :bat_fifties, :default => 0
      t.integer :bat_hundreds, :default => 0
      t.integer :bat_ducks, :default => 0
      t.integer :bat_not_outs, :default => 0
      t.integer :bowl_overs, :default => 0
      t.integer :bowl_runs, :default => 0
      t.integer :bowl_wickets, :default => 0
      t.integer :bowl_4_wickets, :default => 0
      t.integer :bowl_6_wickets, :default => 0
      t.integer :field_catches, :default => 0
      t.integer :field_runouts, :default => 0
      t.integer :field_stumpings, :default => 0
      t.integer :field_drops, :default => 0
      t.integer :field_mom, :default => 0
      t.integer :team, :default => 1

      t.timestamps
    end
    add_column :players, :bat_score, :integer, :default => 0
    add_column :players, :bowl_score, :integer, :default => 0
    add_column :players, :field_score, :integer, :default => 0
    add_column :players, :bonus, :integer, :default => 0
    add_column :players, :bat_avg, :float, :default => 0
    add_column :players, :bowl_avg, :float, :default => 0
    add_column :players, :player_category, :string    
    add_column :players, :total, :integer, :default => 0    
    add_column :players, :price, :integer, :default => 0    

    add_column :players, :ls_bat_innings, :integer, :default => 0
    add_column :players, :ls_bat_runs_scored, :integer, :default => 0
    add_column :players, :ls_bat_fours, :integer, :default => 0
    add_column :players, :ls_bat_sixes, :integer, :default => 0
    add_column :players, :ls_bat_fifties, :integer, :default => 0
    add_column :players, :ls_bat_hundreds, :integer, :default => 0
    add_column :players, :ls_bat_ducks, :integer, :default => 0
    add_column :players, :ls_bat_not_outs, :integer, :default => 0
    add_column :players, :ls_bowl_overs, :integer, :default => 0
    add_column :players, :ls_bowl_runs, :integer, :default => 0
    add_column :players, :ls_bowl_wickets, :integer, :default => 0
    add_column :players, :ls_bowl_4_wickets, :integer, :default => 0
    add_column :players, :ls_bowl_6_wickets, :integer, :default => 0
    add_column :players, :ls_bowl_maidens, :integer, :default => 0
    add_column :players, :ls_field_catches, :integer, :default => 0
    add_column :players, :ls_field_runouts, :integer, :default => 0
    add_column :players, :ls_field_stumpings, :integer, :default => 0
    add_column :players, :ls_field_drops, :integer, :default => 0
    add_column :players, :ls_field_mom, :integer, :default => 0
    add_column :players, :ls_bat_score, :integer, :default => 0
    add_column :players, :ls_bowl_score, :integer, :default => 0
    add_column :players, :ls_field_score, :integer, :default => 0
    add_column :players, :ls_bonus, :integer, :default => 0
    add_column :players, :ls_total, :integer, :default => 0
    add_column :players, :ls_bat_avg, :float, :default => 0.0
    add_column :players, :ls_bowl_avg, :float, :default => 0.0
    add_column :players, :ls_bat_avg_invalid, :boolean
    add_column :players, :ls_bowl_avg_invalid, :boolean

    
  end
end
