class ChangeAnswerHashUserTeam < ActiveRecord::Migration
  def change
    remove_column :user_teams, :answer_hash
    add_column :user_teams, :answer_hash,  :json
  end
end
