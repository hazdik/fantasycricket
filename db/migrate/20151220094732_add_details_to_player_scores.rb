class AddDetailsToPlayerScores < ActiveRecord::Migration
  def change
    add_column :player_scores, :bat_score, :integer, default: 0
    add_column :player_scores, :bowl_score, :integer, default: 0
    add_column :player_scores, :field_score, :integer, default: 0
    add_column :player_scores, :total, :integer, default: 0
    remove_column :player_scores, :name, :string
    remove_column :player_scores, :bat_minutes, :integer
    remove_column :player_scores, :bat_how, :integer
  end
end
