class AddDetailsToMoneyTransaction < ActiveRecord::Migration
  def change
    add_column :money_transactions, :details, :json
  end
end
