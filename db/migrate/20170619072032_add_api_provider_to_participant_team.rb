class AddApiProviderToParticipantTeam < ActiveRecord::Migration
  def change
    add_column :participant_teams, :api_provider, :string
    add_column :matches, :api_provider, :string
    add_column :tournaments, :api_provider, :string
    add_column :players, :api_provider, :string
  end
end
