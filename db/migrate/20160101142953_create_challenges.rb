class CreateChallenges < ActiveRecord::Migration
  def change
    create_table :challenges do |t|
      t.string :fb_request_id
      t.references :user, index: true, foreign_key: true
      t.references :league, index: true, foreign_key: true
      t.integer :challenged_count
      t.string :friends_challenged_ids, array: true, default: []

      t.timestamps null: false
    end
  end
end
