class CreateCareerStat < ActiveRecord::Migration
  def change
    create_table :career_stats do |t|
      t.belongs_to :player, index: true
      t.json :batting
      t.json :bowling
      t.json :fielding
      t.string :api_provider

      t.timestamps null: false
    end
  end
end
