class AddLeagueIdToUserTeam < ActiveRecord::Migration
  def change
    add_column :user_teams, :league_id, :integer
  end
end
