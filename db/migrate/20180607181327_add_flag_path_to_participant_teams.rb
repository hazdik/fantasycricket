class AddFlagPathToParticipantTeams < ActiveRecord::Migration
  def change
    add_column :participant_teams, :flag_path, :string
  end
end
