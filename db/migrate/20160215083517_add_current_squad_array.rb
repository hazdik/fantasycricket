class AddCurrentSquadArray < ActiveRecord::Migration
  def change
    add_column :participant_teams, :current_squad, :text, array:true, default: []
    add_index  :participant_teams, :current_squad, using: 'gin'
  end
end
