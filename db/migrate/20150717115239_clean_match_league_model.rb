class CleanMatchLeagueModel < ActiveRecord::Migration
  def change
    remove_column :matches, :home_team_score
    remove_column :matches, :away_team_score
  end
end
