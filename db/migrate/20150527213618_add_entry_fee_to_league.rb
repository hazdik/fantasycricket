class AddEntryFeeToLeague < ActiveRecord::Migration
  def change
    add_column :leagues, :entry_fee, :integer
    add_column :leagues, :rules, :text
  end
end
