class AddQuestionsAnswersToLeagues < ActiveRecord::Migration
  def change
    enable_extension 'hstore'    
    add_column :user_teams, :answer_hash, :hstore
    add_column :user_teams, :rank, :integer
    
    add_column :leagues, :questions_hash, :hstore
    add_column :leagues, :truth_hash,     :hstore
    add_column :leagues, :answer_type,    :string
    add_column :leagues, :answer_size,    :integer
    add_column :leagues, :rank_hash,      :hstore

    # leagues :
    #   truth_hash      : hstore -> [ids of answers]
    #     { 5 : 21 }, { over : runs }, { 10 : 31 }, ...
    #   answer_type       : Player / Over    
    #   answer_size       : integer  5 | 10
    #   rank_hash         : hstore
    #   questions_hash    : hstore - >
    #     { 1: 'question text and description' },
    #     { 2: 'question text and description' },
    #     { 3: 'question text and description' },
    #   }
  end
  
end
