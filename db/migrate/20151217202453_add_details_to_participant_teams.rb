class AddDetailsToParticipantTeams < ActiveRecord::Migration
  def change
    add_column :participant_teams, :api_team_id, :string
    remove_column :participant_teams, :tournament_id, :integer
  end
end
