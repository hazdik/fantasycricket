class AddBatStatusToPlayerScore < ActiveRecord::Migration
  def change
    add_column :player_scores, :bat_status, :string
  end
end
