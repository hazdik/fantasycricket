class AddDetailsToTournament < ActiveRecord::Migration
  def change
    add_column :tournaments, :details, :json
  end
end
