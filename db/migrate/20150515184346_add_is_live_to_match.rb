class AddIsLiveToMatch < ActiveRecord::Migration
  def change
    add_column :matches, :is_live, :boolean, default: false
  end
end
