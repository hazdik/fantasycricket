--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.12
-- Dumped by pg_dump version 9.5.12

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: authentications; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.authentications (
    id integer NOT NULL,
    user_id integer,
    provider character varying NOT NULL,
    proid character varying NOT NULL,
    token character varying,
    refresh_token character varying,
    secret character varying,
    expires_at timestamp without time zone,
    username character varying,
    image_url character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: authentications_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.authentications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: authentications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.authentications_id_seq OWNED BY public.authentications.id;


--
-- Name: banked_credits; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.banked_credits (
    id integer NOT NULL,
    value integer DEFAULT 0,
    reason character varying,
    user_id integer,
    referring_to_id integer,
    referring_to_type character varying,
    redeemed_on timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    transaction_type character varying DEFAULT ''::character varying
);


--
-- Name: banked_credits_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.banked_credits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: banked_credits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.banked_credits_id_seq OWNED BY public.banked_credits.id;


--
-- Name: career_stats; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.career_stats (
    id integer NOT NULL,
    player_id integer,
    batting json,
    bowling json,
    fielding json,
    api_provider character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: career_stats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.career_stats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: career_stats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.career_stats_id_seq OWNED BY public.career_stats.id;


--
-- Name: challenges; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.challenges (
    id integer NOT NULL,
    fb_request_id character varying,
    user_id integer,
    league_id integer,
    challenged_count integer,
    friends_challenged_ids character varying[] DEFAULT '{}'::character varying[],
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    invite_code character varying
);


--
-- Name: challenges_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.challenges_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: challenges_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.challenges_id_seq OWNED BY public.challenges.id;


--
-- Name: contests; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.contests (
    id integer NOT NULL,
    questions_hash json,
    title character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    team_type character varying
);


--
-- Name: contests_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.contests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.contests_id_seq OWNED BY public.contests.id;


--
-- Name: coupons; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.coupons (
    id integer NOT NULL,
    code character varying,
    discount_value double precision,
    discount_type character varying,
    maximum_discount_value double precision,
    start_date timestamp without time zone,
    end_date timestamp without time zone,
    maximum_redeem_count integer,
    current_redeem_count integer,
    user_validation_eval_string character varying,
    league_validation_eval_string character varying,
    tournament_id integer,
    match_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    per_user_count integer DEFAULT 1,
    concession_type character varying DEFAULT 'LEAGUE_ENTRY_FEE'::character varying,
    coupon_message character varying,
    match_validation_eval_string character varying,
    league_type character varying
);


--
-- Name: coupons_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.coupons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: coupons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.coupons_id_seq OWNED BY public.coupons.id;


--
-- Name: leagues; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.leagues (
    id integer NOT NULL,
    name character varying,
    match_id integer,
    "limit" integer DEFAULT 0,
    is_live boolean DEFAULT false,
    prize_money integer DEFAULT 0,
    is_multiple_allowed boolean DEFAULT false,
    is_fake boolean DEFAULT false,
    fake_size integer DEFAULT 0,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    entry_fee integer,
    contest_id integer,
    is_private boolean DEFAULT false,
    is_tied boolean DEFAULT false,
    user_teams_count integer DEFAULT 0,
    invite_code character varying,
    winner_ranks text[] DEFAULT '{}'::text[],
    loser_ranks text[] DEFAULT '{}'::text[],
    tied_team_ranks text[] DEFAULT '{}'::text[],
    league_type character varying
);


--
-- Name: leagues_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.leagues_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: leagues_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.leagues_id_seq OWNED BY public.leagues.id;


--
-- Name: matches; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.matches (
    id integer NOT NULL,
    home_team_id integer,
    away_team_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    start_time timestamp without time zone,
    end_time timestamp without time zone,
    tournament_id integer,
    is_live boolean DEFAULT false,
    title character varying,
    api_match_id character varying,
    mtype character varying,
    team_names character varying DEFAULT ''::character varying,
    ms character varying DEFAULT 'Match is yet to start'::character varying,
    api_provider character varying,
    is_payment_processed boolean DEFAULT false
);


--
-- Name: matches_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.matches_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: matches_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.matches_id_seq OWNED BY public.matches.id;


--
-- Name: meta_infos; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.meta_infos (
    id integer NOT NULL,
    name character varying,
    identifier integer,
    info json,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    block boolean DEFAULT true
);


--
-- Name: meta_infos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.meta_infos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: meta_infos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.meta_infos_id_seq OWNED BY public.meta_infos.id;


--
-- Name: money_transactions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.money_transactions (
    id integer NOT NULL,
    request_details json,
    response_details json,
    last_verification_request_details json,
    last_verification_response_details json,
    provider character varying,
    amount character varying,
    status character varying,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    money_transaction_type character varying,
    details json
);


--
-- Name: money_transactions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.money_transactions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: money_transactions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.money_transactions_id_seq OWNED BY public.money_transactions.id;


--
-- Name: oauth_caches; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.oauth_caches (
    authentication_id integer NOT NULL,
    data_json text NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: participant_teams; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.participant_teams (
    id integer NOT NULL,
    name character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    captain_id integer,
    keeper_id integer,
    api_team_id character varying,
    current_squad text[] DEFAULT '{}'::text[],
    short_name character varying DEFAULT ''::character varying,
    api_provider character varying,
    flag_path character varying
);


--
-- Name: participant_teams_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.participant_teams_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: participant_teams_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.participant_teams_id_seq OWNED BY public.participant_teams.id;


--
-- Name: player_scores; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.player_scores (
    id integer NOT NULL,
    match_id integer,
    player_id integer,
    bat_runs_scored integer DEFAULT 0,
    bat_balls integer,
    bat_fours integer DEFAULT 0,
    bat_sixes integer DEFAULT 0,
    bat_sr numeric,
    bowl_overs integer,
    bowl_maidens integer DEFAULT 0,
    bowl_runs integer,
    bowl_wickets integer DEFAULT 0,
    bowl_wides integer DEFAULT 0,
    bowl_noballs integer DEFAULT 0,
    bowl_er numeric,
    field_catches integer,
    field_stumpings integer,
    field_runouts integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    bat_score integer DEFAULT 0,
    bowl_score integer DEFAULT 0,
    field_score integer DEFAULT 0,
    total integer DEFAULT 0,
    bat_status character varying,
    bat_singles integer DEFAULT 0,
    bat_duck integer DEFAULT 0,
    bat_hundreds integer DEFAULT 0,
    bat_fifties integer DEFAULT 0,
    inning_no integer DEFAULT 1,
    bat_double_hundreds integer
);


--
-- Name: player_scores_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.player_scores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: player_scores_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.player_scores_id_seq OWNED BY public.player_scores.id;


--
-- Name: players; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.players (
    id integer NOT NULL,
    name character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    player_category character varying,
    price integer DEFAULT 0,
    participant_team_id integer,
    player_xp integer DEFAULT 0,
    photo_url character varying,
    api_player_id character varying,
    image character varying,
    odi_career public.hstore,
    ipl_career public.hstore,
    t20_career public.hstore,
    date_of_birth timestamp without time zone,
    test_career public.hstore,
    api_provider character varying
);


--
-- Name: players_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.players_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: players_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.players_id_seq OWNED BY public.players.id;


--
-- Name: players_teams; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.players_teams (
    user_team_id integer,
    player_id integer
);


--
-- Name: rails_admin_histories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.rails_admin_histories (
    id integer NOT NULL,
    message text,
    username character varying,
    item integer,
    "table" character varying,
    month smallint,
    year bigint,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: rails_admin_histories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.rails_admin_histories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rails_admin_histories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.rails_admin_histories_id_seq OWNED BY public.rails_admin_histories.id;


--
-- Name: referral_credits; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.referral_credits (
    id integer NOT NULL,
    amount_used double precision DEFAULT 0.0,
    amount_remaining double precision DEFAULT 0.0,
    expired boolean DEFAULT false,
    wallet_id integer,
    expired_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: referral_credits_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.referral_credits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: referral_credits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.referral_credits_id_seq OWNED BY public.referral_credits.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


--
-- Name: tournaments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tournaments (
    id integer NOT NULL,
    name character varying,
    start_time timestamp without time zone,
    end_time timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    api_series_id character varying,
    api_provider character varying,
    details json,
    region character varying
);


--
-- Name: tournaments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tournaments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tournaments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tournaments_id_seq OWNED BY public.tournaments.id;


--
-- Name: user_teams; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_teams (
    id integer NOT NULL,
    user_id integer,
    totalscore double precision DEFAULT 0,
    has_won boolean DEFAULT false,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    match_id integer,
    league_id integer,
    rank integer,
    token character varying,
    transaction_data public.hstore,
    captain_id integer,
    contest_id integer,
    captain_id1 integer,
    has_tied boolean DEFAULT false,
    total_wallet_balance_used double precision,
    referral_wallet_balance_used double precision,
    main_wallet_balance_used double precision,
    batsman_ids character varying[] DEFAULT '{}'::character varying[],
    bowler_ids character varying[] DEFAULT '{}'::character varying[],
    allrounder_ids character varying[] DEFAULT '{}'::character varying[],
    wicketkeeper_ids character varying[] DEFAULT '{}'::character varying[]
);


--
-- Name: user_teams_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_teams_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_teams_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_teams_id_seq OWNED BY public.user_teams.id;


--
-- Name: user_transactions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_transactions (
    id integer NOT NULL,
    params public.hstore,
    payment_gateway_response public.hstore,
    user_id integer,
    token character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    league_id integer,
    user_team_id integer,
    match_id integer,
    state character varying DEFAULT 'created'::character varying,
    transaction_category character varying DEFAULT ''::character varying,
    total_amount integer DEFAULT 0,
    balance_amount integer DEFAULT 0
);


--
-- Name: user_transactions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_transactions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_transactions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_transactions_id_seq OWNED BY public.user_transactions.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id integer NOT NULL,
    first_name character varying,
    last_name character varying,
    image_url character varying,
    email character varying DEFAULT ''::character varying,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying,
    last_sign_in_ip character varying,
    confirmation_token character varying,
    confirmed_at timestamp without time zone,
    confirmation_sent_at timestamp without time zone,
    unconfirmed_email character varying,
    failed_attempts integer DEFAULT 0 NOT NULL,
    unlock_token character varying,
    locked_at timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    is_admin boolean,
    username character varying,
    credits integer,
    phone character varying NOT NULL,
    date_of_birth date,
    state character varying,
    country character varying DEFAULT 'IN'::character varying,
    meta_info public.hstore,
    referred_by_id integer,
    referral_code character varying,
    is_spam_referrer boolean DEFAULT false,
    authentication_token character varying DEFAULT ''::character varying,
    wallet_balance character varying DEFAULT ''::character varying,
    registration_info public.hstore
);


--
-- Name: users_coupons; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users_coupons (
    id integer NOT NULL,
    user_id integer,
    coupon_id integer,
    league_id integer,
    user_transaction_id integer,
    state character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    amount double precision
);


--
-- Name: users_coupons_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_coupons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_coupons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_coupons_id_seq OWNED BY public.users_coupons.id;


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: wallet_snaps; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.wallet_snaps (
    id integer NOT NULL,
    wallet_id integer,
    user_id integer,
    main_balance double precision,
    referral_balance double precision,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: wallet_snaps_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.wallet_snaps_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wallet_snaps_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.wallet_snaps_id_seq OWNED BY public.wallet_snaps.id;


--
-- Name: wallet_transactions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.wallet_transactions (
    id integer NOT NULL,
    user_id integer,
    wallet_id integer,
    banked_credit_id integer,
    amount double precision,
    transaction_type character varying,
    category character varying,
    current_wallet_amount json,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    user_transaction_id integer
);


--
-- Name: wallet_transactions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.wallet_transactions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wallet_transactions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.wallet_transactions_id_seq OWNED BY public.wallet_transactions.id;


--
-- Name: wallets; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.wallets (
    id integer NOT NULL,
    main_balance double precision DEFAULT 0.0,
    referral_balance double precision DEFAULT 0.0,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: wallets_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.wallets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wallets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.wallets_id_seq OWNED BY public.wallets.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.authentications ALTER COLUMN id SET DEFAULT nextval('public.authentications_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.banked_credits ALTER COLUMN id SET DEFAULT nextval('public.banked_credits_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_stats ALTER COLUMN id SET DEFAULT nextval('public.career_stats_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.challenges ALTER COLUMN id SET DEFAULT nextval('public.challenges_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contests ALTER COLUMN id SET DEFAULT nextval('public.contests_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.coupons ALTER COLUMN id SET DEFAULT nextval('public.coupons_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.leagues ALTER COLUMN id SET DEFAULT nextval('public.leagues_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.matches ALTER COLUMN id SET DEFAULT nextval('public.matches_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.meta_infos ALTER COLUMN id SET DEFAULT nextval('public.meta_infos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.money_transactions ALTER COLUMN id SET DEFAULT nextval('public.money_transactions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.participant_teams ALTER COLUMN id SET DEFAULT nextval('public.participant_teams_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.player_scores ALTER COLUMN id SET DEFAULT nextval('public.player_scores_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.players ALTER COLUMN id SET DEFAULT nextval('public.players_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rails_admin_histories ALTER COLUMN id SET DEFAULT nextval('public.rails_admin_histories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.referral_credits ALTER COLUMN id SET DEFAULT nextval('public.referral_credits_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tournaments ALTER COLUMN id SET DEFAULT nextval('public.tournaments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_teams ALTER COLUMN id SET DEFAULT nextval('public.user_teams_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_transactions ALTER COLUMN id SET DEFAULT nextval('public.user_transactions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users_coupons ALTER COLUMN id SET DEFAULT nextval('public.users_coupons_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wallet_snaps ALTER COLUMN id SET DEFAULT nextval('public.wallet_snaps_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wallet_transactions ALTER COLUMN id SET DEFAULT nextval('public.wallet_transactions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wallets ALTER COLUMN id SET DEFAULT nextval('public.wallets_id_seq'::regclass);


--
-- Name: authentications_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.authentications
    ADD CONSTRAINT authentications_pkey PRIMARY KEY (id);


--
-- Name: banked_credits_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.banked_credits
    ADD CONSTRAINT banked_credits_pkey PRIMARY KEY (id);


--
-- Name: career_stats_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.career_stats
    ADD CONSTRAINT career_stats_pkey PRIMARY KEY (id);


--
-- Name: challenges_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.challenges
    ADD CONSTRAINT challenges_pkey PRIMARY KEY (id);


--
-- Name: contests_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contests
    ADD CONSTRAINT contests_pkey PRIMARY KEY (id);


--
-- Name: coupons_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.coupons
    ADD CONSTRAINT coupons_pkey PRIMARY KEY (id);


--
-- Name: leagues_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.leagues
    ADD CONSTRAINT leagues_pkey PRIMARY KEY (id);


--
-- Name: matches_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.matches
    ADD CONSTRAINT matches_pkey PRIMARY KEY (id);


--
-- Name: meta_infos_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.meta_infos
    ADD CONSTRAINT meta_infos_pkey PRIMARY KEY (id);


--
-- Name: money_transactions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.money_transactions
    ADD CONSTRAINT money_transactions_pkey PRIMARY KEY (id);


--
-- Name: participant_teams_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.participant_teams
    ADD CONSTRAINT participant_teams_pkey PRIMARY KEY (id);


--
-- Name: player_scores_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.player_scores
    ADD CONSTRAINT player_scores_pkey PRIMARY KEY (id);


--
-- Name: players_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.players
    ADD CONSTRAINT players_pkey PRIMARY KEY (id);


--
-- Name: rails_admin_histories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rails_admin_histories
    ADD CONSTRAINT rails_admin_histories_pkey PRIMARY KEY (id);


--
-- Name: referral_credits_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.referral_credits
    ADD CONSTRAINT referral_credits_pkey PRIMARY KEY (id);


--
-- Name: tournaments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tournaments
    ADD CONSTRAINT tournaments_pkey PRIMARY KEY (id);


--
-- Name: user_teams_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_teams
    ADD CONSTRAINT user_teams_pkey PRIMARY KEY (id);


--
-- Name: user_transactions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_transactions
    ADD CONSTRAINT user_transactions_pkey PRIMARY KEY (id);


--
-- Name: users_coupons_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users_coupons
    ADD CONSTRAINT users_coupons_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: wallet_snaps_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wallet_snaps
    ADD CONSTRAINT wallet_snaps_pkey PRIMARY KEY (id);


--
-- Name: wallet_transactions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wallet_transactions
    ADD CONSTRAINT wallet_transactions_pkey PRIMARY KEY (id);


--
-- Name: wallets_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wallets
    ADD CONSTRAINT wallets_pkey PRIMARY KEY (id);


--
-- Name: index_authentications_on_provider; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_authentications_on_provider ON public.authentications USING btree (provider);


--
-- Name: index_career_stats_on_player_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_career_stats_on_player_id ON public.career_stats USING btree (player_id);


--
-- Name: index_challenges_on_league_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_challenges_on_league_id ON public.challenges USING btree (league_id);


--
-- Name: index_challenges_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_challenges_on_user_id ON public.challenges USING btree (user_id);


--
-- Name: index_coupons_on_match_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_coupons_on_match_id ON public.coupons USING btree (match_id);


--
-- Name: index_coupons_on_tournament_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_coupons_on_tournament_id ON public.coupons USING btree (tournament_id);


--
-- Name: index_leagues_on_invite_code; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_leagues_on_invite_code ON public.leagues USING btree (invite_code);


--
-- Name: index_money_transactions_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_money_transactions_on_user_id ON public.money_transactions USING btree (user_id);


--
-- Name: index_participant_teams_on_current_squad; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_participant_teams_on_current_squad ON public.participant_teams USING gin (current_squad);


--
-- Name: index_players_teams_on_player_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_players_teams_on_player_id ON public.players_teams USING btree (player_id);


--
-- Name: index_players_teams_on_user_team_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_players_teams_on_user_team_id ON public.players_teams USING btree (user_team_id);


--
-- Name: index_rails_admin_histories; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_rails_admin_histories ON public.rails_admin_histories USING btree (item, "table", month, year);


--
-- Name: index_referral_credits_on_wallet_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_referral_credits_on_wallet_id ON public.referral_credits USING btree (wallet_id);


--
-- Name: index_user_teams_on_token; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_teams_on_token ON public.user_teams USING btree (token);


--
-- Name: index_users_coupons_on_coupon_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_coupons_on_coupon_id ON public.users_coupons USING btree (coupon_id);


--
-- Name: index_users_coupons_on_league_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_coupons_on_league_id ON public.users_coupons USING btree (league_id);


--
-- Name: index_users_coupons_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_coupons_on_user_id ON public.users_coupons USING btree (user_id);


--
-- Name: index_users_coupons_on_user_transaction_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_coupons_on_user_transaction_id ON public.users_coupons USING btree (user_transaction_id);


--
-- Name: index_users_on_confirmation_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_confirmation_token ON public.users USING btree (confirmation_token);


--
-- Name: index_users_on_phone; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_phone ON public.users USING btree (phone);


--
-- Name: index_users_on_referral_code; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_referral_code ON public.users USING btree (referral_code);


--
-- Name: index_users_on_referred_by_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_referred_by_id ON public.users USING btree (referred_by_id);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON public.users USING btree (reset_password_token);


--
-- Name: index_users_on_unlock_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_unlock_token ON public.users USING btree (unlock_token);


--
-- Name: index_users_on_username; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_username ON public.users USING btree (username);


--
-- Name: index_wallet_snaps_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_wallet_snaps_on_user_id ON public.wallet_snaps USING btree (user_id);


--
-- Name: index_wallet_snaps_on_wallet_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_wallet_snaps_on_wallet_id ON public.wallet_snaps USING btree (wallet_id);


--
-- Name: index_wallet_transactions_on_user_transaction_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_wallet_transactions_on_user_transaction_id ON public.wallet_transactions USING btree (user_transaction_id);


--
-- Name: index_wallets_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_wallets_on_user_id ON public.wallets USING btree (user_id);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_schema_migrations ON public.schema_migrations USING btree (version);


--
-- Name: fk_rails_5ca53b9fd9; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.challenges
    ADD CONSTRAINT fk_rails_5ca53b9fd9 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: fk_rails_b11f8f5250; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.challenges
    ADD CONSTRAINT fk_rails_b11f8f5250 FOREIGN KEY (league_id) REFERENCES public.leagues(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO schema_migrations (version) VALUES ('20130909170542');

INSERT INTO schema_migrations (version) VALUES ('20130909194719');

INSERT INTO schema_migrations (version) VALUES ('20131020063216');

INSERT INTO schema_migrations (version) VALUES ('20131021224642');

INSERT INTO schema_migrations (version) VALUES ('20140204233100');

INSERT INTO schema_migrations (version) VALUES ('20140204233952');

INSERT INTO schema_migrations (version) VALUES ('20150513113212');

INSERT INTO schema_migrations (version) VALUES ('20150513113432');

INSERT INTO schema_migrations (version) VALUES ('20150513120422');

INSERT INTO schema_migrations (version) VALUES ('20150513204612');

INSERT INTO schema_migrations (version) VALUES ('20150513205437');

INSERT INTO schema_migrations (version) VALUES ('20150513210215');

INSERT INTO schema_migrations (version) VALUES ('20150513210612');

INSERT INTO schema_migrations (version) VALUES ('20150513211911');

INSERT INTO schema_migrations (version) VALUES ('20150513213113');

INSERT INTO schema_migrations (version) VALUES ('20150513220034');

INSERT INTO schema_migrations (version) VALUES ('20150515180454');

INSERT INTO schema_migrations (version) VALUES ('20150515183727');

INSERT INTO schema_migrations (version) VALUES ('20150515184346');

INSERT INTO schema_migrations (version) VALUES ('20150515214933');

INSERT INTO schema_migrations (version) VALUES ('20150515215246');

INSERT INTO schema_migrations (version) VALUES ('20150527213618');

INSERT INTO schema_migrations (version) VALUES ('20150609110929');

INSERT INTO schema_migrations (version) VALUES ('20150610215858');

INSERT INTO schema_migrations (version) VALUES ('20150611125544');

INSERT INTO schema_migrations (version) VALUES ('20150611160415');

INSERT INTO schema_migrations (version) VALUES ('20150618004136');

INSERT INTO schema_migrations (version) VALUES ('20150618024417');

INSERT INTO schema_migrations (version) VALUES ('20150619170741');

INSERT INTO schema_migrations (version) VALUES ('20150619201124');

INSERT INTO schema_migrations (version) VALUES ('20150717115239');

INSERT INTO schema_migrations (version) VALUES ('20150717121034');

INSERT INTO schema_migrations (version) VALUES ('20150717122749');

INSERT INTO schema_migrations (version) VALUES ('20150718110504');

INSERT INTO schema_migrations (version) VALUES ('20151109081338');

INSERT INTO schema_migrations (version) VALUES ('20151209171626');

INSERT INTO schema_migrations (version) VALUES ('20151217195553');

INSERT INTO schema_migrations (version) VALUES ('20151217202453');

INSERT INTO schema_migrations (version) VALUES ('20151218111552');

INSERT INTO schema_migrations (version) VALUES ('20151218174652');

INSERT INTO schema_migrations (version) VALUES ('20151218185919');

INSERT INTO schema_migrations (version) VALUES ('20151220094732');

INSERT INTO schema_migrations (version) VALUES ('20151220125645');

INSERT INTO schema_migrations (version) VALUES ('20151221114034');

INSERT INTO schema_migrations (version) VALUES ('20151223100623');

INSERT INTO schema_migrations (version) VALUES ('20151223101128');

INSERT INTO schema_migrations (version) VALUES ('20160101142206');

INSERT INTO schema_migrations (version) VALUES ('20160101142953');

INSERT INTO schema_migrations (version) VALUES ('20160109165728');

INSERT INTO schema_migrations (version) VALUES ('20160203123907');

INSERT INTO schema_migrations (version) VALUES ('20160210121240');

INSERT INTO schema_migrations (version) VALUES ('20160213102850');

INSERT INTO schema_migrations (version) VALUES ('20160213174054');

INSERT INTO schema_migrations (version) VALUES ('20160215083517');

INSERT INTO schema_migrations (version) VALUES ('20160223200514');

INSERT INTO schema_migrations (version) VALUES ('20160224104352');

INSERT INTO schema_migrations (version) VALUES ('20160226130839');

INSERT INTO schema_migrations (version) VALUES ('20160229173941');

INSERT INTO schema_migrations (version) VALUES ('20160302200807');

INSERT INTO schema_migrations (version) VALUES ('20160311133455');

INSERT INTO schema_migrations (version) VALUES ('20160316162507');

INSERT INTO schema_migrations (version) VALUES ('20160317183744');

INSERT INTO schema_migrations (version) VALUES ('20160318134401');

INSERT INTO schema_migrations (version) VALUES ('20160331192336');

INSERT INTO schema_migrations (version) VALUES ('20160405152726');

INSERT INTO schema_migrations (version) VALUES ('20160416081520');

INSERT INTO schema_migrations (version) VALUES ('20160416084025');

INSERT INTO schema_migrations (version) VALUES ('20160427104956');

INSERT INTO schema_migrations (version) VALUES ('20160509103036');

INSERT INTO schema_migrations (version) VALUES ('20160719101737');

INSERT INTO schema_migrations (version) VALUES ('20160719103643');

INSERT INTO schema_migrations (version) VALUES ('20161212120259');

INSERT INTO schema_migrations (version) VALUES ('20161223064117');

INSERT INTO schema_migrations (version) VALUES ('20161227142505');

INSERT INTO schema_migrations (version) VALUES ('20161228093408');

INSERT INTO schema_migrations (version) VALUES ('20161228103600');

INSERT INTO schema_migrations (version) VALUES ('20170103053106');

INSERT INTO schema_migrations (version) VALUES ('20170106121856');

INSERT INTO schema_migrations (version) VALUES ('20170116105035');

INSERT INTO schema_migrations (version) VALUES ('20170116110056');

INSERT INTO schema_migrations (version) VALUES ('20170124092635');

INSERT INTO schema_migrations (version) VALUES ('20170213081750');

INSERT INTO schema_migrations (version) VALUES ('20170302113915');

INSERT INTO schema_migrations (version) VALUES ('20170322074718');

INSERT INTO schema_migrations (version) VALUES ('20170322093122');

INSERT INTO schema_migrations (version) VALUES ('20170424105609');

INSERT INTO schema_migrations (version) VALUES ('20170427074255');

INSERT INTO schema_migrations (version) VALUES ('20170427102617');

INSERT INTO schema_migrations (version) VALUES ('20170427114528');

INSERT INTO schema_migrations (version) VALUES ('20170501094033');

INSERT INTO schema_migrations (version) VALUES ('20170501104719');

INSERT INTO schema_migrations (version) VALUES ('20170502055541');

INSERT INTO schema_migrations (version) VALUES ('20170502102801');

INSERT INTO schema_migrations (version) VALUES ('20170619072032');

INSERT INTO schema_migrations (version) VALUES ('20170619125214');

INSERT INTO schema_migrations (version) VALUES ('20170622071421');

INSERT INTO schema_migrations (version) VALUES ('20170717083141');

INSERT INTO schema_migrations (version) VALUES ('20170721063550');

INSERT INTO schema_migrations (version) VALUES ('20170726105942');

INSERT INTO schema_migrations (version) VALUES ('20170726133047');

INSERT INTO schema_migrations (version) VALUES ('20170808070129');

INSERT INTO schema_migrations (version) VALUES ('20170907113612');

INSERT INTO schema_migrations (version) VALUES ('20170907123457');

INSERT INTO schema_migrations (version) VALUES ('20170928102928');

INSERT INTO schema_migrations (version) VALUES ('20171115080458');

INSERT INTO schema_migrations (version) VALUES ('20171128093104');

INSERT INTO schema_migrations (version) VALUES ('20171204130937');

INSERT INTO schema_migrations (version) VALUES ('20171213104938');

INSERT INTO schema_migrations (version) VALUES ('20180330101646');

INSERT INTO schema_migrations (version) VALUES ('20180421182048');

INSERT INTO schema_migrations (version) VALUES ('20180607181327');

