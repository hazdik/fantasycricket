# == Schema Information
#
# Table name: user_teams
#
#  id                           :integer          not null, primary key
#  user_id                      :integer
#  totalscore                   :integer          default(0)
#  has_won                      :boolean          default(FALSE)
#  created_at                   :datetime
#  updated_at                   :datetime
#  match_id                     :integer
#  league_id                    :integer
#  rank                         :integer
#  token                        :string
#  transaction_data             :hstore
#  captain_id                   :integer
#  contest_id                   :integer
#  captain_id1                  :integer
#  has_tied                     :boolean          default(FALSE)
#  total_wallet_balance_used    :float
#  referral_wallet_balance_used :float
#  main_wallet_balance_used     :float
#

FactoryGirl.define do
  factory :user_team do
    name "MyString"
user_id 1
  end

end
