# == Schema Information
#
# Table name: leagues
#
#  id                  :integer          not null, primary key
#  name                :string
#  match_id            :integer
#  limit               :integer          default(0)
#  is_live             :boolean          default(FALSE)
#  prize_money         :integer          default(0)
#  is_multiple_allowed :boolean          default(FALSE)
#  is_fake             :boolean          default(FALSE)
#  fake_size           :integer          default(0)
#  created_at          :datetime
#  updated_at          :datetime
#  entry_fee           :integer
#  contest_id          :integer
#  is_private          :boolean          default(FALSE)
#  is_tied             :boolean          default(FALSE)
#  user_teams_count    :integer          default(0)
#  invite_code         :string
#  winner_ranks        :text             default([]), is an Array
#  loser_ranks         :text             default([]), is an Array
#  tied_team_ranks     :text             default([]), is an Array
#

FactoryGirl.define do
  factory :league do
    name "MyString"
description "MyText"
match_id 1
limit 1
joined 1
is_live false
prize_money 1
is_multiple_allowed false
is_fake false
  end

end
