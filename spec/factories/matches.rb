# == Schema Information
#
# Table name: matches
#
#  id            :integer          not null, primary key
#  home_team_id  :integer
#  away_team_id  :integer
#  created_at    :datetime
#  updated_at    :datetime
#  start_time    :datetime
#  end_time      :datetime
#  tournament_id :integer
#  is_live       :boolean          default(FALSE)
#  title         :string
#  api_match_id  :string
#  mtype         :string
#  team_names    :string           default("")
#  ms            :string           default("Match is yet to start")
#

FactoryGirl.define do
  factory :match do
    home_team_id 1
away_team_id 1
home_team_score "MyString"
away_team_score "MyString"
  end

end
