# == Schema Information
#
# Table name: player_scores
#
#  id              :integer          not null, primary key
#  match_id        :integer
#  player_id       :integer
#  bat_runs_scored :integer          default(0)
#  bat_balls       :integer
#  bat_fours       :integer          default(0)
#  bat_sixes       :integer          default(0)
#  bat_sr          :decimal(, )
#  bowl_overs      :integer
#  bowl_maidens    :integer          default(0)
#  bowl_runs       :integer
#  bowl_wickets    :integer          default(0)
#  bowl_wides      :integer          default(0)
#  bowl_noballs    :integer          default(0)
#  bowl_er         :decimal(, )
#  field_catches   :integer
#  field_stumpings :integer
#  field_runouts   :integer
#  created_at      :datetime
#  updated_at      :datetime
#  bat_score       :integer          default(0)
#  bowl_score      :integer          default(0)
#  field_score     :integer          default(0)
#  total           :integer          default(0)
#  bat_status      :string
#  bat_singles     :integer          default(0)
#  bat_duck        :integer          default(0)
#  bat_hundreds    :integer          default(0)
#  bat_fifties     :integer          default(0)
#  inning_no       :integer          default(1)
#

FactoryGirl.define do
  factory :player_score do
    
  end

end
