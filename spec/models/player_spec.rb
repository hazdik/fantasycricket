# == Schema Information
#
# Table name: players
#
#  id                  :integer          not null, primary key
#  name                :string
#  created_at          :datetime
#  updated_at          :datetime
#  player_category     :string
#  price               :integer          default(0)
#  participant_team_id :integer
#  player_xp           :integer          default(0)
#  photo_url           :string
#  api_player_id       :string
#  image               :string
#  odi_career          :hstore
#  ipl_career          :hstore
#  t20_career          :hstore
#  date_of_birth       :datetime
#  test_career         :hstore
#

require 'rails_helper'

RSpec.describe Player, :type => :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
